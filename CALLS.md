## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for flexiWAN. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for flexiWAN.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Cyberark. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">extendedAccountOverview(callback)</td>
    <td style="padding:15px">Extended Account Overview</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/ExtendedAccounts/2_25/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccounts(search, searchType, sort, offset, limit, filter, callback)</td>
    <td style="padding:15px">Get Accounts</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAccount(body, callback)</td>
    <td style="padding:15px">Add Account</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountDetails(callback)</td>
    <td style="padding:15px">Get Account Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Accounts/2_25?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccount(body, callback)</td>
    <td style="padding:15px">Update Account</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Accounts/2_25?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccount(callback)</td>
    <td style="padding:15px">Delete Account</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Accounts/2_25?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountActivity(callback)</td>
    <td style="padding:15px">Get Account Activity</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Accounts/2_25/Activities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPendingAccount(body, callback)</td>
    <td style="padding:15px">Add Pending Account</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/PendingAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">linkanAccount(body, callback)</td>
    <td style="padding:15px">Link an Account</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Accounts/2_25/LinkAccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocConnectthroughPSM(body, callback)</td>
    <td style="padding:15px">Ad-Hoc Connect through PSM</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Accounts/AdHocConnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePasswordImmediately(body, callback)</td>
    <td style="padding:15px">Change Password Immediately</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Accounts/2_25/Change?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePasswordintheVaultOnly(body, callback)</td>
    <td style="padding:15px">Change Password in the Vault Only</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Accounts/2_25/Password/Update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePasswordSetNextPassword(body, callback)</td>
    <td style="padding:15px">Change Password, Set Next Password</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/Accounts/2_25/SetNextPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkInanExclusiveAccount(callback)</td>
    <td style="padding:15px">Check In an Exclusive Account</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Accounts/2_25/CheckIn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectUsingPSM(body, callback)</td>
    <td style="padding:15px">Connect Using PSM</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Accounts/2_25/PSMConnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJustinTimeAccess(callback)</td>
    <td style="padding:15px">Get Just in Time Access</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Accounts/2_25/grantAdministrativeAccess?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPasswordValue(body, callback)</td>
    <td style="padding:15px">Get Password Value</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Accounts/2_25/Password/Retrieve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reconcilePassword(callback)</td>
    <td style="padding:15px">Reconcile Password</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Accounts/2_25/Reconcile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyPassword(callback)</td>
    <td style="padding:15px">Verify Password</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Accounts/2_25/Verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePassword(body, callback)</td>
    <td style="padding:15px">Change Password</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Accounts/2_25/ChangeCredentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredAccountDetails(callback)</td>
    <td style="padding:15px">Get Discovered Account Details</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/DiscoveredAccounts/2_25?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredAccounts(filter, search, searchType, offset, limit, callback)</td>
    <td style="padding:15px">Get Discovered Accounts</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/DiscoveredAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDiscoveredAccountsV108(body, callback)</td>
    <td style="padding:15px">Add Discovered Accounts (v10.8+)</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/DiscoveredAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logonCyberArkAuthentication(body, callback)</td>
    <td style="padding:15px">Logon - CyberArk Authentication</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Auth/CyberArk/Logon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logonLDAPAuthentication(body, callback)</td>
    <td style="padding:15px">Logon - LDAP Authentication</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Auth/LDAP/Logon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logonRADIUSAuthentication(body, callback)</td>
    <td style="padding:15px">Logon - RADIUS Authentication</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Auth/radius/Logon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logonWindowsAuthentication(body, callback)</td>
    <td style="padding:15px">Logon - Windows Authentication</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/auth/Windows/Logon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logoff(callback)</td>
    <td style="padding:15px">Logoff</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Auth/Logoff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSafes(callback)</td>
    <td style="padding:15px">Get All Safes</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Safes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSafe(body, callback)</td>
    <td style="padding:15px">Add Safe</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Safes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSafebyPlatformID(callback)</td>
    <td style="padding:15px">Get Safe by Platform ID</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Platforms/Safes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSafeDetails(callback)</td>
    <td style="padding:15px">Get Safe Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Safes/D-APP-Ansible?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSafe(callback)</td>
    <td style="padding:15px">Delete Safe</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Safes/D-APP-Ansible?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSafeAccountGroups(callback)</td>
    <td style="padding:15px">Get Safe Account Groups</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Safes/D-APP-Ansible/AccountGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSafeMembers(callback)</td>
    <td style="padding:15px">Get Safe Members</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Safes/D-APP-Ansible/Members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSafeMember(body, callback)</td>
    <td style="padding:15px">Add Safe Member</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Safes/D-APP-Ansible/Members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserDetails(callback)</td>
    <td style="padding:15px">Get User Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(body, callback)</td>
    <td style="padding:15px">Update User</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(callback)</td>
    <td style="padding:15px">Delete User</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(filter, search, extendedDetails, callback)</td>
    <td style="padding:15px">Get Users</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUser(body, callback)</td>
    <td style="padding:15px">Add User</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetUserPassword(body, callback)</td>
    <td style="padding:15px">Reset User Password</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users/ResetPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateUser(callback)</td>
    <td style="padding:15px">Activate User</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users/Activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(filter, search, callback)</td>
    <td style="padding:15px">Get Groups</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/UserGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroup(body, callback)</td>
    <td style="padding:15px">Create Group</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/UserGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMembertoGroup(body, callback)</td>
    <td style="padding:15px">Add Member to Group</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/UserGroups/Members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroup(body, callback)</td>
    <td style="padding:15px">Update Group</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/UserGroups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(callback)</td>
    <td style="padding:15px">Delete Group</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/UserGroups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeUserfromGroup(callback)</td>
    <td style="padding:15px">Remove User from Group</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/UserGroups/Members/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountDetailsV93(keywords, safe, callback)</td>
    <td style="padding:15px">Get Account Details [v9.3+]</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAccountV90(body, callback)</td>
    <td style="padding:15px">Add Account [v9.0+]</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Account?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccountDetailsV95(body, callback)</td>
    <td style="padding:15px">Update Account Details [v9.5+]</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Accounts/2_25?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountV93(callback)</td>
    <td style="padding:15px">Delete Account [v9.3+]</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Accounts/2_25?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountValueV97910(callback)</td>
    <td style="padding:15px">Get Account Value [v9.7-9.10]</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Accounts/2_25/Credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeCredentialsandSetNextPasswordV10(body, callback)</td>
    <td style="padding:15px">Change Credentials and Set Next Password [v10]</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Accounts/2_25/SetNextPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeCredentialsintheVaultV10(body, callback)</td>
    <td style="padding:15px">Change Credentials in the Vault [v10]</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Accounts/2_25/Password/Update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyCredentialsV97V995(body, callback)</td>
    <td style="padding:15px">Verify Credentials [v9.7-v9.9.5]</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Accounts/2_25/VerifyCredentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logon(body, callback)</td>
    <td style="padding:15px">Logon</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/auth/Cyberark/CyberArkAuthenticationService.svc/Logon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogoff(callback)</td>
    <td style="padding:15px">Logoff</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/auth/Cyberark/CyberArkAuthenticationService.svc/Logoff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSafes1(callback)</td>
    <td style="padding:15px">Get All Safes</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Safes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSafe(body, callback)</td>
    <td style="padding:15px">Add Safe</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Safes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSafeDetails1(callback)</td>
    <td style="padding:15px">Get Safe Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Safes/D-APP-Ansible?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSafe(body, callback)</td>
    <td style="padding:15px">Update Safe</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Safes/D-APP-Ansible?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSafeMembers1(callback)</td>
    <td style="padding:15px">Get Safe Members</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Safes/D-APP-Ansible/Members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSafeMember(body, callback)</td>
    <td style="padding:15px">Update Safe Member</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Safes/D-APP-Ansible/Members/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSafeMember(callback)</td>
    <td style="padding:15px">Delete Safe Member</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Safes/D-APP-Ansible/Members/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserDetails1(callback)</td>
    <td style="padding:15px">Get User Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Users/Svc_CybrAutomation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpdateUser(body, callback)</td>
    <td style="padding:15px">Update User</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Users/Svc_CybrAutomation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser1(callback)</td>
    <td style="padding:15px">Delete User</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Users/Svc_CybrAutomation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loggedOnUserDetails(callback)</td>
    <td style="padding:15px">Logged On User Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/User?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddUser(body, callback)</td>
    <td style="padding:15px">Add User</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putActivateUser(callback)</td>
    <td style="padding:15px">Activate User</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddMembertoGroup(body, callback)</td>
    <td style="padding:15px">Add Member to Group</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Groups/Users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountGroupbySafe(safe, callback)</td>
    <td style="padding:15px">Get Account Group by Safe</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/AccountGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountGroupMembers(callback)</td>
    <td style="padding:15px">Get Account Group Members</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/AccountGroups/Members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAccountGroup(body, callback)</td>
    <td style="padding:15px">Add Account Group</td>
    <td style="padding:15px">{base_path}/{version}/API/AccountGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAccounttoAccountGroup(body, callback)</td>
    <td style="padding:15px">Add Account to Account Group</td>
    <td style="padding:15px">{base_path}/{version}/API/AccountGroups/Members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMemberfromAccountGroup(body, callback)</td>
    <td style="padding:15px">Delete Member from Account Group</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/AccountGroups/Members/2_25?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllBulkAccountUploadsforUser(callback)</td>
    <td style="padding:15px">Get All Bulk Account Uploads for User</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/bulkactions/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBulkUploadofAccounts(body, callback)</td>
    <td style="padding:15px">Create Bulk Upload of Accounts</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/bulkactions/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBulkAccountUploadResult(callback)</td>
    <td style="padding:15px">Get Bulk Account Upload Result</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/bulkactions/accounts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApplications(location, includeSublocations, callback)</td>
    <td style="padding:15px">List Applications</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addApplication(body, callback)</td>
    <td style="padding:15px">Add Application</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteaSpecificApplication(appID, callback)</td>
    <td style="padding:15px">Delete a Specific Application</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallAuthenticationMethodsofaSpecificApplication(callback)</td>
    <td style="padding:15px">List all Authentication Methods of a Specific Application</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Applications/Ansible/Authentications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAuthentication(body, callback)</td>
    <td style="padding:15px">Add Authentication</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Applications/Ansible/Authentications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteaSpecificAuthentication(authID, callback)</td>
    <td style="padding:15px">Delete a Specific Authentication</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Applications/Ansible/Authentications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogon(concurrentSession, apiUse, sAMLResponse, callback)</td>
    <td style="padding:15px">Logon</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/auth/SAML/Logon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogoff1(callback)</td>
    <td style="padding:15px">Logoff</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/auth/SAML/Logoff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogon1(callback)</td>
    <td style="padding:15px">Logon</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/auth/Shared/RestfulAuthenticationService.svc/Logon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogoff12(callback)</td>
    <td style="padding:15px">Logoff</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/auth/Shared/RestfulAuthenticationService.svc/Logoff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOpenIDConnectIdentityProviders(callback)</td>
    <td style="padding:15px">Get All OpenID Connect Identity Providers</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/OIDC/Providers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOpenIDConnectIdentityProvider(body, callback)</td>
    <td style="padding:15px">Add OpenID Connect Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/OIDC/Providers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpecificOpenIDConnectIdentityProvider(callback)</td>
    <td style="padding:15px">Get Specific OpenID Connect Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/OIDC/Providers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOpenIDConnectIdentityProvider(body, callback)</td>
    <td style="padding:15px">Update OpenID Connect Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/OIDC/Providers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOpenIDConnectIdentityProvider(callback)</td>
    <td style="padding:15px">Delete OpenID Connect Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/OIDC/Providers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationMethods(callback)</td>
    <td style="padding:15px">Get Authentication Methods</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/AuthenticationMethods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAuthenticationMethod(body, callback)</td>
    <td style="padding:15px">Add Authentication Method</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/AuthenticationMethods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpecificAuthenticationMethod(callback)</td>
    <td style="padding:15px">Get Specific Authentication Method</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/AuthenticationMethods/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAuthenticationMethod(body, callback)</td>
    <td style="padding:15px">Update Authentication Method</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/AuthenticationMethods/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthenticationMethod(callback)</td>
    <td style="padding:15px">Delete Authentication Method</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/AuthenticationMethods/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pTAAuthentication(username, password, callback)</td>
    <td style="padding:15px">PTA Authentication</td>
    <td style="padding:15px">{base_path}/{version}/api/getauthtoken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">login(callback)</td>
    <td style="padding:15px">Login</td>
    <td style="padding:15px">{base_path}/{version}/authn/conjur/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">whoami(callback)</td>
    <td style="padding:15px">Whoami</td>
    <td style="padding:15px">{base_path}/{version}/whoami?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticate(callback)</td>
    <td style="padding:15px">Authenticate</td>
    <td style="padding:15px">{base_path}/{version}/authn/conjur/admin/authenticate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeYourPassword(callback)</td>
    <td style="padding:15px">Change Your Password</td>
    <td style="padding:15px">{base_path}/{version}/authn/conjur/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ePMAuthentication(body, callback)</td>
    <td style="padding:15px">EPM Authentication</td>
    <td style="padding:15px">{base_path}/{version}/Auth/EPM/Logon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">windowsAuthentication(body, callback)</td>
    <td style="padding:15px">Windows Authentication</td>
    <td style="padding:15px">{base_path}/{version}/Auth/Windows/Logon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logout(callback)</td>
    <td style="padding:15px">Logout</td>
    <td style="padding:15px">{base_path}/{version}/Auth/EPM/Logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPassword(appID, safe, object, callback)</td>
    <td style="padding:15px">GetPassword</td>
    <td style="padding:15px">{base_path}/{version}/AIMWebService/api/Accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rotateYourOwnAPIKey(callback)</td>
    <td style="padding:15px">Rotate Your Own API Key</td>
    <td style="padding:15px">{base_path}/{version}/authn/conjur/api_key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">batchRetrieval(variableIds, callback)</td>
    <td style="padding:15px">Batch Retrieval</td>
    <td style="padding:15px">{base_path}/{version}/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveASecret(callback)</td>
    <td style="padding:15px">Retrieve A Secret</td>
    <td style="padding:15px">{base_path}/{version}/secrets/conjur/variable/aws/access_key_id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addASecret(body, callback)</td>
    <td style="padding:15px">Add A Secret</td>
    <td style="padding:15px">{base_path}/{version}/secrets/conjur/variable/aws/access_key_id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addKubernetesCACertificate(body, callback)</td>
    <td style="padding:15px">Add Kubernetes CA Certificate</td>
    <td style="padding:15px">{base_path}/{version}/secrets/conjur/variable/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appendtoaPolicy(body, callback)</td>
    <td style="padding:15px">Append to a Policy</td>
    <td style="padding:15px">{base_path}/{version}/policies/conjur/policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replacePolicy(body, callback)</td>
    <td style="padding:15px">Replace Policy</td>
    <td style="padding:15px">{base_path}/{version}/policies/conjur/policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aWSauthnIamPolicyAppendtoRootPolicy(body, callback)</td>
    <td style="padding:15px">AWS authn-iam Policy Append to Root Policy</td>
    <td style="padding:15px">{base_path}/{version}/policies/conjur/policy/root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyAddAuthPolicyModifier(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/AddAuthPolicyModifier</td>
    <td style="padding:15px">{base_path}/{version}/Policy/AddAuthPolicyModifier?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyDeleteAuthPolicyModifier(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/DeleteAuthPolicyModifier</td>
    <td style="padding:15px">{base_path}/{version}/Policy/DeleteAuthPolicyModifier?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyDeletePolicyBlock(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/DeletePolicyBlock</td>
    <td style="padding:15px">{base_path}/{version}/Policy/DeletePolicyBlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetAuthPolicyModifiers(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetAuthPolicyModifiers</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetAuthPolicyModifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetNicePlinks(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetNicePlinks</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetNicePlinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetNicePolicyBlock(name, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetNicePolicyBlock</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetNicePolicyBlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetOathOtpClientName(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetOathOtpClientName</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetOathOtpClientName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetPasswordComplexityRequirements(uuidOrName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetPasswordComplexityRequirements</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetPasswordComplexityRequirements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetPlinks(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetPlinks</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetPlinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetPolicyBlock(name, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetPolicyBlock</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetPolicyBlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetPolicyBool(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetPolicyBool</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetPolicyBool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetPolicyInt(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetPolicyInt</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetPolicyInt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetPolicyMetaData(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetPolicyMetaData</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetPolicyMetaData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetPolicyString(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetPolicyString</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetPolicyString?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetRsop(userid, deviceid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetRsop</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetRsop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetU2fClientName(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetU2fClientName</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetU2fClientName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyGetUsingCloudMobileGP(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/GetUsingCloudMobileGP</td>
    <td style="padding:15px">{base_path}/{version}/Policy/GetUsingCloudMobileGP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicyPolicyChecks(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/PolicyChecks</td>
    <td style="padding:15px">{base_path}/{version}/Policy/PolicyChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicySavePolicyBlock2(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/SavePolicyBlock2</td>
    <td style="padding:15px">{base_path}/{version}/Policy/SavePolicyBlock2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicySavePolicyBlock3(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/SavePolicyBlock3</td>
    <td style="padding:15px">{base_path}/{version}/Policy/SavePolicyBlock3?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicySetPlinks(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/SetPlinks</td>
    <td style="padding:15px">{base_path}/{version}/Policy/SetPlinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicySetPlinksv2(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/SetPlinksv2</td>
    <td style="padding:15px">{base_path}/{version}/Policy/SetPlinksv2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPolicySetUsingCloudMobileGP(value, useCloudCA, hideMobilePolicyForAD, refreshInterval, gpUpdateInterval, activeDirectoryCA, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Policy/SetUsingCloudMobileGP</td>
    <td style="padding:15px">{base_path}/{version}/Policy/SetUsingCloudMobileGP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicyACL(aCLPolicyID, callback)</td>
    <td style="padding:15px">List Policy/ACL</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Policy/{pathv1}/PrivilegedCommands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPolicyACL(body, aCLPolicyID, callback)</td>
    <td style="padding:15px">Add Policy/ACL</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Policy/{pathv1}/PrivilegedCommands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyACL(id, aCLPolicyID, callback)</td>
    <td style="padding:15px">Delete Policy/ACL</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Policy/{pathv1}/PrivilegedCommands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">health(callback)</td>
    <td style="padding:15px">Health</td>
    <td style="padding:15px">{base_path}/{version}/health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">information(callback)</td>
    <td style="padding:15px">Information</td>
    <td style="padding:15px">{base_path}/{version}/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicies(kind, callback)</td>
    <td style="padding:15px">List Policies</td>
    <td style="padding:15px">{base_path}/{version}/resources/conjur?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxEventsintheLastDay(eventType, publisher, justification, applicationType, callback)</td>
    <td style="padding:15px">Inbox Events in the Last Day</td>
    <td style="padding:15px">{base_path}/{version}/EPM/API/Sets/Events/ApplicationEvents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxFromSpecificDates(publisher, dateFrom, dateTo, justification, applicationType, policyName, callback)</td>
    <td style="padding:15px">Inbox From Specific Dates</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Events/ApplicationEvents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">policyAuditFromDatePolicyName(publisher, dateFrom, dateTo, justification, applicationType, policyName, callback)</td>
    <td style="padding:15px">Policy Audit From Date & Policy Name</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Events/PolicyAudit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">threatDetectionFromSpecificDates(publisher, dateFrom, dateTo, justification, applicationType, callback)</td>
    <td style="padding:15px">Threat Detection From Specific Dates</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Events/ThreatDetection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComputers(callback)</td>
    <td style="padding:15px">Get Computers</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Computers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComputerGroups(callback)</td>
    <td style="padding:15px">Get Computer Groups</td>
    <td style="padding:15px">{base_path}/{version}/Set/ComputerGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicies(callback)</td>
    <td style="padding:15px">Get Policies</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicy(body, callback)</td>
    <td style="padding:15px">Create Policy</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyDetails(callback)</td>
    <td style="padding:15px">Get Policy Details</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Policies/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyForAdHocUserElevationPolicyonlyAdHocElevatePolicytype(body, callback)</td>
    <td style="padding:15px">Update Policy - For Ad Hoc User Elevation Policy only - "AdHocElevate" Policy type</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyForWindowsAdvancedPolicyonlyAdvancedWinAppPolicytype(body, callback)</td>
    <td style="padding:15px">Update Policy - For Windows Advanced Policy only - "AdvancedWinApp" Policy type</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(callback)</td>
    <td style="padding:15px">Delete Policy</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRansomwareMode(mode, callback)</td>
    <td style="padding:15px">Update Ransomware Mode</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Policies/Ransomware?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ePMVersion(callback)</td>
    <td style="padding:15px">EPM Version</td>
    <td style="padding:15px">{base_path}/{version}/Server/Version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rawEventDetails(callback)</td>
    <td style="padding:15px">Raw Event Details</td>
    <td style="padding:15px">{base_path}/{version}/Sets/Events/ApplicationEvents/Raw/112535?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sets(callback)</td>
    <td style="padding:15px">Sets</td>
    <td style="padding:15px">{base_path}/{version}/Sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAclCheckRowRight(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Acl/CheckRowRight</td>
    <td style="padding:15px">{base_path}/{version}/Acl/CheckRowRight?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAclGetAce(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Acl/GetAce</td>
    <td style="padding:15px">{base_path}/{version}/Acl/GetAce?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAclGetCollectionAces(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Acl/GetCollectionAces</td>
    <td style="padding:15px">{base_path}/{version}/Acl/GetCollectionAces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAclGetCollectionAcesHelper(table, rowkey, reduceSysadmin, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Acl/GetCollectionAcesHelper</td>
    <td style="padding:15px">{base_path}/{version}/Acl/GetCollectionAcesHelper?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAclGetDirAces(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Acl/GetDirAces</td>
    <td style="padding:15px">{base_path}/{version}/Acl/GetDirAces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAclGetEffectiveDirRights(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Acl/GetEffectiveDirRights</td>
    <td style="padding:15px">{base_path}/{version}/Acl/GetEffectiveDirRights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAclGetEffectiveFileRights(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Acl/GetEffectiveFileRights</td>
    <td style="padding:15px">{base_path}/{version}/Acl/GetEffectiveFileRights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAclGetEffectiveRowRights(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Acl/GetEffectiveRowRights</td>
    <td style="padding:15px">{base_path}/{version}/Acl/GetEffectiveRowRights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAclGetFileAces(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Acl/GetFileAces</td>
    <td style="padding:15px">{base_path}/{version}/Acl/GetFileAces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAclGetRowAces(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Acl/GetRowAces</td>
    <td style="padding:15px">{base_path}/{version}/Acl/GetRowAces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAclGetRowAcesHelper(reduceSys, inherit, table, rk, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Acl/GetRowAcesHelper</td>
    <td style="padding:15px">{base_path}/{version}/Acl/GetRowAcesHelper?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAuthProfileDeleteProfile(uuid, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/AuthProfile/DeleteProfile</td>
    <td style="padding:15px">{base_path}/{version}/AuthProfile/DeleteProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAuthProfileGetProfile(uuid, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/AuthProfile/GetProfile</td>
    <td style="padding:15px">{base_path}/{version}/AuthProfile/GetProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAuthProfileGetProfileList(callback)</td>
    <td style="padding:15px">{{baseUrl}}/AuthProfile/GetProfileList</td>
    <td style="padding:15px">{base_path}/{version}/AuthProfile/GetProfileList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlAuthProfileSaveProfile(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/AuthProfile/SaveProfile</td>
    <td style="padding:15px">{base_path}/{version}/AuthProfile/SaveProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlBrandInfo(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Brand/Info</td>
    <td style="padding:15px">{base_path}/{version}/Brand/Info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlBrandMyBrand(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Brand/MyBrand</td>
    <td style="padding:15px">{base_path}/{version}/Brand/MyBrand?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceBulkDeleteUsers(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/BulkDeleteUsers</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/BulkDeleteUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceChangeUser(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/ChangeUser</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/ChangeUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceCreateUser(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/CreateUser</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/CreateUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceCreateUserBulk(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/CreateUserBulk</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/CreateUserBulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceCreateUserQuick(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/CreateUserQuick</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/CreateUserQuick?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceCreateUsers(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/CreateUsers</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/CreateUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceDelete(id, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/Delete</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/Delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceDeleteUser(iD, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/DeleteUser</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/DeleteUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceExemptUserFromMfa(iD, timespan, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/ExemptUserFromMfa</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/ExemptUserFromMfa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceGetBulkImportWithExtAtt(callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/GetBulkImportWithExtAtt</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/GetBulkImportWithExtAtt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceGetTechSupportUser(callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/GetTechSupportUser</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/GetTechSupportUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceGetUser(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/GetUser</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/GetUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceGetUserAttributes(callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/GetUserAttributes</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/GetUserAttributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceGetUserByName(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/GetUserByName</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/GetUserByName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceGetUsers(callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/GetUsers</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/GetUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceGetUsersFromCsvFile(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/GetUsersFromCsvFile</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/GetUsersFromCsvFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceGrantAccess(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/GrantAccess</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/GrantAccess?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceRefreshToken(iD, directoryServiceUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/RefreshToken</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/RefreshToken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceRemoveAuthSource(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/RemoveAuthSource</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/RemoveAuthSource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceRemoveFederationAuthSource(federationUuid, sendInvites, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/RemoveFederationAuthSource</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/RemoveFederationAuthSource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceSetUserPicture(iD, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/SetUserPicture</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/SetUserPicture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceSetUserState(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/SetUserState</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/SetUserState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceStandardJsonResultWithPermissionCheck(action, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/StandardJsonResultWithPermissionCheck</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/StandardJsonResultWithPermissionCheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCDirectoryServiceSubmitUploadedFile(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CDirectoryService/SubmitUploadedFile</td>
    <td style="padding:15px">{base_path}/{version}/CDirectoryService/SubmitUploadedFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionCreateDynamicCollection(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/CreateDynamicCollection</td>
    <td style="padding:15px">{base_path}/{version}/Collection/CreateDynamicCollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionCreateManualCollection(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/CreateManualCollection</td>
    <td style="padding:15px">{base_path}/{version}/Collection/CreateManualCollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionDeleteCollection(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/DeleteCollection</td>
    <td style="padding:15px">{base_path}/{version}/Collection/DeleteCollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionGetBucketContents(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/GetBucketContents</td>
    <td style="padding:15px">{base_path}/{version}/Collection/GetBucketContents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionGetCollection(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/GetCollection</td>
    <td style="padding:15px">{base_path}/{version}/Collection/GetCollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionGetCollectionPermissions(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/GetCollectionPermissions</td>
    <td style="padding:15px">{base_path}/{version}/Collection/GetCollectionPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionGetCollectionReferences(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/GetCollectionReferences</td>
    <td style="padding:15px">{base_path}/{version}/Collection/GetCollectionReferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionGetCollectionRights(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/GetCollectionRights</td>
    <td style="padding:15px">{base_path}/{version}/Collection/GetCollectionRights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionGetCollectionTemplate(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/GetCollectionTemplate</td>
    <td style="padding:15px">{base_path}/{version}/Collection/GetCollectionTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionGetMembers(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/GetMembers</td>
    <td style="padding:15px">{base_path}/{version}/Collection/GetMembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionGetObjectCollections(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/GetObjectCollections</td>
    <td style="padding:15px">{base_path}/{version}/Collection/GetObjectCollections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionGetObjectCollectionsAndFilters(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/GetObjectCollectionsAndFilters</td>
    <td style="padding:15px">{base_path}/{version}/Collection/GetObjectCollectionsAndFilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionIsMember(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/IsMember</td>
    <td style="padding:15px">{base_path}/{version}/Collection/IsMember?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionSetCollectionPermissions(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/SetCollectionPermissions</td>
    <td style="padding:15px">{base_path}/{version}/Collection/SetCollectionPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionUpdateCollection(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/UpdateCollection</td>
    <td style="padding:15px">{base_path}/{version}/Collection/UpdateCollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCollectionUpdateMembersCollection(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Collection/UpdateMembersCollection</td>
    <td style="padding:15px">{base_path}/{version}/Collection/UpdateMembersCollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreAddBlockedIpRange(value, oldvalue, label, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/AddBlockedIpRange</td>
    <td style="padding:15px">{base_path}/{version}/Core/AddBlockedIpRange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreAddPremDetectRange(value, oldvalue, label, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/AddPremDetectRange</td>
    <td style="padding:15px">{base_path}/{version}/Core/AddPremDetectRange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreAssignDirectoryFileRightsToRoles(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/AssignDirectoryFileRightsToRoles</td>
    <td style="padding:15px">{base_path}/{version}/Core/AssignDirectoryFileRightsToRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreAssignDirectoryRightsToRoles(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/AssignDirectoryRightsToRoles</td>
    <td style="padding:15px">{base_path}/{version}/Core/AssignDirectoryRightsToRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreAssignFileRightsToRoles(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/AssignFileRightsToRoles</td>
    <td style="padding:15px">{base_path}/{version}/Core/AssignFileRightsToRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreCheckProxyHealth(proxyUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/CheckProxyHealth</td>
    <td style="padding:15px">{base_path}/{version}/Core/CheckProxyHealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreCreateDirectory(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/CreateDirectory</td>
    <td style="padding:15px">{base_path}/{version}/Core/CreateDirectory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreCreateTenantReportsDirectory(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/CreateTenantReportsDirectory</td>
    <td style="padding:15px">{base_path}/{version}/Core/CreateTenantReportsDirectory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreCreateUserHomeReportsDirectory(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/CreateUserHomeReportsDirectory</td>
    <td style="padding:15px">{base_path}/{version}/Core/CreateUserHomeReportsDirectory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDelBlockedIpRange(value, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DelBlockedIpRange</td>
    <td style="padding:15px">{base_path}/{version}/Core/DelBlockedIpRange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDeleteAlias(alias, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DeleteAlias</td>
    <td style="padding:15px">{base_path}/{version}/Core/DeleteAlias?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDeleteAliases(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DeleteAliases</td>
    <td style="padding:15px">{base_path}/{version}/Core/DeleteAliases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDeleteCertificate(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DeleteCertificate</td>
    <td style="padding:15px">{base_path}/{version}/Core/DeleteCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDeleteDirectory(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DeleteDirectory</td>
    <td style="padding:15px">{base_path}/{version}/Core/DeleteDirectory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDeleteFile(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DeleteFile</td>
    <td style="padding:15px">{base_path}/{version}/Core/DeleteFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDeleteFiles(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DeleteFiles</td>
    <td style="padding:15px">{base_path}/{version}/Core/DeleteFiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDeleteProxies(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DeleteProxies</td>
    <td style="padding:15px">{base_path}/{version}/Core/DeleteProxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDeleteProxy(proxyUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DeleteProxy</td>
    <td style="padding:15px">{base_path}/{version}/Core/DeleteProxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDeleteTenantConfig(key, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DeleteTenantConfig</td>
    <td style="padding:15px">{base_path}/{version}/Core/DeleteTenantConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDelPremDetectRange(value, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DelPremDetectRange</td>
    <td style="padding:15px">{base_path}/{version}/Core/DelPremDetectRange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDirectoryExists(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DirectoryExists</td>
    <td style="padding:15px">{base_path}/{version}/Core/DirectoryExists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDownloadCertificate(thumbprint, filename, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DownloadCertificate</td>
    <td style="padding:15px">{base_path}/{version}/Core/DownloadCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreDownloadFile(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/DownloadFile</td>
    <td style="padding:15px">{base_path}/{version}/Core/DownloadFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreFileExists(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/FileExists</td>
    <td style="padding:15px">{base_path}/{version}/Core/FileExists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGenerateNewProxyCode(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GenerateNewProxyCode</td>
    <td style="padding:15px">{base_path}/{version}/Core/GenerateNewProxyCode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGeneratePassword(passwordLength, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GeneratePassword</td>
    <td style="padding:15px">{base_path}/{version}/Core/GeneratePassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetAdLoginSuffixesByForest(forestRootDomain, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetAdLoginSuffixesByForest</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetAdLoginSuffixesByForest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetAdTopology(directoryServiceUuidOrDomainName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetAdTopology</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetAdTopology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetAliasesForTenant(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetAliasesForTenant</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetAliasesForTenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetAssignedAdministrativeRights(role, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetAssignedAdministrativeRights</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetAssignedAdministrativeRights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetBlockedIpRanges(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetBlockedIpRanges</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetBlockedIpRanges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetCaCertChain(filename, tenantId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetCaCertChain</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetCaCertChain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetCdsAliasesForTenant(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetCdsAliasesForTenant</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetCdsAliasesForTenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetCloudCACert(filename, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetCloudCACert</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetCloudCACert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetConnectorLog4NetConfig(proxyId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetConnectorLog4NetConfig</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetConnectorLog4NetConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetCurrentIwaJsonpUrl(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetCurrentIwaJsonpUrl</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetCurrentIwaJsonpUrl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetCurrentIwaUrl(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetCurrentIwaUrl</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetCurrentIwaUrl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetDefaultGlobalAppSigningCert(certFileName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetDefaultGlobalAppSigningCert</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetDefaultGlobalAppSigningCert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetDirectories(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetDirectories</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetDirectories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetDirectoryContents(pathParam, filter, fileext, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetDirectoryContents</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetDirectoryContents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetDirectoryFileRolesAndRights(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetDirectoryFileRolesAndRights</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetDirectoryFileRolesAndRights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetDirectoryInfo(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetDirectoryInfo</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetDirectoryInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetDirectoryRolesAndRights(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetDirectoryRolesAndRights</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetDirectoryRolesAndRights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetDirectoryServices(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetDirectoryServices</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetDirectoryServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetDomainControllersForDomain(directoryServiceUuid, domainName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetDomainControllersForDomain</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetDomainControllersForDomain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetDownloadUrls(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetDownloadUrls</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetDownloadUrls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetFileInfo(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetFileInfo</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetFileInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetFileRolesAndRights(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetFileRolesAndRights</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetFileRolesAndRights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetIwaTrustRootCert(filename, tenantId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetIwaTrustRootCert</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetIwaTrustRootCert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetLocTag(tag, locale, qualifier, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetLocTag</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetLocTag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetOUTreeContents(id, directoryServiceUuid, useCache, getAdminAccountStatus, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetOUTreeContents</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetOUTreeContents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetPremDetectRanges(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetPremDetectRanges</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetPremDetectRanges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetProxyIwaHostCertificateFile(proxyUuid, filename, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetProxyIwaHostCertificateFile</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetProxyIwaHostCertificateFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetProxyIwaSettings(proxyUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetProxyIwaSettings</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetProxyIwaSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetPurchasedLicenses(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetPurchasedLicenses</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetPurchasedLicenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetReportsDirectoryContents(pathParam, filter, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetReportsDirectoryContents</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetReportsDirectoryContents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetSupportedCultures(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetSupportedCultures</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetSupportedCultures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetTenantCACert(filename, tenantId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetTenantCACert</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetTenantCACert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetTenantConfig(key, dflt, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetTenantConfig</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetTenantConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetUniqueFileName(pathParam, name, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetUniqueFileName</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetUniqueFileName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetUserSettings(iD, settingType, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetUserSettings</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetUserSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetZsoCertAuthority(certFileName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetZsoCertAuthority</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetZsoCertAuthority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreGetZsoHostInfo(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/GetZsoHostInfo</td>
    <td style="padding:15px">{base_path}/{version}/Core/GetZsoHostInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreHandleTwilioSmsReceipt(state, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/HandleTwilioSmsReceipt</td>
    <td style="padding:15px">{base_path}/{version}/Core/HandleTwilioSmsReceipt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreIssueUserCert(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/IssueUserCert</td>
    <td style="padding:15px">{base_path}/{version}/Core/IssueUserCert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreIssueZsoUserCert(certFileName, certPass, deviceId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/IssueZsoUserCert</td>
    <td style="padding:15px">{base_path}/{version}/Core/IssueZsoUserCert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreListDirectory(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/ListDirectory</td>
    <td style="padding:15px">{base_path}/{version}/Core/ListDirectory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreLocalizeReportNameDesc(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/LocalizeReportNameDesc</td>
    <td style="padding:15px">{base_path}/{version}/Core/LocalizeReportNameDesc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreMakeFile(fileName, text, contentType, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/MakeFile</td>
    <td style="padding:15px">{base_path}/{version}/Core/MakeFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreMoveDirectory(pathParam, toPath, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/MoveDirectory</td>
    <td style="padding:15px">{base_path}/{version}/Core/MoveDirectory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreNotifyEnvironment(proxyId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/NotifyEnvironment</td>
    <td style="padding:15px">{base_path}/{version}/Core/NotifyEnvironment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreProcessProxyIwaCloudRedirect(oneTimePass, targetUrl, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/ProcessProxyIwaCloudRedirect</td>
    <td style="padding:15px">{base_path}/{version}/Core/ProcessProxyIwaCloudRedirect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreReadFile(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/ReadFile</td>
    <td style="padding:15px">{base_path}/{version}/Core/ReadFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreReIssueIwaHostCertificate(proxyUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/ReIssueIwaHostCertificate</td>
    <td style="padding:15px">{base_path}/{version}/Core/ReIssueIwaHostCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreRenameCertificate(thumbprint, newName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/RenameCertificate</td>
    <td style="padding:15px">{base_path}/{version}/Core/RenameCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreSetConnectorLog4NetConfig(proxyId, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/SetConnectorLog4NetConfig</td>
    <td style="padding:15px">{base_path}/{version}/Core/SetConnectorLog4NetConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreSetDefaultCertificate(type, thumbprint, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/SetDefaultCertificate</td>
    <td style="padding:15px">{base_path}/{version}/Core/SetDefaultCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreSetProxyIwaHostCertificateFile(proxyUuid, passwd, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/SetProxyIwaHostCertificateFile</td>
    <td style="padding:15px">{base_path}/{version}/Core/SetProxyIwaHostCertificateFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreSetProxyIwaSettings(proxyUuid, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/SetProxyIwaSettings</td>
    <td style="padding:15px">{base_path}/{version}/Core/SetProxyIwaSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreSetTenantConfig(key, value, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/SetTenantConfig</td>
    <td style="padding:15px">{base_path}/{version}/Core/SetTenantConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreStartService(proxyId, serviceName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/StartService</td>
    <td style="padding:15px">{base_path}/{version}/Core/StartService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreStopService(proxyId, serviceName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/StopService</td>
    <td style="padding:15px">{base_path}/{version}/Core/StopService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreStoreAlias(alias, domain, oldName, cdsAlias, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/StoreAlias</td>
    <td style="padding:15px">{base_path}/{version}/Core/StoreAlias?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreStoreUser(name, guid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/StoreUser</td>
    <td style="padding:15px">{base_path}/{version}/Core/StoreUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreStoreUserSetting(iD, target, settingType, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/StoreUserSetting</td>
    <td style="padding:15px">{base_path}/{version}/Core/StoreUserSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreUpdateDirectoryServicesPrecedence(precedence, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/UpdateDirectoryServicesPrecedence</td>
    <td style="padding:15px">{base_path}/{version}/Core/UpdateDirectoryServicesPrecedence?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreUpdateProxyIwaSettings(proxyUuid, password, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/UpdateProxyIwaSettings</td>
    <td style="padding:15px">{base_path}/{version}/Core/UpdateProxyIwaSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreUploadCertificate(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/UploadCertificate</td>
    <td style="padding:15px">{base_path}/{version}/Core/UploadCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCoreWriteFile(pathParam, text, content, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Core/WriteFile</td>
    <td style="padding:15px">{base_path}/{version}/Core/WriteFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCssIntegrationDzdoExecute(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CssIntegration/DzdoExecute</td>
    <td style="padding:15px">{base_path}/{version}/CssIntegration/DzdoExecute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCssIntegrationGetChallengeProfiles(callback)</td>
    <td style="padding:15px">{{baseUrl}}/CssIntegration/GetChallengeProfiles</td>
    <td style="padding:15px">{base_path}/{version}/CssIntegration/GetChallengeProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCssIntegrationMfaLogin(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CssIntegration/MfaLogin</td>
    <td style="padding:15px">{base_path}/{version}/CssIntegration/MfaLogin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlCssIntegrationSetChallengeProfiles(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/CssIntegration/SetChallengeProfiles</td>
    <td style="padding:15px">{base_path}/{version}/CssIntegration/SetChallengeProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlExtDataGetColumn(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ExtData/GetColumn</td>
    <td style="padding:15px">{base_path}/{version}/ExtData/GetColumn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlExtDataGetColumns(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ExtData/GetColumns</td>
    <td style="padding:15px">{base_path}/{version}/ExtData/GetColumns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlExtDataGetSchema(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ExtData/GetSchema</td>
    <td style="padding:15px">{base_path}/{version}/ExtData/GetSchema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlExtDataSetColumn(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ExtData/SetColumn</td>
    <td style="padding:15px">{base_path}/{version}/ExtData/SetColumn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlExtDataSetColumns(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ExtData/SetColumns</td>
    <td style="padding:15px">{base_path}/{version}/ExtData/SetColumns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlExtDataUpdateSchema(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ExtData/UpdateSchema</td>
    <td style="padding:15px">{base_path}/{version}/ExtData/UpdateSchema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlExternalCaMgmtAddCertAuthority(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ExternalCaMgmt/AddCertAuthority</td>
    <td style="padding:15px">{base_path}/{version}/ExternalCaMgmt/AddCertAuthority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlExternalCaMgmtDownloadCertAuthority(externalCaId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ExternalCaMgmt/DownloadCertAuthority</td>
    <td style="padding:15px">{base_path}/{version}/ExternalCaMgmt/DownloadCertAuthority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlExternalCaMgmtGetCertAuthorities(callback)</td>
    <td style="padding:15px">{{baseUrl}}/ExternalCaMgmt/GetCertAuthorities</td>
    <td style="padding:15px">{base_path}/{version}/ExternalCaMgmt/GetCertAuthorities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlExternalCaMgmtRemoveCertAuthority(externalCaId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ExternalCaMgmt/RemoveCertAuthority</td>
    <td style="padding:15px">{base_path}/{version}/ExternalCaMgmt/RemoveCertAuthority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlExternalCaMgmtUpdateCertAuthority(externalCaId, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ExternalCaMgmt/UpdateCertAuthority</td>
    <td style="padding:15px">{base_path}/{version}/ExternalCaMgmt/UpdateCertAuthority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationAddGlobalGroupAssertionMapping(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/AddGlobalGroupAssertionMapping</td>
    <td style="padding:15px">{base_path}/{version}/Federation/AddGlobalGroupAssertionMapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationCreateFederation(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/CreateFederation</td>
    <td style="padding:15px">{base_path}/{version}/Federation/CreateFederation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationDeleteFederation(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/DeleteFederation</td>
    <td style="padding:15px">{base_path}/{version}/Federation/DeleteFederation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationDeleteGlobalGroupAssertionMapping(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/DeleteGlobalGroupAssertionMapping</td>
    <td style="padding:15px">{base_path}/{version}/Federation/DeleteGlobalGroupAssertionMapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationFederationMetadata(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/FederationMetadata</td>
    <td style="padding:15px">{base_path}/{version}/Federation/FederationMetadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationGetFederatedGroupMembers(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/GetFederatedGroupMembers</td>
    <td style="padding:15px">{base_path}/{version}/Federation/GetFederatedGroupMembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationGetFederatedGroupsForUser(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/GetFederatedGroupsForUser</td>
    <td style="padding:15px">{base_path}/{version}/Federation/GetFederatedGroupsForUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationGetFederation(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/GetFederation</td>
    <td style="padding:15px">{base_path}/{version}/Federation/GetFederation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationGetFederationGroupAssertionMappings(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/GetFederationGroupAssertionMappings</td>
    <td style="padding:15px">{base_path}/{version}/Federation/GetFederationGroupAssertionMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationGetFederations(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/GetFederations</td>
    <td style="padding:15px">{base_path}/{version}/Federation/GetFederations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationGetFederationTypes(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/GetFederationTypes</td>
    <td style="padding:15px">{base_path}/{version}/Federation/GetFederationTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationGetGlobalFederationSettings(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/GetGlobalFederationSettings</td>
    <td style="padding:15px">{base_path}/{version}/Federation/GetGlobalFederationSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationGetGlobalGroupAssertionMappings(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/GetGlobalGroupAssertionMappings</td>
    <td style="padding:15px">{base_path}/{version}/Federation/GetGlobalGroupAssertionMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationGetGroups(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/GetGroups</td>
    <td style="padding:15px">{base_path}/{version}/Federation/GetGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationRemoveUserFromFederatedGroup(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/RemoveUserFromFederatedGroup</td>
    <td style="padding:15px">{base_path}/{version}/Federation/RemoveUserFromFederatedGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationSPSigningCertificate(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/SPSigningCertificate</td>
    <td style="padding:15px">{base_path}/{version}/Federation/SPSigningCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationSPSigningCertificateAuthority(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/SPSigningCertificateAuthority</td>
    <td style="padding:15px">{base_path}/{version}/Federation/SPSigningCertificateAuthority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationUpdateFederation(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/UpdateFederation</td>
    <td style="padding:15px">{base_path}/{version}/Federation/UpdateFederation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationUpdateFederationGroupAssertionMappings(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/UpdateFederationGroupAssertionMappings</td>
    <td style="padding:15px">{base_path}/{version}/Federation/UpdateFederationGroupAssertionMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlFederationUpdateGlobalGroupAssertionMappings(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Federation/UpdateGlobalGroupAssertionMappings</td>
    <td style="padding:15px">{base_path}/{version}/Federation/UpdateGlobalGroupAssertionMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlGoogleDirectoryAuthCallback(code, state, error, callback)</td>
    <td style="padding:15px">{{baseUrl}}/GoogleDirectory/AuthCallback</td>
    <td style="padding:15px">{base_path}/{version}/GoogleDirectory/AuthCallback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlGoogleDirectoryGetAuthTokenState(pollingToken, callback)</td>
    <td style="padding:15px">{{baseUrl}}/GoogleDirectory/GetAuthTokenState</td>
    <td style="padding:15px">{base_path}/{version}/GoogleDirectory/GetAuthTokenState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlGoogleDirectoryGetDirectoryServiceConfig(directoryServiceUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/GoogleDirectory/GetDirectoryServiceConfig</td>
    <td style="padding:15px">{base_path}/{version}/GoogleDirectory/GetDirectoryServiceConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlGoogleDirectoryGetServiceLoginUrlInfo(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/GoogleDirectory/GetServiceLoginUrlInfo</td>
    <td style="padding:15px">{base_path}/{version}/GoogleDirectory/GetServiceLoginUrlInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlGoogleDirectoryRemoveDirectoryService(directoryServiceUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/GoogleDirectory/RemoveDirectoryService</td>
    <td style="padding:15px">{base_path}/{version}/GoogleDirectory/RemoveDirectoryService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlGoogleDirectoryUpdateDirectoryServiceConfig(directoryServiceUuid, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/GoogleDirectory/UpdateDirectoryServiceConfig</td>
    <td style="padding:15px">{base_path}/{version}/GoogleDirectory/UpdateDirectoryServiceConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlJobFlowDeleteJob(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/JobFlow/DeleteJob</td>
    <td style="padding:15px">{base_path}/{version}/JobFlow/DeleteJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlJobFlowEvent(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/JobFlow/Event</td>
    <td style="padding:15px">{base_path}/{version}/JobFlow/Event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlJobFlowGetJob(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/JobFlow/GetJob</td>
    <td style="padding:15px">{base_path}/{version}/JobFlow/GetJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlJobFlowGetJobs(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/JobFlow/GetJobs</td>
    <td style="padding:15px">{base_path}/{version}/JobFlow/GetJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlJobFlowGetMyJobs(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/JobFlow/GetMyJobs</td>
    <td style="padding:15px">{base_path}/{version}/JobFlow/GetMyJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlJobFlowStartJob(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/JobFlow/StartJob</td>
    <td style="padding:15px">{base_path}/{version}/JobFlow/StartJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlJsManageGetDashboardRolesAndRights(rowKey, callback)</td>
    <td style="padding:15px">{{baseUrl}}/JsManage/GetDashboardRolesAndRights</td>
    <td style="padding:15px">{base_path}/{version}/JsManage/GetDashboardRolesAndRights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlJsManageGetReportRolesAndRights(rowKey, callback)</td>
    <td style="padding:15px">{{baseUrl}}/JsManage/GetReportRolesAndRights</td>
    <td style="padding:15px">{base_path}/{version}/JsManage/GetReportRolesAndRights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlKmipConfigure(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Kmip/Configure</td>
    <td style="padding:15px">{base_path}/{version}/Kmip/Configure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlKmipDeleteConfig(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Kmip/DeleteConfig</td>
    <td style="padding:15px">{base_path}/{version}/Kmip/DeleteConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlKmipGetConfig(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Kmip/GetConfig</td>
    <td style="padding:15px">{base_path}/{version}/Kmip/GetConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlKmipPasswordStoredRemotely(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Kmip/PasswordStoredRemotely</td>
    <td style="padding:15px">{base_path}/{version}/Kmip/PasswordStoredRemotely?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceAddLDAPDirectoryServiceConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/AddLDAPDirectoryServiceConfig</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/AddLDAPDirectoryServiceConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceDeleteLDAPDirectoryServiceConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/DeleteLDAPDirectoryServiceConfig</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/DeleteLDAPDirectoryServiceConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceGetCloudConnectors(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/GetCloudConnectors</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/GetCloudConnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceGetDirectoryServiceVersion(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/GetDirectoryServiceVersion</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/GetDirectoryServiceVersion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/GetLDAPDirectoryServiceConfig</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/GetLDAPDirectoryServiceConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceUuidByName(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/GetLDAPDirectoryServiceUuidByName</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/GetLDAPDirectoryServiceUuidByName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceGetMappableAttributeList(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/GetMappableAttributeList</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/GetMappableAttributeList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceGetPropertyToAttributeMappings(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/GetPropertyToAttributeMappings</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/GetPropertyToAttributeMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceGetScriptingPropertyToAttributeMappings(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/GetScriptingPropertyToAttributeMappings</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/GetScriptingPropertyToAttributeMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceModifyLDAPDirectoryServiceConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/ModifyLDAPDirectoryServiceConfig</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/ModifyLDAPDirectoryServiceConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceSetPropertyToAttributeMappings(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/SetPropertyToAttributeMappings</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/SetPropertyToAttributeMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceSetScriptingPropertyToAttributeMappings(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/SetScriptingPropertyToAttributeMappings</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/SetScriptingPropertyToAttributeMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceTestUserLookup(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/TestUserLookup</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/TestUserLookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLDAPDirectoryServiceVerifyLDAPDirectoryServiceConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LDAPDirectoryService/VerifyLDAPDirectoryServiceConfig</td>
    <td style="padding:15px">{base_path}/{version}/LDAPDirectoryService/VerifyLDAPDirectoryServiceConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileDeleteDevice(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/DeleteDevice</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/DeleteDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileDisableSSO(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/DisableSSO</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/DisableSSO?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileEnableSSO(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/EnableSSO</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/EnableSSO?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileGetGlobalDevicePermissions(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/GetGlobalDevicePermissions</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/GetGlobalDevicePermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileKnoxResetContainerPassword(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/KnoxResetContainerPassword</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/KnoxResetContainerPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileLockClientApp(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/LockClientApp</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/LockClientApp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileLockDevice(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/LockDevice</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/LockDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobilePingDevice(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/PingDevice</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/PingDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobilePowerOff(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/PowerOff</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/PowerOff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileReapplyDevicePolicy(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/ReapplyDevicePolicy</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/ReapplyDevicePolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileReboot(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/Reboot</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/Reboot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileRemoveDeviceProfile(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/RemoveDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/RemoveDeviceProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileResetClientAppLockPin(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/ResetClientAppLockPin</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/ResetClientAppLockPin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileSetDevicePermissions(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/SetDevicePermissions</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/SetDevicePermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileSetPrimaryDevice(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/SetPrimaryDevice</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/SetPrimaryDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileUnlockDevice(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/UnlockDevice</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/UnlockDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileUpdateDevicePolicy(deviceID, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/UpdateDevicePolicy</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/UpdateDevicePolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlMobileWipeDevice(deviceID, passcode, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Mobile/WipeDevice</td>
    <td style="padding:15px">{base_path}/{version}/Mobile/WipeDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathAddOrUpdateProfile(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/AddOrUpdateProfile</td>
    <td style="padding:15px">{base_path}/{version}/Oath/AddOrUpdateProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathCentrifyOathOtpProfileCheck(userToken, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/CentrifyOathOtpProfileCheck</td>
    <td style="padding:15px">{base_path}/{version}/Oath/CentrifyOathOtpProfileCheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathDeleteProfiles(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/DeleteProfiles</td>
    <td style="padding:15px">{base_path}/{version}/Oath/DeleteProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathGetDataFromCsvFile(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/GetDataFromCsvFile</td>
    <td style="padding:15px">{base_path}/{version}/Oath/GetDataFromCsvFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathGetImportProfileList(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/GetImportProfileList</td>
    <td style="padding:15px">{base_path}/{version}/Oath/GetImportProfileList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathGetProfileList(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/GetProfileList</td>
    <td style="padding:15px">{base_path}/{version}/Oath/GetProfileList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathGetProfileListForDevice(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/GetProfileListForDevice</td>
    <td style="padding:15px">{base_path}/{version}/Oath/GetProfileListForDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathResetCentrifyOathProfile(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/ResetCentrifyOathProfile</td>
    <td style="padding:15px">{base_path}/{version}/Oath/ResetCentrifyOathProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathResynchronizeOathToken(firstCode, secondCode, tokenId, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/ResynchronizeOathToken</td>
    <td style="padding:15px">{base_path}/{version}/Oath/ResynchronizeOathToken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathSaveProfile(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/SaveProfile</td>
    <td style="padding:15px">{base_path}/{version}/Oath/SaveProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathSetResponseParamsToEntity(oathProfileDe, showSecret, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/SetResponseParamsToEntity</td>
    <td style="padding:15px">{base_path}/{version}/Oath/SetResponseParamsToEntity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathSubmitUploadedFile(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/SubmitUploadedFile</td>
    <td style="padding:15px">{base_path}/{version}/Oath/SubmitUploadedFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathUpdateOathProfileCounter(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/UpdateOathProfileCounter</td>
    <td style="padding:15px">{base_path}/{version}/Oath/UpdateOathProfileCounter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOathValidateOtpCode(uuid, otpCode, useOathDefaults, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Oath/ValidateOtpCode</td>
    <td style="padding:15px">{base_path}/{version}/Oath/ValidateOtpCode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOAuth2Authorize(bounce, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/OAuth2/Authorize</td>
    <td style="padding:15px">{base_path}/{version}/OAuth2/Authorize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOAuth2Confirm(bounce, result, scopes, callback)</td>
    <td style="padding:15px">{{baseUrl}}/OAuth2/Confirm</td>
    <td style="padding:15px">{base_path}/{version}/OAuth2/Confirm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOAuth2EndSession(postLogoutRedirectUri, state, idTokenHint, callback)</td>
    <td style="padding:15px">{{baseUrl}}/OAuth2/EndSession</td>
    <td style="padding:15px">{base_path}/{version}/OAuth2/EndSession?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOAuth2GetMeta(serviceName, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/OAuth2/GetMeta</td>
    <td style="padding:15px">{base_path}/{version}/OAuth2/GetMeta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOAuth2Introspect(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/OAuth2/Introspect</td>
    <td style="padding:15px">{base_path}/{version}/OAuth2/Introspect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOAuth2Keys(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/OAuth2/Keys</td>
    <td style="padding:15px">{base_path}/{version}/OAuth2/Keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOAuth2Revoke(callback)</td>
    <td style="padding:15px">{{baseUrl}}/OAuth2/Revoke</td>
    <td style="padding:15px">{base_path}/{version}/OAuth2/Revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOAuth2Token(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/OAuth2/Token</td>
    <td style="padding:15px">{base_path}/{version}/OAuth2/Token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOAuth2UserInfo(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/OAuth2/UserInfo</td>
    <td style="padding:15px">{base_path}/{version}/OAuth2/UserInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRadiusGetClients(isQueryResponse, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Radius/GetClients</td>
    <td style="padding:15px">{base_path}/{version}/Radius/GetClients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRadiusGetConfig(connectorUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Radius/GetConfig</td>
    <td style="padding:15px">{base_path}/{version}/Radius/GetConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRadiusGetServers(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Radius/GetServers</td>
    <td style="padding:15px">{base_path}/{version}/Radius/GetServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRadiusGetUserIdentifierAttributes(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Radius/GetUserIdentifierAttributes</td>
    <td style="padding:15px">{base_path}/{version}/Radius/GetUserIdentifierAttributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRadiusRemoveClients(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Radius/RemoveClients</td>
    <td style="padding:15px">{base_path}/{version}/Radius/RemoveClients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRadiusRemoveServers(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Radius/RemoveServers</td>
    <td style="padding:15px">{base_path}/{version}/Radius/RemoveServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRadiusSetClient(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Radius/SetClient</td>
    <td style="padding:15px">{base_path}/{version}/Radius/SetClient?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRadiusSetConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Radius/SetConfig</td>
    <td style="padding:15px">{base_path}/{version}/Radius/SetConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRadiusSetServer(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Radius/SetServer</td>
    <td style="padding:15px">{base_path}/{version}/Radius/SetServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRegistrationCustomerInfo(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Registration/CustomerInfo</td>
    <td style="padding:15px">{base_path}/{version}/Registration/CustomerInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRegistrationRegisterNewTenant(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Registration/RegisterNewTenant</td>
    <td style="padding:15px">{base_path}/{version}/Registration/RegisterNewTenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRolesGetPagedRoleMembers(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Roles/GetPagedRoleMembers</td>
    <td style="padding:15px">{base_path}/{version}/Roles/GetPagedRoleMembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlRolesUpdateRole(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Roles/UpdateRole</td>
    <td style="padding:15px">{base_path}/{version}/Roles/UpdateRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageAddUsersAndGroupsToRole(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/AddUsersAndGroupsToRole</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/AddUsersAndGroupsToRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageDeleteApplication(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/DeleteApplication</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/DeleteApplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageDeleteRole(name, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/DeleteRole</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/DeleteRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageDeleteRoles(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/DeleteRoles</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/DeleteRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageGetAppIDByServiceName(name, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/GetAppIDByServiceName</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/GetAppIDByServiceName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageGetApplication(rowKey, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/GetApplication</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/GetApplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageGetRole(name, suppressPrincipalsList, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/GetRole</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/GetRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageGetRoleMembers(name, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/GetRoleMembers</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/GetRoleMembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageGetTemplatesAndCategories(callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/GetTemplatesAndCategories</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/GetTemplatesAndCategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageImportAppFromTemplate(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/ImportAppFromTemplate</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/ImportAppFromTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageIsApplicationAvailableInCatalog(appKey, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/IsApplicationAvailableInCatalog</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/IsApplicationAvailableInCatalog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageRemoveUsersAndGroupsFromRole(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/RemoveUsersAndGroupsFromRole</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/RemoveUsersAndGroupsFromRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageSetApplicationPermissions(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/SetApplicationPermissions</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/SetApplicationPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageStoreRole(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/StoreRole</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/StoreRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageUpdateApplicationDE(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/UpdateApplicationDE</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/UpdateApplicationDE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSaasManageUpdateRole(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SaasManage/UpdateRole</td>
    <td style="padding:15px">{base_path}/{version}/SaasManage/UpdateRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSchedulerHistoryDeleteJobHistory(jobid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SchedulerHistory/DeleteJobHistory</td>
    <td style="padding:15px">{base_path}/{version}/SchedulerHistory/DeleteJobHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSchedulerHistoryGetJobReport(jobId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SchedulerHistory/GetJobReport</td>
    <td style="padding:15px">{base_path}/{version}/SchedulerHistory/GetJobReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityAdvanceAuthentication(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/AdvanceAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/Security/AdvanceAuthentication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityAdvanceForgotUsername(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/AdvanceForgotUsername</td>
    <td style="padding:15px">{base_path}/{version}/Security/AdvanceForgotUsername?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityAmIAuthenticated(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/AmIAuthenticated</td>
    <td style="padding:15px">{base_path}/{version}/Security/AmIAuthenticated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityAnswerOOBChallenge(answer, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/AnswerOOBChallenge</td>
    <td style="padding:15px">{base_path}/{version}/Security/AnswerOOBChallenge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityChallengeUser(profileName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/ChallengeUser</td>
    <td style="padding:15px">{base_path}/{version}/Security/ChallengeUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityCleanupAuthentication(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/CleanupAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/Security/CleanupAuthentication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityDoIHaveRight(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/DoIHaveRight</td>
    <td style="padding:15px">{base_path}/{version}/Security/DoIHaveRight?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityForgotUsername(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/ForgotUsername</td>
    <td style="padding:15px">{base_path}/{version}/Security/ForgotUsername?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityGetOneTimePassword(use, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/GetOneTimePassword</td>
    <td style="padding:15px">{base_path}/{version}/Security/GetOneTimePassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityGetRiskAnalysisLevels(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/GetRiskAnalysisLevels</td>
    <td style="padding:15px">{base_path}/{version}/Security/GetRiskAnalysisLevels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityLogin(systemID, user, password, persist, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/Login</td>
    <td style="padding:15px">{base_path}/{version}/Security/Login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityLogout(redirectUrl, allowIWA, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/Logout</td>
    <td style="padding:15px">{base_path}/{version}/Security/Logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityMultiAuthLogin(user, customerId, persist, elevate, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/MultiAuthLogin</td>
    <td style="padding:15px">{base_path}/{version}/Security/MultiAuthLogin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityOnDemandChallenge(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/OnDemandChallenge</td>
    <td style="padding:15px">{base_path}/{version}/Security/OnDemandChallenge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityRefreshToken(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/RefreshToken</td>
    <td style="padding:15px">{base_path}/{version}/Security/RefreshToken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityResumeFromExtIdpAuth(extIdpAuthChallengeState, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/ResumeFromExtIdpAuth</td>
    <td style="padding:15px">{base_path}/{version}/Security/ResumeFromExtIdpAuth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityStartAuthentication(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/StartAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/Security/StartAuthentication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityStartChallenge(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/StartChallenge</td>
    <td style="padding:15px">{base_path}/{version}/Security/StartChallenge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityStartForgotUsername(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/StartForgotUsername</td>
    <td style="padding:15px">{base_path}/{version}/Security/StartForgotUsername?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityStartSocialAuthentication(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/StartSocialAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/Security/StartSocialAuthentication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecuritySubmitOathOtpCode(otpCode, userUuid, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/SubmitOathOtpCode</td>
    <td style="padding:15px">{base_path}/{version}/Security/SubmitOathOtpCode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityTaskCheck(task, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/TaskCheck</td>
    <td style="padding:15px">{base_path}/{version}/Security/TaskCheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityTaskChecks(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/TaskChecks</td>
    <td style="padding:15px">{base_path}/{version}/Security/TaskChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityTwilioPhoneChallengeCompleted(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/TwilioPhoneChallengeCompleted</td>
    <td style="padding:15px">{base_path}/{version}/Security/TwilioPhoneChallengeCompleted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityTwilioPhoneChallengeNotAnswered(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/TwilioPhoneChallengeNotAnswered</td>
    <td style="padding:15px">{base_path}/{version}/Security/TwilioPhoneChallengeNotAnswered?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSecurityWhoAmI(challenge, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Security/WhoAmI</td>
    <td style="padding:15px">{base_path}/{version}/Security/WhoAmI?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentAddEnrollmentCode(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/AddEnrollmentCode</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/AddEnrollmentCode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentDeleteEnrollmentCode(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/DeleteEnrollmentCode</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/DeleteEnrollmentCode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentDisableFeatures(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/DisableFeatures</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/DisableFeatures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentEnableFeatures(callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/EnableFeatures</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/EnableFeatures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentEnableFeaturesV2(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/EnableFeaturesV2</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/EnableFeaturesV2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentEnroll(callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/Enroll</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/Enroll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentEnrollV2(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/EnrollV2</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/EnrollV2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentGetAllEnrollmentCodes(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/GetAllEnrollmentCodes</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/GetAllEnrollmentCodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentGetCertificate(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/GetCertificate</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/GetCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentRegister(callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/Register</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/Register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentRegisterV2(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/RegisterV2</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/RegisterV2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentUnenroll(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/Unenroll</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/Unenroll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerAgentVerifyPasswordV2(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerAgent/VerifyPasswordV2</td>
    <td style="padding:15px">{base_path}/{version}/ServerAgent/VerifyPasswordV2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManageAddAccount(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/AddAccount</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/AddAccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManageCheckinPassword(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/CheckinPassword</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/CheckinPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManageCheckoutPassword(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/CheckoutPassword</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/CheckoutPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManageDeleteAccount(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/DeleteAccount</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/DeleteAccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManageExtendCheckout(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/ExtendCheckout</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/ExtendCheckout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManageGetAccountPermissions(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/GetAccountPermissions</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/GetAccountPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManageGetRetiredPassword(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/GetRetiredPassword</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/GetRetiredPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManagePreCheckout(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/PreCheckout</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/PreCheckout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManageSetAccountPermissions(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/SetAccountPermissions</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/SetAccountPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManageSetDomainPermissions(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/SetDomainPermissions</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/SetDomainPermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManageUpdateAccount(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/UpdateAccount</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/UpdateAccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlServerManageUpdatePassword(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/ServerManage/UpdatePassword</td>
    <td style="padding:15px">{base_path}/{version}/ServerManage/UpdatePassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthFacebookAuthCallback(code, state, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuth/FacebookAuthCallback</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuth/FacebookAuthCallback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthGoogleAuthCallback(code, state, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuth/GoogleAuthCallback</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuth/GoogleAuthCallback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthLinkedInAuthCallback(code, state, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuth/LinkedInAuthCallback</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuth/LinkedInAuthCallback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthMicrosoftActAuthCallback(code, state, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuth/MicrosoftActAuthCallback</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuth/MicrosoftActAuthCallback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthTwitterAuthCallback(oauthToken, oauthVerifier, state, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuth/TwitterAuthCallback</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuth/TwitterAuthCallback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthMgmtGetAllCustomConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuthMgmt/GetAllCustomConfig</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuthMgmt/GetAllCustomConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthMgmtGetApplicationClientSecret(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuthMgmt/GetApplicationClientSecret</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuthMgmt/GetApplicationClientSecret?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthMgmtGetAuthConfig(callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuthMgmt/GetAuthConfig</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuthMgmt/GetAuthConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthMgmtGetCustomConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuthMgmt/GetCustomConfig</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuthMgmt/GetCustomConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthMgmtResetAuthConfig(callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuthMgmt/ResetAuthConfig</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuthMgmt/ResetAuthConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthMgmtSetAuthConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuthMgmt/SetAuthConfig</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuthMgmt/SetAuthConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSocialAuthMgmtSetCustomConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/SocialAuthMgmt/SetCustomConfig</td>
    <td style="padding:15px">{base_path}/{version}/SocialAuthMgmt/SetCustomConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSysInfoAbout(callback)</td>
    <td style="padding:15px">{{baseUrl}}/SysInfo/About</td>
    <td style="padding:15px">{base_path}/{version}/SysInfo/About?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSysInfoDummy(callback)</td>
    <td style="padding:15px">{{baseUrl}}/SysInfo/Dummy</td>
    <td style="padding:15px">{base_path}/{version}/SysInfo/Dummy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSysInfoGetMySession(callback)</td>
    <td style="padding:15px">{{baseUrl}}/SysInfo/GetMySession</td>
    <td style="padding:15px">{base_path}/{version}/SysInfo/GetMySession?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlSysInfoVersion(callback)</td>
    <td style="padding:15px">{{baseUrl}}/SysInfo/Version</td>
    <td style="padding:15px">{base_path}/{version}/SysInfo/Version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTaskCancelJob(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Task/CancelJob</td>
    <td style="padding:15px">{base_path}/{version}/Task/CancelJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTaskCreateOneTimeJob(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Task/CreateOneTimeJob</td>
    <td style="padding:15px">{base_path}/{version}/Task/CreateOneTimeJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTaskEmailReport(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Task/EmailReport</td>
    <td style="padding:15px">{base_path}/{version}/Task/EmailReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTaskGetJobHistory(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Task/GetJobHistory</td>
    <td style="padding:15px">{base_path}/{version}/Task/GetJobHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTaskGetSingleJobHistory(jobId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Task/GetSingleJobHistory</td>
    <td style="padding:15px">{base_path}/{version}/Task/GetSingleJobHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTaskJobReport(hoursBack, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Task/JobReport</td>
    <td style="padding:15px">{base_path}/{version}/Task/JobReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantCnamesGet(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantCnames/Get</td>
    <td style="padding:15px">{base_path}/{version}/TenantCnames/Get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantCnamesGetDomainInfo(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantCnames/GetDomainInfo</td>
    <td style="padding:15px">{base_path}/{version}/TenantCnames/GetDomainInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantCnamesRegister(cnamePrefix, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantCnames/Register</td>
    <td style="padding:15px">{base_path}/{version}/TenantCnames/Register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantCnamesSetPreferred(customCname, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantCnames/SetPreferred</td>
    <td style="padding:15px">{base_path}/{version}/TenantCnames/SetPreferred?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantCnamesUiGet(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantCnames/UiGet</td>
    <td style="padding:15px">{base_path}/{version}/TenantCnames/UiGet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantCnamesUnRegister(customCname, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantCnames/UnRegister</td>
    <td style="padding:15px">{base_path}/{version}/TenantCnames/UnRegister?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigDeleteAdminSecurityQuestion(id, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/DeleteAdminSecurityQuestion</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/DeleteAdminSecurityQuestion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigDeleteAdvancedConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/DeleteAdvancedConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/DeleteAdvancedConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigGetAdminSecurityQuestion(id, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/GetAdminSecurityQuestion</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/GetAdminSecurityQuestion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigGetAdminSecurityQuestions(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/GetAdminSecurityQuestions</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/GetAdminSecurityQuestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigGetAdvancedConfig(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/GetAdvancedConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/GetAdvancedConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigGetCustomerConfig(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/GetCustomerConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/GetCustomerConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigGetEditableMailTemplates(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/GetEditableMailTemplates</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/GetEditableMailTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigGetEditableMessageTemplate(templateName, templateType, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/GetEditableMessageTemplate</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/GetEditableMessageTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigGetEditableMessageTemplates(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/GetEditableMessageTemplates</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/GetEditableMessageTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigGetGoogleKey(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/GetGoogleKey</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/GetGoogleKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigGetMobileConfig(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/GetMobileConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/GetMobileConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigGetSMTPConfig(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/GetSMTPConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/GetSMTPConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigGetTwilioConfig(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/GetTwilioConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/GetTwilioConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigResetPortalConfig(callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/ResetPortalConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/ResetPortalConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigSendTestMessageTemplate(templatePath, templateType, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/SendTestMessageTemplate</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/SendTestMessageTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigSetAdminSecurityQuestion(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/SetAdminSecurityQuestion</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/SetAdminSecurityQuestion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigSetAdvancedConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/SetAdvancedConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/SetAdvancedConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigSetCustomerConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/SetCustomerConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/SetCustomerConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigSetGoogleKey(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/SetGoogleKey</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/SetGoogleKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigSetMobileConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/SetMobileConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/SetMobileConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigSetPasswordPersistance(value, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/SetPasswordPersistance</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/SetPasswordPersistance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigSetSMTPConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/SetSMTPConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/SetSMTPConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigSetTwilioConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/SetTwilioConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/SetTwilioConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigTestSMTPConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/TestSMTPConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/TestSMTPConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlTenantConfigTestTwilioConfig(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/TenantConfig/TestTwilioConfig</td>
    <td style="padding:15px">{base_path}/{version}/TenantConfig/TestTwilioConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlU2fAnswerRegistrationChallenge(rawRegisterResponse, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/U2f/AnswerRegistrationChallenge</td>
    <td style="padding:15px">{base_path}/{version}/U2f/AnswerRegistrationChallenge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlU2fDeleteU2fDevice(keyHandle, callback)</td>
    <td style="padding:15px">{{baseUrl}}/U2f/DeleteU2fDevice</td>
    <td style="padding:15px">{base_path}/{version}/U2f/DeleteU2fDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlU2fDeleteU2fDevices(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/U2f/DeleteU2fDevices</td>
    <td style="padding:15px">{base_path}/{version}/U2f/DeleteU2fDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlU2fFacets(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/U2f/Facets</td>
    <td style="padding:15px">{base_path}/{version}/U2f/Facets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlU2fGetRegistrationChallenge(userDefinedName, authenticatortype, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/U2f/GetRegistrationChallenge</td>
    <td style="padding:15px">{base_path}/{version}/U2f/GetRegistrationChallenge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlU2fGetU2fDevices(callback)</td>
    <td style="padding:15px">{{baseUrl}}/U2f/GetU2fDevices</td>
    <td style="padding:15px">{base_path}/{version}/U2f/GetU2fDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlU2fGetU2fDevicesForUser(type, callback)</td>
    <td style="padding:15px">{{baseUrl}}/U2f/GetU2fDevicesForUser</td>
    <td style="padding:15px">{base_path}/{version}/U2f/GetU2fDevicesForUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUPRestGetAppByKey(appkey, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UPRest/GetAppByKey</td>
    <td style="padding:15px">{base_path}/{version}/UPRest/GetAppByKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUPRestGetResultantAppsForUser(userUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UPRest/GetResultantAppsForUser</td>
    <td style="padding:15px">{base_path}/{version}/UPRest/GetResultantAppsForUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUPRestGetTagsForApp(appkey, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UPRest/GetTagsForApp</td>
    <td style="padding:15px">{base_path}/{version}/UPRest/GetTagsForApp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUPRestGetUPData(force, username, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UPRest/GetUPData</td>
    <td style="padding:15px">{base_path}/{version}/UPRest/GetUPData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUPRestSetUserCredsForApp(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UPRest/SetUserCredsForApp</td>
    <td style="padding:15px">{base_path}/{version}/UPRest/SetUserCredsForApp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUPRestUpsertTagsForApp(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UPRest/UpsertTagsForApp</td>
    <td style="padding:15px">{base_path}/{version}/UPRest/UpsertTagsForApp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtAnalyzeAdaptiveMfaRisk(callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/AnalyzeAdaptiveMfaRisk</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/AnalyzeAdaptiveMfaRisk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtCanEditUserAttributes(iD, directoryServiceUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/CanEditUserAttributes</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/CanEditUserAttributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtChangeUserAttributes(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/ChangeUserAttributes</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/ChangeUserAttributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtChangeUserPassword(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/ChangeUserPassword</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/ChangeUserPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtCheckUserProfileChallenge(callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/CheckUserProfileChallenge</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/CheckUserProfileChallenge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtDirectoryServiceQuery(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/DirectoryServiceQuery</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/DirectoryServiceQuery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtGetCachedEntity(uuidOrName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/GetCachedEntity</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/GetCachedEntity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtGetCachedUser(uuidOrName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/GetCachedUser</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/GetCachedUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtGetSecurityQuestions(id, addAdminQuestions, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/GetSecurityQuestions</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/GetSecurityQuestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtGetUserAttributes(iD, directoryServiceUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/GetUserAttributes</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/GetUserAttributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtGetUserCertificateInfo(user, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/GetUserCertificateInfo</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/GetUserCertificateInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtGetUserHierarchy(iD, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/GetUserHierarchy</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/GetUserHierarchy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtGetUserInfo(iD, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/GetUserInfo</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/GetUserInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtGetUserPicture(iD, directoryServiceUuid, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/GetUserPicture</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/GetUserPicture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtGetUserPreferences(callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/GetUserPreferences</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/GetUserPreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtGetUsersRolesAndAdministrativeRights(id, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/GetUsersRolesAndAdministrativeRights</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/GetUsersRolesAndAdministrativeRights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtInviteUsers(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/InviteUsers</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/InviteUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtIsUserCloudLocked(user, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/IsUserCloudLocked</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/IsUserCloudLocked?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtIsUserLockedOutByPolicy(user, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/IsUserLockedOutByPolicy</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/IsUserLockedOutByPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtIsUserSubjectToCloudLocks(user, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/IsUserSubjectToCloudLocks</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/IsUserSubjectToCloudLocks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtRemoveUser(iD, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/RemoveUser</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/RemoveUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtRemoveUserCertificate(user, thumbprint, certType, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/RemoveUserCertificate</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/RemoveUserCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtRemoveUsers(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/RemoveUsers</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/RemoveUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtResetSecurityQuestions(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/ResetSecurityQuestions</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/ResetSecurityQuestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtResetUserPassword(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/ResetUserPassword</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/ResetUserPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtSendLoginEmail(iD, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/SendLoginEmail</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/SendLoginEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtSendLoginEmails(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/SendLoginEmails</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/SendLoginEmails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtSendSmsInvite(iD, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/SendSmsInvite</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/SendSmsInvite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtSetCloudLock(user, lockUser, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/SetCloudLock</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/SetCloudLock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtSetPhonePin(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/SetPhonePin</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/SetPhonePin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtSetSecurityQuestion(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/SetSecurityQuestion</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/SetSecurityQuestion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtUncacheUserPreferences(userUuidOrName, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/UncacheUserPreferences</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/UncacheUserPreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtUpdateSecurityQuestions(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/UpdateSecurityQuestions</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/UpdateSecurityQuestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUserMgmtUpdateUserPreferences(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/UserMgmt/UpdateUserPreferences</td>
    <td style="padding:15px">{base_path}/{version}/UserMgmt/UpdateUserPreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlVfsGetFile(pathParam, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Vfs/GetFile</td>
    <td style="padding:15px">{base_path}/{version}/Vfs/GetFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlVfsGetFileLower(pathParam, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Vfs/GetFileLower</td>
    <td style="padding:15px">{base_path}/{version}/Vfs/GetFileLower?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlZsoAuthenticateSession(sessionId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Zso/AuthenticateSession</td>
    <td style="padding:15px">{base_path}/{version}/Zso/AuthenticateSession?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlZsoCertLogin(redirectUrl, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Zso/CertLogin</td>
    <td style="padding:15px">{base_path}/{version}/Zso/CertLogin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlZsoClearMacSafariZsoCookie(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Zso/ClearMacSafariZsoCookie</td>
    <td style="padding:15px">{base_path}/{version}/Zso/ClearMacSafariZsoCookie?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlZsoIsMacSafariZsoCookieSet(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Zso/IsMacSafariZsoCookieSet</td>
    <td style="padding:15px">{base_path}/{version}/Zso/IsMacSafariZsoCookieSet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlZsoIsSessionAuthenticated(sessionId, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Zso/IsSessionAuthenticated</td>
    <td style="padding:15px">{base_path}/{version}/Zso/IsSessionAuthenticated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlZsoSetMacSafariZsoCookie(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Zso/SetMacSafariZsoCookie</td>
    <td style="padding:15px">{base_path}/{version}/Zso/SetMacSafariZsoCookie?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlHealthCheck(callback)</td>
    <td style="padding:15px">{{baseUrl}}/Health/Check</td>
    <td style="padding:15px">{base_path}/{version}/Health/Check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlHomeGetLoginData(customerid, debug, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/Home/GetLoginData</td>
    <td style="padding:15px">{base_path}/{version}/Home/GetLoginData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlLibOnlyVfsGetFile(pathParam, body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/LibOnlyVfs/GetFile</td>
    <td style="padding:15px">{base_path}/{version}/LibOnlyVfs/GetFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlOAuth2ManCreateClientToken(body, callback)</td>
    <td style="padding:15px">{{baseUrl}}/OAuth2Man/CreateClientToken</td>
    <td style="padding:15px">{base_path}/{version}/OAuth2Man/CreateClientToken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlPKILogin(redirUrl, callback)</td>
    <td style="padding:15px">{{baseUrl}}/PKI/Login</td>
    <td style="padding:15px">{base_path}/{version}/PKI/Login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">baseUrlUEvaluate(shortUrlKey, callback)</td>
    <td style="padding:15px">{{baseUrl}}/U/Evaluate</td>
    <td style="padding:15px">{base_path}/{version}/U/Evaluate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAllowedReferrer(body, callback)</td>
    <td style="padding:15px">Add Allowed Referrer</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/Configuration/AccessRestriction/AllowedReferrers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllowedReferrer(callback)</td>
    <td style="padding:15px">Get Allowed Referrer</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/Configuration/AccessRestriction/AllowedReferrers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectories(callback)</td>
    <td style="padding:15px">Get Directories</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/LDAP/Directories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDirectory(body, callback)</td>
    <td style="padding:15px">Create Directory</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/LDAP/Directories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectoryDetails(callback)</td>
    <td style="padding:15px">Get Directory Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/LDAP/Directories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDirectory(callback)</td>
    <td style="padding:15px">Delete Directory</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/LDAP/Directories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectoryMappingList(callback)</td>
    <td style="padding:15px">Get Directory Mapping List</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/LDAP/Directories/Mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDirectoryMapping(body, callback)</td>
    <td style="padding:15px">Create Directory Mapping</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/LDAP/Directories/Mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMappingDetails(callback)</td>
    <td style="padding:15px">Get Mapping Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/LDAP/Directories/Mappings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDirectoryMapping(body, callback)</td>
    <td style="padding:15px">Edit Directory Mapping</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/LDAP/Directories/Mappings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDirectoryMapping(callback)</td>
    <td style="padding:15px">Delete Directory Mapping</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/LDAP/Directories/Mappings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reorderDirectoryMappings(body, callback)</td>
    <td style="padding:15px">Reorder Directory Mappings</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Configuration/LDAP/Directories/Mappings/Reorder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitoraLiveSession(callback)</td>
    <td style="padding:15px">Monitor a Live Session</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/LiveSessions/Monitor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resumeaSuspendedSession(body, callback)</td>
    <td style="padding:15px">Resume a Suspended Session</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/LiveSessions/Resume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">suspendanActiveSession(body, callback)</td>
    <td style="padding:15px">Suspend an Active Session</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/LiveSessions/Suspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">terminateanActiveSession(body, callback)</td>
    <td style="padding:15px">Terminate an Active Session</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/LiveSessions/Terminate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecordings(limit, sort, offset, search, safe, fromTime, toTime, activities, callback)</td>
    <td style="padding:15px">Get Recordings</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Recordings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecordingDetails(callback)</td>
    <td style="padding:15px">Get Recording Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Recordings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecordingActivities(callback)</td>
    <td style="padding:15px">Get Recording Activities</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Recordings/activities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecordingProperties(callback)</td>
    <td style="padding:15px">Get Recording Properties</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Recordings/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">playRecording(callback)</td>
    <td style="padding:15px">Play Recording</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/recordings/Play?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLiveSessions(limit, sort, offset, search, safe, fromTime, toTime, activities, callback)</td>
    <td style="padding:15px">Get Live Sessions</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/LiveSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLiveSessionDetails(callback)</td>
    <td style="padding:15px">Get Live Session Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/LiveSessions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLiveSessionActivities(callback)</td>
    <td style="padding:15px">Get Live Session Activities</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/LiveSessions/activities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLiveSessionProperties(callback)</td>
    <td style="padding:15px">Get Live Session Properties</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/LiveSessions/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOnboardingRule(callback)</td>
    <td style="padding:15px">Get Onboarding Rule</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/AutomaticOnboardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOnboardingRule(body, callback)</td>
    <td style="padding:15px">Add Onboarding Rule</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/AutomaticOnboardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOnboardingRule(id, body, callback)</td>
    <td style="padding:15px">Update Onboarding Rule</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/AutomaticOnboardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOnboardingRule(id, callback)</td>
    <td style="padding:15px">Delete Onboarding Rule</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/AutomaticOnboardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccountACL(aCLAddress, aCLUserName, aCLPolicyID, callback)</td>
    <td style="padding:15px">List Account/ACL</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Account/{pathv1}/PrivilegedCommands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAccountACL(body, aCLAddress, aCLUserName, aCLPolicyID, callback)</td>
    <td style="padding:15px">Add Account/ACL</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Account/{pathv1}/PrivilegedCommands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountACL(id, aCLAddress, aCLUserName, aCLPolicyID, callback)</td>
    <td style="padding:15px">Delete Account/ACL</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Account/{pathv1}/PrivilegedCommands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTargetPlatforms(callback)</td>
    <td style="padding:15px">Get Target Platforms</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/targets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateTargetPlatform(callback)</td>
    <td style="padding:15px">Activate Target Platform</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/targets/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateTargetPlatform(callback)</td>
    <td style="padding:15px">Deactivate Target Platform</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/targets/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">duplicateTargetPlatforms(body, callback)</td>
    <td style="padding:15px">Duplicate Target Platforms</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/targets/duplicate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTargetPlatform(callback)</td>
    <td style="padding:15px">Delete Target Platform</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/targets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDependentPlatforms(search, callback)</td>
    <td style="padding:15px">Get Dependent Platforms</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/dependents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">duplicateDependentPlatforms(body, callback)</td>
    <td style="padding:15px">Duplicate Dependent Platforms</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/dependent/duplicate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDependentPlatform(callback)</td>
    <td style="padding:15px">Delete Dependent Platform</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/dependents/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupPlatforms(search, callback)</td>
    <td style="padding:15px">Get Group Platforms</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateGroupPlatform(callback)</td>
    <td style="padding:15px">Activate Group Platform</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/groups/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateGroupPlatform(callback)</td>
    <td style="padding:15px">Deactivate Group Platform</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/groups/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">duplicateGroupPlatforms(body, callback)</td>
    <td style="padding:15px">Duplicate Group Platforms</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/groups/duplicate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupPlatform(callback)</td>
    <td style="padding:15px">Delete Group Platform</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRotationalGroupPlatforms(search, callback)</td>
    <td style="padding:15px">Get Rotational Group Platforms</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/rotationalGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateRotationalGroupPlatform(callback)</td>
    <td style="padding:15px">Activate Rotational Group Platform</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/rotationalGroups/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateRotationalGroupPlatform(callback)</td>
    <td style="padding:15px">Deactivate Rotational Group Platform</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/rotationalGroups/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">duplicateRotationalGroupPlatforms(body, callback)</td>
    <td style="padding:15px">Duplicate Rotational Group Platforms</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/rotationalGroups/duplicate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRotationalGroupPlatform(callback)</td>
    <td style="padding:15px">Delete Rotational Group Platform</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/platforms/rotationalGroups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlatformDetails(callback)</td>
    <td style="padding:15px">Get Platform Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Platforms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlatforms(active, platformType, platformName, callback)</td>
    <td style="padding:15px">Get Platforms</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Platforms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportPlatform(callback)</td>
    <td style="padding:15px">Export Platform</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Platforms/Export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importPlatform(body, callback)</td>
    <td style="padding:15px">Import Platform</td>
    <td style="padding:15px">{base_path}/{version}/API/Platforms/Import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installPTA(callback)</td>
    <td style="padding:15px">Install PTA</td>
    <td style="padding:15px">{base_path}/{version}/installer/api/installation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serverEncryptionKey(callback)</td>
    <td style="padding:15px">Server Encryption Key</td>
    <td style="padding:15px">{base_path}/{version}/installer/api/encryptionkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pTAServerAuthentication(callback)</td>
    <td style="padding:15px">PTA Server Authentication</td>
    <td style="padding:15px">{base_path}/{version}/installer/api/getauthtoken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityEvents(callback)</td>
    <td style="padding:15px">Get Security Events</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/pta/API/Events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecuritySettings(callback)</td>
    <td style="padding:15px">Get Security Settings</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/pta/API/Settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRiskyCommandsRule(body, callback)</td>
    <td style="padding:15px">Add Risky Commands Rule</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/pta/API/Settings/RiskyActivity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRiskyCommandsRule(body, callback)</td>
    <td style="padding:15px">Update Risky Commands Rule</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/pta/API/Settings/RiskyActivity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecurityRemediationSettings(callback)</td>
    <td style="padding:15px">Update Security Remediation Settings</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/pta/API/Settings/AutomaticRemediations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecurityEvent(body, callback)</td>
    <td style="padding:15px">Update Security Event</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/pta/API/Events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPTAReplicationStatus(callback)</td>
    <td style="padding:15px">Get PTA Replication Status</td>
    <td style="padding:15px">{base_path}/{version}/api/monitoring?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncomingRequestList(onlywaiting, expired, callback)</td>
    <td style="padding:15px">Get Incoming Request List</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/IncomingRequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetailsofaRequestforConfirmation(callback)</td>
    <td style="padding:15px">Get Details of a Request for Confirmation</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/IncomingRequests/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmRequest(body, callback)</td>
    <td style="padding:15px">Confirm Request</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/IncomingRequests/Confirm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rejectRequest(body, callback)</td>
    <td style="padding:15px">Reject Request</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/IncomingRequests/Reject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMyRequests(onlywaiting, expired, callback)</td>
    <td style="padding:15px">Get My Requests</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/MyRequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createaRequest(body, callback)</td>
    <td style="padding:15px">Create a Request</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/MyRequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetailsofMyRequests(callback)</td>
    <td style="padding:15px">Get Details of My Requests</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/MyRequests/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMyRequest(body, callback)</td>
    <td style="padding:15px">Delete My Request</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/MyRequests/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logo(type, callback)</td>
    <td style="padding:15px">Logo</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Logo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">server(callback)</td>
    <td style="padding:15px">Server</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verify(callback)</td>
    <td style="padding:15px">Verify</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllConnectionComponents(callback)</td>
    <td style="padding:15px">Get All Connection Components</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/PSM/Connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPSMServers(callback)</td>
    <td style="padding:15px">Get All PSM Servers</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/PSM/Servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionManagementPolicyofPlatform(callback)</td>
    <td style="padding:15px">Get Session Management Policy of Platform</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/Platforms/Targets/PrivilegedSessionManagement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importConnectionComponent(body, callback)</td>
    <td style="padding:15px">Import Connection Component</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/ConnectionComponents/Import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSessionManagementPolicyofPlatform(body, callback)</td>
    <td style="padding:15px">Update Session Management Policy of Platform</td>
    <td style="padding:15px">{base_path}/{version}/passwordvault/api/Platforms/Targets/PrivilegedSessionManagement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfiguration(callback)</td>
    <td style="padding:15px">Get Configuration</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/settings/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPTASystemHealth(match, callback)</td>
    <td style="padding:15px">Get PTA System Health</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/federate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemDetails(callback)</td>
    <td style="padding:15px">System Details</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/ComponentsMonitoringDetails/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemSummary(callback)</td>
    <td style="padding:15px">System Summary</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/API/ComponentsMonitoringSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPublicSSHKey(callback)</td>
    <td style="padding:15px">Get Public SSH Key</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Users/Svc_CybrAutomation/AuthenticationMethods/SSHKeyAuthentication/AuthorizedKeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addaPublicSSHKey(body, callback)</td>
    <td style="padding:15px">Add a Public SSH Key</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Users/Svc_CybrAutomation/AuthenticationMethods/SSHKeyAuthentication/AuthorizedKeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePublicSSHKey(callback)</td>
    <td style="padding:15px">Delete Public SSH Key</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/WebServices/PIMServices.svc/Users/Svc_CybrAutomation/AuthenticationMethods/SSHKeyAuthentication/AuthorizedKeys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateanMFACachingSSHKey(body, callback)</td>
    <td style="padding:15px">Generate an MFA Caching SSH Key</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users/Secret/SSHKeys/Cache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateanMFACachingSSHKeyforAnotherUser(body, callback)</td>
    <td style="padding:15px">Generate an MFA Caching SSH Key for Another User</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users/Svc_CybrAutomation/Secrets/SSHKeys/Cache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteanMFACachingSSHKeyforAnotherUser(callback)</td>
    <td style="padding:15px">Delete an MFA Caching SSH Key for Another User</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users/Svc_CybrAutomation/Secrets/SSHKeys/Cache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllMFACachingSSHKeys(callback)</td>
    <td style="padding:15px">Delete All MFA Caching SSH Keys</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users/Secrets/SSHKeys/ClearCache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteanMFACachingSSHkey(callback)</td>
    <td style="padding:15px">Delete an MFA Caching SSH key</td>
    <td style="padding:15px">{base_path}/{version}/PasswordVault/api/Users/Secrets/SSHKeys/Cache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
