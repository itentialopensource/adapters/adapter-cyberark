
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:23PM

See merge request itentialopensource/adapters/adapter-cyberark!13

---

## 0.4.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cyberark!11

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:34PM

See merge request itentialopensource/adapters/adapter-cyberark!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:47PM

See merge request itentialopensource/adapters/adapter-cyberark!9

---

## 0.4.0 [05-14-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-cyberark!8

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_13:43PM

See merge request itentialopensource/adapters/security/adapter-cyberark!7

---

## 0.3.3 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/security/adapter-cyberark!6

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_16:15PM

See merge request itentialopensource/adapters/security/adapter-cyberark!5

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:49AM

See merge request itentialopensource/adapters/security/adapter-cyberark!4

---

## 0.3.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-cyberark!3

---

## 0.2.0 [05-29-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-cyberark!1

---

## 0.1.1 [08-02-2021]

- Initial Commit

See commit 542dd2a

---
