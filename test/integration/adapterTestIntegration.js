/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cyberark',
      type: 'Cyberark',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Cyberark = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Cyberark Adapter Test', () => {
  describe('Cyberark Class Tests', () => {
    const a = new Cyberark(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const v2APIChangePasswordImmediatelyBodyParam = {
      ChangeEntireGroup: false
    };
    describe('#changePasswordImmediately - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changePasswordImmediately(v2APIChangePasswordImmediatelyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'changePasswordImmediately', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkInanExclusiveAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkInanExclusiveAccount((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'checkInanExclusiveAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIConnectUsingPSMBodyParam = {};
    describe('#connectUsingPSM - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.connectUsingPSM(v2APIConnectUsingPSMBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'connectUsingPSM', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reconcilePassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reconcilePassword((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'reconcilePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verifyPassword((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'verifyPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APILogonCyberArkAuthenticationBodyParam = {
      username: 'string',
      password: samProps.authentication.password,
      concurrentSession: 'string'
    };
    describe('#logonCyberArkAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logonCyberArkAuthentication(v2APILogonCyberArkAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'logonCyberArkAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APILogonLDAPAuthenticationBodyParam = {
      username: 'string',
      password: samProps.authentication.password,
      concurrentSession: 'string'
    };
    describe('#logonLDAPAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logonLDAPAuthentication(v2APILogonLDAPAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'logonLDAPAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logoff - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.logoff((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.LogoffUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'logoff', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APILogonRADIUSAuthenticationBodyParam = {
      Username: 'string',
      Password: 'string',
      concurrentSessions: 'string'
    };
    describe('#logonRADIUSAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logonRADIUSAuthentication(v2APILogonRADIUSAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'logonRADIUSAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APILogonWindowsAuthenticationBodyParam = {
      username: 'string',
      password: samProps.authentication.password,
      concurrentSessions: 'string'
    };
    describe('#logonWindowsAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logonWindowsAuthentication(v2APILogonWindowsAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'logonWindowsAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIAddPendingAccountBodyParam = {
      pendingAccount: {
        UserName: '<user name>',
        Address: '<address>',
        AccountDiscoveryDate: '<YYYY-MM-DDThh:mm:ssZ>',
        AccountEnabled: '<enabled/disabled>',
        AccountOSGroups: '<group name>',
        AccountType: '<domain/local>',
        Domain: '<domain name>',
        PasswordNeverExpires: '<true/false>',
        OSVersion: '<OS version>',
        OU: '<OU>',
        AccountCategory: '<Privileged/Non-privileged>',
        UserDisplayName: '<user display name>',
        AccountDescription: '<description>',
        GID: '<GID>',
        UID: '<UID>',
        OSType: '<Windows/Unix>',
        DiscoveryPlatformType: '<platform name>',
        MachineOSFamily: '<workstation/server>',
        LastLogonDate: '<YYYY-MM-DDThh:mm:ssZ>',
        LastPasswordSetDate: '<YYYY-MM-DDThh:mm:ssZ>',
        AccountExpirationDate: '<YYYY-MM-DDThh:mm:ssZ>',
        AccountCategoryCriteria: '<criteria>'
      }
    };
    describe('#addPendingAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPendingAccount(v2APIAddPendingAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'addPendingAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIAddAccountBodyParam = {
      name: 'string',
      address: 'string',
      userName: 'string',
      platformId: 'string',
      safeName: 'string',
      secretType: 'string',
      secret: 'string',
      platformAccountProperties: {
        LogonDomain: 'string',
        Port: 'integer'
      },
      secretManagement: {
        automaticManagementEnabled: true,
        manualManagementReason: 'string'
      },
      remoteMachinesAccess: {
        remoteMachines: 'string',
        accessRestrictedToRemoteMachines: true
      }
    };
    describe('#addAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addAccount(v2APIAddAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.address);
                assert.equal('string', data.response.userName);
                assert.equal('string', data.response.platformId);
                assert.equal('string', data.response.safeName);
                assert.equal('string', data.response.secretType);
                assert.equal('object', typeof data.response.platformAccountProperties);
                assert.equal('object', typeof data.response.secretManagement);
                assert.equal(3, data.response.createdTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'addAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APILinkanAccountBodyParam = {
      Name: 'string',
      ExtraPassID: 'string',
      Folder: 'string'
    };
    describe('#linkanAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.linkanAccount(v2APILinkanAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'linkanAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIGetPasswordValueBodyParam = {};
    describe('#getPasswordValue - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPasswordValue(v2APIGetPasswordValueBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getPasswordValue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIChangePasswordintheVaultOnlyBodyParam = {
      ChangeEntireGroup: false,
      NewCredentials: 'string'
    };
    describe('#changePasswordintheVaultOnly - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changePasswordintheVaultOnly(v2APIChangePasswordintheVaultOnlyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'changePasswordintheVaultOnly', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJustinTimeAccess - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getJustinTimeAccess((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getJustinTimeAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIAdHocConnectthroughPSMBodyParam = {
      UserName: 'string',
      Secret: 'string',
      Address: 'string',
      PlatformId: 'string',
      extraFields: {},
      PSMConnectPrerequisites: {
        ConnectionComponent: '<Connection Component ID>',
        ConnectionType: '<RDPFile or PSMGW>'
      }
    };
    describe('#adHocConnectthroughPSM - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocConnectthroughPSM(v2APIAdHocConnectthroughPSMBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'adHocConnectthroughPSM', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIAddDiscoveredAccountsV108BodyParam = {};
    describe('#addDiscoveredAccountsV108 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addDiscoveredAccountsV108(v2APIAddDiscoveredAccountsV108BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'addDiscoveredAccountsV108', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIAddSafeBodyParam = {
      SafeName: 'string',
      Description: 'string',
      OLACEnabled: true,
      ManagingCPM: 'string',
      NumberOfVersionsRetention: 'string',
      NumberOfDaysRetention: 3,
      AutoPurgeEnabled: true,
      Location: 'string'
    };
    describe('#addSafe - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSafe(v2APIAddSafeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'addSafe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIAddSafeMemberBodyParam = {
      memberName: 'string',
      searchIn: 'string',
      membershipExpirationDate: 9,
      permissions: {
        useAccounts: false,
        retrieveAccounts: false,
        listAccounts: false,
        addAccounts: false,
        updateAccountContent: false,
        updateAccountProperties: false,
        initiateCPMAccountManagementOperations: false,
        specifyNextAccountContent: false,
        renameAccounts: false,
        deleteAccounts: false,
        unlockAccounts: false,
        manageSafe: false,
        manageSafeMembers: false,
        backupSafe: false,
        viewAuditLog: false,
        viewSafeMembers: false,
        accessWithoutConfirmation: false,
        createFolders: false,
        deleteFolders: false,
        moveAccountsAndFolders: false,
        requestsAuthorizationLevel1: false,
        requestsAuthorizationLevel2: false
      }
    };
    describe('#addSafeMember - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSafeMember(v2APIAddSafeMemberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'addSafeMember', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APICreateGroupBodyParam = {
      groupName: 'string',
      description: 'string',
      location: 'string'
    };
    describe('#createGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createGroup(v2APICreateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.groupType);
                assert.equal('string', data.response.groupName);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.location);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'createGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIAddMembertoGroupBodyParam = {
      memberId: 'string',
      memberType: 'string',
      domainName: 'string'
    };
    describe('#addMembertoGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addMembertoGroup(v2APIAddMembertoGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'addMembertoGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIAddUserBodyParam = {
      username: 'string',
      userType: 'string',
      initialPassword: 'string',
      authenticationMethod: [
        'string'
      ],
      location: 'string',
      unAuthorizedInterfaces: [
        'string'
      ],
      expiryDate: 4,
      vaultAuthorization: [
        'string'
      ],
      enableUser: false,
      changePassOnNextLogon: false,
      passwordNeverExpires: true,
      distinguishedName: 'string',
      description: 'string',
      businessAddress: {
        workStreet: '',
        workCity: '',
        workState: '',
        workZip: '',
        workCountry: ''
      },
      internet: {
        homePage: '',
        homeEmail: '',
        businessEmail: '',
        otherEmail: ''
      },
      phones: {
        homeNumber: '',
        businessNumber: '',
        cellularNumber: '',
        faxNumber: '',
        pagerNumber: ''
      },
      personalDetails: {
        street: '',
        city: '',
        state: '',
        zip: '',
        country: '',
        title: '',
        organization: '',
        department: '',
        profession: '',
        firstName: '',
        middleName: '',
        lastName: ''
      }
    };
    describe('#addUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addUser(v2APIAddUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'addUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateUser((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'activateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIResetUserPasswordBodyParam = {
      id: 'string',
      newPassword: 'string'
    };
    describe('#resetUserPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resetUserPassword(v2APIResetUserPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'resetUserPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIChangePasswordSetNextPasswordBodyParam = {
      ChangeImmediately: true,
      NewCredentials: 'string'
    };
    describe('#changePasswordSetNextPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changePasswordSetNextPassword(v2APIChangePasswordSetNextPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'changePasswordSetNextPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafeAccountGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSafeAccountGroups((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getSafeAccountGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountActivity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountActivity((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.GetAccountActivitiesResult));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getAccountActivity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIChangePasswordBodyParam = {};
    describe('#changePassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changePassword(v2APIChangePasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'changePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APISearch = 'fakedata';
    const v2APISearchType = 'fakedata';
    const v2APISort = 'fakedata';
    const v2APIOffset = 555;
    const v2APILimit = 555;
    const v2APIFilter = 'fakedata';
    describe('#getAccounts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAccounts(v2APISearch, v2APISearchType, v2APISort, v2APIOffset, v2APILimit, v2APIFilter, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIUpdateAccountBodyParam = [
      {
        op: 'replace',
        path: '/address',
        value: 'NewAddress'
      }
    ];
    describe('#updateAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAccount(v2APIUpdateAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'updateAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountDetails((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.address);
                assert.equal('string', data.response.userName);
                assert.equal('string', data.response.platformId);
                assert.equal('string', data.response.safeName);
                assert.equal('string', data.response.secretType);
                assert.equal('object', typeof data.response.secretManagement);
                assert.equal(6, data.response.createdTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getAccountDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#extendedAccountOverview - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.extendedAccountOverview((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'extendedAccountOverview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafebyPlatformID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSafebyPlatformID((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal(7, data.response.count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getSafebyPlatformID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSafes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSafes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.GetSafesResult));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getAllSafes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafeDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSafeDetails((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.GetSafeResult);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getSafeDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafeMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSafeMembers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getSafeMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroups(v2APIFilter, v2APISearch, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal(10, data.response.count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIUpdateGroupBodyParam = {
      groupName: 'string'
    };
    describe('#updateGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateGroup(v2APIUpdateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'updateGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIExtendedDetails = true;
    describe('#getUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsers(v2APIFilter, v2APISearch, v2APIExtendedDetails, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Users));
                assert.equal(1, data.response.Total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v2APIUpdateUserBodyParam = {
      enableUser: false,
      changePassOnNextLogon: true,
      expiryDate: 10,
      suspended: true,
      unAuthorizedInterfaces: [
        'string'
      ],
      authenticationMethod: [
        'string'
      ],
      passwordNeverExpires: true,
      distinguishedName: 'string',
      description: 'string',
      businessAddress: {
        workStreet: '',
        workCity: '',
        workState: '',
        workZip: '',
        workCountry: ''
      },
      internet: {
        homePage: '',
        homeEmail: '',
        businessEmail: '',
        otherEmail: ''
      },
      phones: {
        homeNumber: '',
        businessNumber: '',
        cellularNumber: '',
        faxNumber: '',
        pagerNumber: ''
      },
      personalDetails: {
        street: '',
        city: '',
        state: '',
        zip: '',
        country: '',
        title: '',
        organization: '',
        department: '',
        profession: '',
        firstName: '',
        middleName: '',
        lastName: ''
      },
      id: 2,
      username: 'string',
      source: 'string',
      userType: 'string',
      componentUser: true,
      vaultAuthorization: [
        'string'
      ],
      location: 'string'
    };
    describe('#updateUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUser(v2APIUpdateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'updateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserDetails((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.enableUser);
                assert.equal(false, data.response.changePassOnNextLogon);
                assert.equal(6, data.response.expiryDate);
                assert.equal(true, data.response.suspended);
                assert.equal(2, data.response.lastSuccessfulLoginDate);
                assert.equal(true, Array.isArray(data.response.unAuthorizedInterfaces));
                assert.equal(true, Array.isArray(data.response.authenticationMethod));
                assert.equal(false, data.response.passwordNeverExpires);
                assert.equal('string', data.response.distinguishedName);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.businessAddress);
                assert.equal('object', typeof data.response.internet);
                assert.equal('object', typeof data.response.phones);
                assert.equal('object', typeof data.response.personalDetails);
                assert.equal(true, Array.isArray(data.response.groupsMembership));
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.source);
                assert.equal('string', data.response.userType);
                assert.equal(true, data.response.componentUser);
                assert.equal(true, Array.isArray(data.response.vaultAuthorization));
                assert.equal('string', data.response.location);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getUserDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDiscoveredAccounts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDiscoveredAccounts(v2APIFilter, v2APISearch, v2APISearchType, v2APIOffset, v2APILimit, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getDiscoveredAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDiscoveredAccountDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDiscoveredAccountDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'getDiscoveredAccountDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIChangeCredentialsintheVaultV10BodyParam = {
      ChangeCredsForGroup: 'string',
      AutoGenerate: 'string',
      NewCredentials: 'string'
    };
    describe('#changeCredentialsintheVaultV10 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeCredentialsintheVaultV10(v1APIChangeCredentialsintheVaultV10BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'changeCredentialsintheVaultV10', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIChangeCredentialsandSetNextPasswordV10BodyParam = {
      ChangeImmediately: 'string',
      NewCredentials: 'string'
    };
    describe('#changeCredentialsandSetNextPasswordV10 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeCredentialsandSetNextPasswordV10(v1APIChangeCredentialsandSetNextPasswordV10BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'changeCredentialsandSetNextPasswordV10', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIAddAccountV90BodyParam = {
      account: {
        safe: 'D-APP-Ansible',
        platformID: 'WinDomain',
        address: 'example.com',
        accountName: 'AccountName',
        password: samProps.authentication.password,
        username: 'Svc_CybrAutomation',
        disableAutoMgmt: 'false',
        disableAutoMgmtReason: 'N/A',
        groupName: '',
        groupPlatformID: '',
        properties: [
          {
            Key: 'Port',
            Value: '<port>'
          },
          {
            Key: 'ParamName',
            Value: 'Parameter value'
          }
        ]
      }
    };
    describe('#addAccountV90 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAccountV90(v1APIAddAccountV90BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'addAccountV90', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIVerifyCredentialsV97V995BodyParam = {};
    describe('#verifyCredentialsV97V995 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verifyCredentialsV97V995(v1APIVerifyCredentialsV97V995BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'verifyCredentialsV97V995', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIPostAddMembertoGroupBodyParam = {
      UserName: 'string'
    };
    describe('#postAddMembertoGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAddMembertoGroup(v1APIPostAddMembertoGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'postAddMembertoGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIPostAddSafeBodyParam = {};
    describe('#postAddSafe - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAddSafe(v1APIPostAddSafeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'postAddSafe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIPostAddUserBodyParam = {};
    describe('#postAddUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAddUser(v1APIPostAddUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'postAddUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogoff - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postLogoff((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'postLogoff', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APILogonBodyParam = {
      username: 'string',
      password: samProps.authentication.password,
      connectionNumber: 'string'
    };
    describe('#logon - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logon(v1APILogonBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'logon', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIKeywords = 'fakedata';
    const v1APISafe = 'fakedata';
    describe('#getAccountDetailsV93 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountDetailsV93(v1APIKeywords, v1APISafe, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.Count);
                assert.equal(true, Array.isArray(data.response.accounts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'getAccountDetailsV93', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIUpdateAccountDetailsV95BodyParam = {
      Accounts: {
        Folder: 'Root',
        AccountName: '',
        DeviceType: 'Operating System',
        PlatformID: 'WinDomain',
        Address: 'example.com',
        UserName: 'Svc_CybrAutomation',
        GroupName: '',
        GroupPlatformID: '',
        Properties: [
          {
            Key: 'Notes',
            Value: 'Test User for CyberArk'
          },
          {
            Key: 'Ticket Number',
            Value: 'CHG100001'
          },
          {
            Key: 'ParamName',
            Value: 'Parameter value'
          }
        ]
      }
    };
    describe('#updateAccountDetailsV95 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAccountDetailsV95(v1APIUpdateAccountDetailsV95BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'updateAccountDetailsV95', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountValueV97910 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAccountValueV97910((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'getAccountValueV97910', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSafes1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSafes1((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.GetSafesResult));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'getAllSafes1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIUpdateSafeBodyParam = {};
    describe('#updateSafe - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSafe(v1APIUpdateSafeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'updateSafe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafeDetails1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSafeDetails1((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.GetSafeResult);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'getSafeDetails1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafeMembers1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSafeMembers1((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'getSafeMembers1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIUpdateSafeMemberBodyParam = {};
    describe('#updateSafeMember - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSafeMember(v1APIUpdateSafeMemberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'updateSafeMember', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loggedOnUserDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loggedOnUserDetails((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.AgentUser);
                assert.equal(true, data.response.Disabled);
                assert.equal('string', data.response.Email);
                assert.equal(true, data.response.Expired);
                assert.equal('string', data.response.ExpiryDate);
                assert.equal('string', data.response.FirstName);
                assert.equal('string', data.response.LastName);
                assert.equal('string', data.response.Location);
                assert.equal('string', data.response.Source);
                assert.equal(false, data.response.Suspended);
                assert.equal('string', data.response.UserName);
                assert.equal('string', data.response.UserTypeName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'loggedOnUserDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putActivateUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putActivateUser((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'putActivateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const v1APIPutUpdateUserBodyParam = {};
    describe('#putUpdateUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putUpdateUser(v1APIPutUpdateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'putUpdateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserDetails1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserDetails1((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.AgentUser);
                assert.equal(true, data.response.Disabled);
                assert.equal('string', data.response.Email);
                assert.equal(false, data.response.Expired);
                assert.equal('string', data.response.ExpiryDate);
                assert.equal('string', data.response.FirstName);
                assert.equal('string', data.response.LastName);
                assert.equal('string', data.response.Location);
                assert.equal('string', data.response.Source);
                assert.equal(true, data.response.Suspended);
                assert.equal('string', data.response.UserName);
                assert.equal('string', data.response.UserTypeName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'getUserDetails1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountGroupsAddAccountGroupBodyParam = {
      GroupName: 'string',
      GroupPlatform: 'string',
      Safe: 'string'
    };
    describe('#addAccountGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAccountGroup(accountGroupsAddAccountGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountGroups', 'addAccountGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountGroupsAddAccounttoAccountGroupBodyParam = {
      AccountID: 'string'
    };
    describe('#addAccounttoAccountGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAccounttoAccountGroup(accountGroupsAddAccounttoAccountGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountGroups', 'addAccounttoAccountGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountGroupsSafe = 'fakedata';
    describe('#getAccountGroupbySafe - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAccountGroupbySafe(accountGroupsSafe, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountGroups', 'getAccountGroupbySafe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountGroupMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAccountGroupMembers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountGroups', 'getAccountGroupMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bulkUploadOfAccountsCreateBulkUploadofAccountsBodyParam = {
      source: 'string',
      accountsList: [
        {
          uploadIndex: '1',
          username: 'JohnDoe',
          address: '192.0.2.0',
          platformId: 'WinDomain',
          safeName: 'WinDomainSafe',
          secret: '123456',
          platformAccountProperties: {
            port: '111'
          },
          secretManagement: {
            automaticManagementEnabled: true,
            manualManagementReason: ''
          },
          remoteMachinesAccess: {
            accessRestrictedToRemoteMachines: true,
            remoteMachines: 'example.com'
          },
          groupName: 'DomainGroup'
        }
      ]
    };
    describe('#createBulkUploadofAccounts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBulkUploadofAccounts(bulkUploadOfAccountsCreateBulkUploadofAccountsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BulkUploadOfAccounts', 'createBulkUploadofAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllBulkAccountUploadsforUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllBulkAccountUploadsforUser((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BulkUploadOfAccounts', 'getAllBulkAccountUploadsforUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkAccountUploadResult - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBulkAccountUploadResult((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BulkUploadOfAccounts', 'getBulkAccountUploadResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationsAddApplicationBodyParam = {
      application: {
        AppID: 'Ansible',
        Description: 'Testing DevOps Deployments with CyberArk',
        Location: '/Applications',
        AccessPermittedFrom: 0,
        AccessPermittedTo: 23,
        ExpirationDate: '',
        Disabled: 'No',
        BusinessOwnerFName: 'John',
        BusinessOwnerLName: 'Doe',
        BusinessOwnerEmail: 'John.Doe@CyberArk.com',
        BusinessOwnerPhone: '555-555-1212'
      }
    };
    describe('#addApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addApplication(applicationsAddApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Applications', 'addApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationsAddAuthenticationBodyParam = 'fakedata';
    describe('#addAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAuthentication(applicationsAddAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Applications', 'addAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationsLocation = 'fakedata';
    const applicationsIncludeSublocations = true;
    describe('#listApplications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listApplications(applicationsLocation, applicationsIncludeSublocations, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.application));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Applications', 'listApplications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listallAuthenticationMethodsofaSpecificApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listallAuthenticationMethodsofaSpecificApplication((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.authentication));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Applications', 'listallAuthenticationMethodsofaSpecificApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sAMLAuthenticationConcurrentSession = true;
    const sAMLAuthenticationApiUse = true;
    const sAMLAuthenticationSAMLResponse = 'fakedata';
    describe('#postLogon - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postLogon(sAMLAuthenticationConcurrentSession, sAMLAuthenticationApiUse, sAMLAuthenticationSAMLResponse, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SAMLAuthentication', 'postLogon', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogoff1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postLogoff1((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SAMLAuthentication', 'postLogoff1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogoff12 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postLogoff12((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SharedLogonAuthentication', 'postLogoff12', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogon1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postLogon1((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SharedLogonAuthentication', 'postLogon1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const openIDConnectIdentityProviderAddOpenIDConnectIdentityProviderBodyParam = {
      id: 'string',
      authenticationFlow: 'string',
      discoveryEndpointUrl: 'string',
      clientId: 'string',
      clientSecret: 'string',
      clientSecretMethod: 'string'
    };
    describe('#addOpenIDConnectIdentityProvider - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addOpenIDConnectIdentityProvider(openIDConnectIdentityProviderAddOpenIDConnectIdentityProviderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OpenIDConnectIdentityProvider', 'addOpenIDConnectIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOpenIDConnectIdentityProviders - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllOpenIDConnectIdentityProviders((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OpenIDConnectIdentityProvider', 'getAllOpenIDConnectIdentityProviders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const openIDConnectIdentityProviderUpdateOpenIDConnectIdentityProviderBodyParam = {
      authenticationFlow: 'string',
      discoveryEndpointUrl: 'string',
      clientId: 'string',
      clientSecretMethod: 'string',
      userNameClaim: 'string'
    };
    describe('#updateOpenIDConnectIdentityProvider - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateOpenIDConnectIdentityProvider(openIDConnectIdentityProviderUpdateOpenIDConnectIdentityProviderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OpenIDConnectIdentityProvider', 'updateOpenIDConnectIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpecificOpenIDConnectIdentityProvider - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSpecificOpenIDConnectIdentityProvider((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OpenIDConnectIdentityProvider', 'getSpecificOpenIDConnectIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationMethodsConfigAddAuthenticationMethodBodyParam = {
      id: 'string',
      displayName: 'string',
      enabled: true,
      mobileEnabled: false,
      logoffUrl: 'string',
      secondFactorAuth: 'string',
      signInLabel: 'string',
      usernameFieldLabel: 'string',
      passwordFieldLabel: 'string'
    };
    describe('#addAuthenticationMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAuthenticationMethod(authenticationMethodsConfigAddAuthenticationMethodBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationMethodsConfig', 'addAuthenticationMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationMethods - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthenticationMethods((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Methods));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationMethodsConfig', 'getAuthenticationMethods', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationMethodsConfigUpdateAuthenticationMethodBodyParam = {
      displayName: 'string',
      enabled: false,
      mobileEnabled: false,
      logoffUrl: 'string',
      secondFactorAuth: 'string',
      signInLabel: 'string',
      usernameFieldLabel: 'string',
      passwordFieldLabel: 'string'
    };
    describe('#updateAuthenticationMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAuthenticationMethod(authenticationMethodsConfigUpdateAuthenticationMethodBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationMethodsConfig', 'updateAuthenticationMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpecificAuthenticationMethod - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSpecificAuthenticationMethod((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.displayName);
                assert.equal(true, data.response.enabled);
                assert.equal(false, data.response.mobileEnabled);
                assert.equal('string', data.response.logoffUrl);
                assert.equal('string', data.response.secondFactorAuth);
                assert.equal('string', data.response.signInLabel);
                assert.equal('string', data.response.usernameFieldLabel);
                assert.equal('string', data.response.passwordFieldLabel);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationMethodsConfig', 'getSpecificAuthenticationMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationEPMAuthenticationBodyParam = {
      Username: 'string',
      Password: 'string',
      ApplicationID: 'string'
    };
    describe('#ePMAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ePMAuthentication(authenticationEPMAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'ePMAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logout((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'logout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationWindowsAuthenticationBodyParam = {
      ApplicationID: 'string'
    };
    describe('#windowsAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.windowsAuthentication(authenticationWindowsAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'windowsAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationUsername = 'fakedata';
    const authenticationPassword = 'fakedata';
    describe('#pTAAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pTAAuthentication(authenticationUsername, authenticationPassword, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'pTAAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authenticate((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#login - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.login((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'login', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeYourPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeYourPassword((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'changeYourPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#whoami - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.whoami((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'whoami', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const centralCredentialProviderCCPAppID = 'fakedata';
    const centralCredentialProviderCCPSafe = 'fakedata';
    const centralCredentialProviderCCPObject = 'fakedata';
    describe('#getPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPassword(centralCredentialProviderCCPAppID, centralCredentialProviderCCPSafe, centralCredentialProviderCCPObject, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CentralCredentialProviderCCP', 'getPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rotateYourOwnAPIKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rotateYourOwnAPIKey((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rotate', 'rotateYourOwnAPIKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsAddKubernetesCACertificateBodyParam = 'fakedata';
    describe('#addKubernetesCACertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addKubernetesCACertificate(secretsAddKubernetesCACertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'addKubernetesCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsAddASecretBodyParam = 'fakedata';
    describe('#addASecret - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addASecret(secretsAddASecretBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'addASecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsVariableIds = 'fakedata';
    describe('#batchRetrieval - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.batchRetrieval(secretsVariableIds, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'batchRetrieval', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveASecret - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveASecret((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'retrieveASecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBaseUrlPolicyAddAuthPolicyModifierBodyParam = {
      PolicyModifier: 'string'
    };
    describe('#baseUrlPolicyAddAuthPolicyModifier - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyAddAuthPolicyModifier(policyBaseUrlPolicyAddAuthPolicyModifierBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyAddAuthPolicyModifier', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBaseUrlPolicyDeleteAuthPolicyModifierBodyParam = {
      PolicyModifier: 'string'
    };
    describe('#baseUrlPolicyDeleteAuthPolicyModifier - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyDeleteAuthPolicyModifier(policyBaseUrlPolicyDeleteAuthPolicyModifierBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyDeleteAuthPolicyModifier', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBaseUrlPolicyDeletePolicyBlockBodyParam = {
      path: 'string'
    };
    describe('#baseUrlPolicyDeletePolicyBlock - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyDeletePolicyBlock(policyBaseUrlPolicyDeletePolicyBlockBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyDeletePolicyBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetAuthPolicyModifiers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetAuthPolicyModifiers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetAuthPolicyModifiers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetNicePlinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetNicePlinks((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetNicePlinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyName = 'fakedata';
    describe('#baseUrlPolicyGetNicePolicyBlock - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetNicePolicyBlock(policyName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetNicePolicyBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetOathOtpClientName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetOathOtpClientName((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetOathOtpClientName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUuidOrName = 'fakedata';
    describe('#baseUrlPolicyGetPasswordComplexityRequirements - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetPasswordComplexityRequirements(policyUuidOrName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetPasswordComplexityRequirements', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetPlinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetPlinks((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetPlinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetPolicyBlock - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetPolicyBlock(policyName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetPolicyBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBaseUrlPolicyGetPolicyBoolBodyParam = {
      def: 'string',
      name: 'string'
    };
    describe('#baseUrlPolicyGetPolicyBool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetPolicyBool(policyBaseUrlPolicyGetPolicyBoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetPolicyBool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBaseUrlPolicyGetPolicyIntBodyParam = {
      def: 'string',
      name: 'string'
    };
    describe('#baseUrlPolicyGetPolicyInt - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetPolicyInt(policyBaseUrlPolicyGetPolicyIntBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetPolicyInt', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetPolicyMetaData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetPolicyMetaData((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetPolicyMetaData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBaseUrlPolicyGetPolicyStringBodyParam = {
      name: 'string'
    };
    describe('#baseUrlPolicyGetPolicyString - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetPolicyString(policyBaseUrlPolicyGetPolicyStringBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetPolicyString', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUserid = 'fakedata';
    const policyDeviceid = 'fakedata';
    describe('#baseUrlPolicyGetRsop - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetRsop(policyUserid, policyDeviceid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetRsop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetU2fClientName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetU2fClientName((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetU2fClientName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetUsingCloudMobileGP - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyGetUsingCloudMobileGP((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyGetUsingCloudMobileGP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBaseUrlPolicyPolicyChecksBodyParam = {
      deviceId: 'string',
      policies: [
        'string'
      ]
    };
    describe('#baseUrlPolicyPolicyChecks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicyPolicyChecks(policyBaseUrlPolicyPolicyChecksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicyPolicyChecks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBaseUrlPolicySavePolicyBlock2BodyParam = {
      path: 'string',
      settings: 'string',
      newpolicy: false
    };
    describe('#baseUrlPolicySavePolicyBlock2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicySavePolicyBlock2(policyBaseUrlPolicySavePolicyBlock2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicySavePolicyBlock2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBaseUrlPolicySavePolicyBlock3BodyParam = {
      policy: {
        Newpolicy: '<boolean>',
        Version: '<integer>',
        Path: '<string>',
        RevStamp: '<string>',
        Description: '<string>'
      },
      plinks: [
        'string'
      ]
    };
    describe('#baseUrlPolicySavePolicyBlock3 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicySavePolicyBlock3(policyBaseUrlPolicySavePolicyBlock3BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicySavePolicyBlock3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBaseUrlPolicySetPlinksBodyParam = {
      Plinks: [
        {
          Filters: [
            '<string>',
            '<string>'
          ],
          AllowedPolicies: [
            '<string>',
            '<string>'
          ],
          LinkType: '<string>',
          I18NDescriptionTag: '<string>',
          PolicySet: '<string>',
          EnableCompliant: '<boolean>',
          Description: '<string>',
          Params: [
            '<string>',
            '<string>'
          ]
        }
      ]
    };
    describe('#baseUrlPolicySetPlinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicySetPlinks(policyBaseUrlPolicySetPlinksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicySetPlinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBaseUrlPolicySetPlinksv2BodyParam = {
      Plinks: [
        {
          Filters: [
            '<string>',
            '<string>'
          ],
          AllowedPolicies: [
            '<string>',
            '<string>'
          ],
          LinkType: '<string>',
          I18NDescriptionTag: '<string>',
          PolicySet: '<string>',
          EnableCompliant: '<boolean>',
          Description: '<string>',
          Params: [
            '<string>',
            '<string>'
          ]
        }
      ],
      RevStamp: 'string'
    };
    describe('#baseUrlPolicySetPlinksv2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicySetPlinksv2(policyBaseUrlPolicySetPlinksv2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicySetPlinksv2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyValue = 'fakedata';
    const policyUseCloudCA = 'fakedata';
    const policyHideMobilePolicyForAD = 'fakedata';
    const policyRefreshInterval = 'fakedata';
    const policyGpUpdateInterval = 'fakedata';
    const policyActiveDirectoryCA = 'fakedata';
    describe('#baseUrlPolicySetUsingCloudMobileGP - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPolicySetUsingCloudMobileGP(policyValue, policyUseCloudCA, policyHideMobilePolicyForAD, policyRefreshInterval, policyGpUpdateInterval, policyActiveDirectoryCA, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'baseUrlPolicySetUsingCloudMobileGP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyAppendtoaPolicyBodyParam = {};
    describe('#appendtoaPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appendtoaPolicy(policyAppendtoaPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'appendtoaPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyAWSauthnIamPolicyAppendtoRootPolicyBodyParam = {};
    describe('#aWSauthnIamPolicyAppendtoRootPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.aWSauthnIamPolicyAppendtoRootPolicy(policyAWSauthnIamPolicyAppendtoRootPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'aWSauthnIamPolicyAppendtoRootPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyACLPolicyID = 'fakedata';
    const policyAddPolicyACLBodyParam = {
      Command: 'string',
      CommandGroup: false,
      PermissionType: 'string',
      Restrictions: 'string',
      UserName: 'string'
    };
    describe('#addPolicyACL - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPolicyACL(policyAddPolicyACLBodyParam, policyACLPolicyID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'addPolicyACL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPolicyACL - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPolicyACL(policyACLPolicyID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'listPolicyACL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyReplacePolicyBodyParam = {};
    describe('#replacePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.replacePolicy(policyReplacePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'replacePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#health - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.health((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConjurSecretsManager', 'health', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#information - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.information((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('11.1', data.response.release);
                assert.equal('5.6.0', data.response.version);
                assert.equal('object', typeof data.response.services);
                assert.equal('master', data.response.role);
                assert.equal('object', typeof data.response.configuration);
                assert.equal('object', typeof data.response.authenticators);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConjurSecretsManager', 'information', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const conjurSecretsManagerKind = 'fakedata';
    describe('#listPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listPolicies(conjurSecretsManagerKind, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConjurSecretsManager', 'listPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aggregatedEventsEventType = 'fakedata';
    const aggregatedEventsPublisher = 'fakedata';
    const aggregatedEventsJustification = 'fakedata';
    const aggregatedEventsApplicationType = 'fakedata';
    describe('#inboxEventsintheLastDay - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inboxEventsintheLastDay(aggregatedEventsEventType, aggregatedEventsPublisher, aggregatedEventsJustification, aggregatedEventsApplicationType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AggregatedEvents', 'inboxEventsintheLastDay', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aggregatedEventsDateFrom = 'fakedata';
    const aggregatedEventsDateTo = 'fakedata';
    const aggregatedEventsPolicyName = 'fakedata';
    describe('#inboxFromSpecificDates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inboxFromSpecificDates(aggregatedEventsPublisher, aggregatedEventsDateFrom, aggregatedEventsDateTo, aggregatedEventsJustification, aggregatedEventsApplicationType, aggregatedEventsPolicyName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AggregatedEvents', 'inboxFromSpecificDates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#policyAuditFromDatePolicyName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.policyAuditFromDatePolicyName(aggregatedEventsPublisher, aggregatedEventsDateFrom, aggregatedEventsDateTo, aggregatedEventsJustification, aggregatedEventsApplicationType, aggregatedEventsPolicyName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AggregatedEvents', 'policyAuditFromDatePolicyName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#threatDetectionFromSpecificDates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.threatDetectionFromSpecificDates(aggregatedEventsPublisher, aggregatedEventsDateFrom, aggregatedEventsDateTo, aggregatedEventsJustification, aggregatedEventsApplicationType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AggregatedEvents', 'threatDetectionFromSpecificDates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComputerGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getComputerGroups((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ComputerManagement', 'getComputerGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComputers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getComputers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ComputerManagement', 'getComputers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managePoliciesCreatePolicyBodyParam = {
      Duration: 3,
      PolicyType: 3,
      Name: 'string',
      Audit: true,
      Justification: 'string',
      Termination: false,
      IncludeUsers: [
        {
          UserName: 'Win10\\Username',
          Sid: ''
        }
      ],
      IncludeComputersInSet: [
        {
          Id: '5fb75c0d-03b4-49a9-ade2-edb1e4daeeb4'
        }
      ],
      IncludeComputerGroups: [
        {
          Id: '5fb75c0d-03b4-49a9-ade2-edb1e4daeeb4'
        }
      ],
      IncludeADComputerGroups: [
        {
          GroupName: 'Group\\Name',
          Sid: ''
        }
      ],
      LocalGroups: [
        {
          GroupName: 'OS_Group\\Name'
        }
      ]
    };
    describe('#createPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPolicy(managePoliciesCreatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagePolicies', 'createPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPolicies((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagePolicies', 'getPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPolicyDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagePolicies', 'getPolicyDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managePoliciesMode = 'fakedata';
    describe('#updateRansomwareMode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRansomwareMode(managePoliciesMode, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagePolicies', 'updateRansomwareMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managePoliciesUpdatePolicyForAdHocUserElevationPolicyonlyAdHocElevatePolicytypeBodyParam = {
      Duration: 6,
      PolicyType: 5,
      Name: 'string',
      Audit: false,
      Justification: 'string',
      TerminateProcesses: true,
      IncludeUsers: [
        {
          UserName: 'Win10\\Username',
          Sid: ''
        }
      ],
      IncludeComputersInSet: [
        {
          Id: '5fb75c0d-03b4-49a9-ade2-edb1e4daeeb4'
        }
      ],
      IncludeComputerGroups: [
        {
          Id: '5fb75c0d-03b4-49a9-ade2-edb1e4daeeb4'
        }
      ],
      IncludeADComputerGroups: [
        {
          GroupName: 'Group\\Name',
          Sid: ''
        }
      ],
      LocalGroups: [
        {
          GroupName: 'OS_Group\\Name'
        }
      ]
    };
    describe('#updatePolicyForAdHocUserElevationPolicyonlyAdHocElevatePolicytype - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePolicyForAdHocUserElevationPolicyonlyAdHocElevatePolicytype(managePoliciesUpdatePolicyForAdHocUserElevationPolicyonlyAdHocElevatePolicytypeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagePolicies', 'updatePolicyForAdHocUserElevationPolicyonlyAdHocElevatePolicytype', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managePoliciesUpdatePolicyForWindowsAdvancedPolicyonlyAdvancedWinAppPolicytypeBodyParam = {
      PolicyType: 'string',
      Name: 'string',
      Activation: {
        Enabled: true,
        DeactivateDate: {
          Year: 2025,
          Month: 5,
          Day: 30,
          Hours: 8,
          Minutes: 40,
          Seconds: 0
        }
      },
      IncludeComputersInSet: [
        {
          Id: '5fb75c0d-03b4-49a9-ade2-edb1e4daeeb4'
        }
      ],
      ExcludeComputersInSet: [
        {
          Id: 'abc2469a-6a7c-f42c-5b5e-3245332d6add'
        }
      ],
      IncludeComputerGroups: [
        {
          Id: '5fb75c0d-03b4-49a9-ade2-edb1e4daeeb4'
        }
      ],
      ExcludeComputerGroups: [
        {
          Id: 'abc2469a-6a7c-f42c-5b5e-3245332d6add'
        }
      ],
      IncludeUsers: [
        {
          UserName: 'elirazOwnDomain\\ElirazUserTest'
        }
      ],
      IncludeUserGroups: [
        {
          Sid: 'S-1-5-32-544'
        }
      ],
      IncludeADComputerGroups: [
        {
          GroupName: 'ADEliraz\\ADEliraz'
        }
      ],
      ExcludeADComputerGroups: [
        {
          Sid: 'S-1-5-83-0'
        }
      ],
      Applications: [
        {
          Type: 'EXE',
          Checksum: 'SHA1:5FF30FF14984820C01941F767CE47F0F39BDE265',
          FileName: 'grep.exe',
          FileSize: 135680,
          Location: 'Fixed'
        }
      ]
    };
    describe('#updatePolicyForWindowsAdvancedPolicyonlyAdvancedWinAppPolicytype - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePolicyForWindowsAdvancedPolicyonlyAdvancedWinAppPolicytype(managePoliciesUpdatePolicyForWindowsAdvancedPolicyonlyAdvancedWinAppPolicytypeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagePolicies', 'updatePolicyForWindowsAdvancedPolicyonlyAdvancedWinAppPolicytype', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ePMVersion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ePMVersion((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EndpointPrivilegeManagerEPM', 'ePMVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sets((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EndpointPrivilegeManagerEPM', 'sets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rawEventDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rawEventDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EndpointPrivilegeManagerEPM', 'rawEventDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclBaseUrlAclCheckRowRightBodyParam = {
      RowKey: 'string',
      Rights: 'string',
      Table: [
        'string'
      ]
    };
    describe('#baseUrlAclCheckRowRight - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAclCheckRowRight(aclBaseUrlAclCheckRowRightBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acl', 'baseUrlAclCheckRowRight', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclBaseUrlAclGetAceBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlAclGetAce - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAclGetAce(aclBaseUrlAclGetAceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acl', 'baseUrlAclGetAce', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclBaseUrlAclGetCollectionAcesBodyParam = {
      RowKey: 'string',
      ReduceSysadmin: 'string',
      Table: [
        'string'
      ]
    };
    describe('#baseUrlAclGetCollectionAces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAclGetCollectionAces(aclBaseUrlAclGetCollectionAcesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acl', 'baseUrlAclGetCollectionAces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclTable = 'fakedata';
    const aclRowkey = 'fakedata';
    const aclReduceSysadmin = 'fakedata';
    describe('#baseUrlAclGetCollectionAcesHelper - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAclGetCollectionAcesHelper(aclTable, aclRowkey, aclReduceSysadmin, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acl', 'baseUrlAclGetCollectionAcesHelper', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclBaseUrlAclGetDirAcesBodyParam = {
      Path: 'string',
      Inherit: 'string'
    };
    describe('#baseUrlAclGetDirAces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAclGetDirAces(aclBaseUrlAclGetDirAcesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acl', 'baseUrlAclGetDirAces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclBaseUrlAclGetEffectiveDirRightsBodyParam = {
      Paths: [
        'string'
      ]
    };
    describe('#baseUrlAclGetEffectiveDirRights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAclGetEffectiveDirRights(aclBaseUrlAclGetEffectiveDirRightsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acl', 'baseUrlAclGetEffectiveDirRights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclBaseUrlAclGetEffectiveFileRightsBodyParam = {
      Paths: [
        'string'
      ]
    };
    describe('#baseUrlAclGetEffectiveFileRights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAclGetEffectiveFileRights(aclBaseUrlAclGetEffectiveFileRightsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acl', 'baseUrlAclGetEffectiveFileRights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclBaseUrlAclGetEffectiveRowRightsBodyParam = {
      Rows: [
        {
          RowKey: '<string>',
          ReduceSysadmin: '<boolean>',
          Table: '<string>'
        }
      ]
    };
    describe('#baseUrlAclGetEffectiveRowRights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAclGetEffectiveRowRights(aclBaseUrlAclGetEffectiveRowRightsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acl', 'baseUrlAclGetEffectiveRowRights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclBaseUrlAclGetFileAcesBodyParam = {
      Inherit: 'string',
      Paths: [
        'string'
      ]
    };
    describe('#baseUrlAclGetFileAces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAclGetFileAces(aclBaseUrlAclGetFileAcesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acl', 'baseUrlAclGetFileAces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclBaseUrlAclGetRowAcesBodyParam = {
      RowKey: 'string',
      Table: 'string',
      ReduceSysadmin: 'string',
      Inherit: 'string'
    };
    describe('#baseUrlAclGetRowAces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAclGetRowAces(aclBaseUrlAclGetRowAcesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acl', 'baseUrlAclGetRowAces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclReduceSys = 'fakedata';
    const aclInherit = 'fakedata';
    const aclRk = 'fakedata';
    describe('#baseUrlAclGetRowAcesHelper - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAclGetRowAcesHelper(aclReduceSys, aclInherit, aclTable, aclRk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acl', 'baseUrlAclGetRowAcesHelper', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authProfileUuid = 'fakedata';
    const authProfileBaseUrlAuthProfileDeleteProfileBodyParam = {
      uuid: 'string'
    };
    describe('#baseUrlAuthProfileDeleteProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAuthProfileDeleteProfile(authProfileUuid, authProfileBaseUrlAuthProfileDeleteProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthProfile', 'baseUrlAuthProfileDeleteProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authProfileBaseUrlAuthProfileGetProfileBodyParam = {
      uuid: 'string'
    };
    describe('#baseUrlAuthProfileGetProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAuthProfileGetProfile(authProfileUuid, authProfileBaseUrlAuthProfileGetProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthProfile', 'baseUrlAuthProfileGetProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAuthProfileGetProfileList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAuthProfileGetProfileList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthProfile', 'baseUrlAuthProfileGetProfileList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authProfileBaseUrlAuthProfileSaveProfileBodyParam = {
      settings: {
        Challenges: [
          '<string>',
          '<string>'
        ],
        AdditionalData: '<object>',
        Uuid: '<string>',
        DurationInMinutes: '<integer>',
        Name: '<string>'
      }
    };
    describe('#baseUrlAuthProfileSaveProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlAuthProfileSaveProfile(authProfileBaseUrlAuthProfileSaveProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthProfile', 'baseUrlAuthProfileSaveProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlBrandInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlBrandInfo((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Brand', 'baseUrlBrandInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlBrandMyBrand - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlBrandMyBrand((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Brand', 'baseUrlBrandMyBrand', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceBulkDeleteUsersBodyParam = {
      Users: 'string'
    };
    describe('#baseUrlCDirectoryServiceBulkDeleteUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceBulkDeleteUsers(cDirectoryServiceBaseUrlCDirectoryServiceBulkDeleteUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceBulkDeleteUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceChangeUserBodyParam = {
      CmaRedirectedUserUuid: 'string',
      HomeNumber: 'string',
      AccountExp: 'string',
      ReportsTo: 'string',
      DisplayName: 'string',
      PasswordNeverExpire: 'string',
      ID: 'string',
      OfficeNumber: 'string',
      MobileNumber: 'string',
      InEverybodyRole: 'string',
      ServiceUser: 'string',
      PreferredCulture: 'string',
      Mail: 'string',
      Description: 'string',
      Name: 'string'
    };
    describe('#baseUrlCDirectoryServiceChangeUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceChangeUser(cDirectoryServiceBaseUrlCDirectoryServiceChangeUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceChangeUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceCreateUserBodyParam = {
      Name: 'string',
      InEverybodyRole: 'string',
      SendSmsInvite: 'string',
      InSysAdminRole: 'string',
      Description: 'string',
      MobileNumber: 'string',
      Password: 'string',
      CmaRedirectedUserUuid: 'string',
      ServiceUser: 'string',
      OfficeNumber: 'string',
      ReportsTo: 'string',
      SendEmailInvite: 'string',
      HomeNumber: 'string',
      DisplayName: 'string',
      ForcePasswordChangeNext: 'string',
      PasswordNeverExpire: 'string',
      Mail: 'string'
    };
    describe('#baseUrlCDirectoryServiceCreateUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceCreateUser(cDirectoryServiceBaseUrlCDirectoryServiceCreateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceCreateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceCreateUserBulkBodyParam = {
      Name: 'string',
      InEverybodyRole: 'string',
      SendSmsInvite: 'string',
      InSysAdminRole: 'string',
      Description: 'string',
      MobileNumber: 'string',
      Password: 'string',
      CmaRedirectedUserUuid: 'string',
      ServiceUser: 'string',
      OfficeNumber: 'string',
      ReportsTo: 'string',
      SendEmailInvite: 'string',
      HomeNumber: 'string',
      DisplayName: 'string',
      ForcePasswordChangeNext: 'string',
      PasswordNeverExpire: 'string',
      Mail: 'string'
    };
    describe('#baseUrlCDirectoryServiceCreateUserBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceCreateUserBulk(cDirectoryServiceBaseUrlCDirectoryServiceCreateUserBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceCreateUserBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceCreateUserQuickBodyParam = {
      Mail: 'string',
      Name: 'string',
      HomeNumber: 'string',
      AccountExp: 'string',
      ReportsTo: 'string',
      PasswordNeverExpire: 'string',
      DisplayName: 'string',
      OfficeNumber: 'string',
      MobileNumber: 'string',
      InEverybodyRole: 'string',
      Description: 'string'
    };
    describe('#baseUrlCDirectoryServiceCreateUserQuick - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceCreateUserQuick(cDirectoryServiceBaseUrlCDirectoryServiceCreateUserQuickBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceCreateUserQuick', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceCreateUsersBodyParam = {
      Rows: [
        {
          InEverybodyRole: '<boolean>',
          SendSmsInvite: '<boolean>',
          InSysAdminRole: '<boolean>',
          Description: '<string>',
          Name: '<string>',
          MobileNumber: '<string>',
          Password: '<string>',
          CmaRedirectedUserUuid: '<string>',
          ServiceUser: '<boolean>',
          OfficeNumber: '<string>',
          ReportsTo: '<string>',
          SendEmailInvite: '<boolean>',
          HomeNumber: '<string>',
          DisplayName: '<string>',
          ForcePasswordChangeNext: '<boolean>',
          PasswordNeverExpire: '<boolean>',
          Mail: '<string>'
        }
      ]
    };
    describe('#baseUrlCDirectoryServiceCreateUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceCreateUsers(cDirectoryServiceBaseUrlCDirectoryServiceCreateUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceCreateUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceId = 'fakedata';
    describe('#baseUrlCDirectoryServiceDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceDelete(cDirectoryServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceID = 'fakedata';
    describe('#baseUrlCDirectoryServiceDeleteUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceDeleteUser(cDirectoryServiceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceDeleteUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceTimespan = 'fakedata';
    describe('#baseUrlCDirectoryServiceExemptUserFromMfa - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceExemptUserFromMfa(cDirectoryServiceID, cDirectoryServiceTimespan, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceExemptUserFromMfa', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGetBulkImportWithExtAtt - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceGetBulkImportWithExtAtt((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceGetBulkImportWithExtAtt', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGetTechSupportUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceGetTechSupportUser((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceGetTechSupportUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceGetUserBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlCDirectoryServiceGetUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceGetUser(cDirectoryServiceBaseUrlCDirectoryServiceGetUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceGetUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGetUserAttributes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceGetUserAttributes((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceGetUserAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceGetUserByNameBodyParam = {
      username: 'string'
    };
    describe('#baseUrlCDirectoryServiceGetUserByName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceGetUserByName(cDirectoryServiceBaseUrlCDirectoryServiceGetUserByNameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceGetUserByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGetUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceGetUsers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceGetUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceGetUsersFromCsvFileBodyParam = {
      FileName: {
        InEverybodyRole: '<boolean>',
        SendSmsInvite: '<boolean>',
        InSysAdminRole: '<boolean>',
        Description: '<string>',
        Name: '<string>',
        MobileNumber: '<string>',
        Password: '<string>',
        CmaRedirectedUserUuid: '<string>',
        ServiceUser: '<boolean>',
        OfficeNumber: '<string>',
        ReportsTo: '<string>',
        SendEmailInvite: '<boolean>',
        HomeNumber: '<string>',
        DisplayName: '<string>',
        ForcePasswordChangeNext: '<boolean>',
        PasswordNeverExpire: '<boolean>',
        Mail: '<string>'
      }
    };
    describe('#baseUrlCDirectoryServiceGetUsersFromCsvFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceGetUsersFromCsvFile(cDirectoryServiceBaseUrlCDirectoryServiceGetUsersFromCsvFileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceGetUsersFromCsvFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceGrantAccessBodyParam = {
      SupportAccessTimeInMinute: 'string',
      SupportEmailAddress: 'string',
      SendEmail: 'string'
    };
    describe('#baseUrlCDirectoryServiceGrantAccess - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceGrantAccess(cDirectoryServiceBaseUrlCDirectoryServiceGrantAccessBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceGrantAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceDirectoryServiceUuid = 'fakedata';
    describe('#baseUrlCDirectoryServiceRefreshToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceRefreshToken(cDirectoryServiceID, cDirectoryServiceDirectoryServiceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceRefreshToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceRemoveAuthSourceBodyParam = {
      Users: [
        'string'
      ],
      SendInvites: 'string'
    };
    describe('#baseUrlCDirectoryServiceRemoveAuthSource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceRemoveAuthSource(cDirectoryServiceBaseUrlCDirectoryServiceRemoveAuthSourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceRemoveAuthSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceFederationUuid = 'fakedata';
    const cDirectoryServiceSendInvites = 'fakedata';
    describe('#baseUrlCDirectoryServiceRemoveFederationAuthSource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceRemoveFederationAuthSource(cDirectoryServiceFederationUuid, cDirectoryServiceSendInvites, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceRemoveFederationAuthSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceSetUserPictureBodyParam = {
      File: 'string'
    };
    describe('#baseUrlCDirectoryServiceSetUserPicture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceSetUserPicture(cDirectoryServiceID, cDirectoryServiceBaseUrlCDirectoryServiceSetUserPictureBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceSetUserPicture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceSetUserStateBodyParam = {
      ID: 'string',
      state: 'string'
    };
    describe('#baseUrlCDirectoryServiceSetUserState - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceSetUserState(cDirectoryServiceBaseUrlCDirectoryServiceSetUserStateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceSetUserState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceAction = 'fakedata';
    describe('#baseUrlCDirectoryServiceStandardJsonResultWithPermissionCheck - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceStandardJsonResultWithPermissionCheck(cDirectoryServiceAction, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceStandardJsonResultWithPermissionCheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cDirectoryServiceBaseUrlCDirectoryServiceSubmitUploadedFileBodyParam = {
      File: 'string'
    };
    describe('#baseUrlCDirectoryServiceSubmitUploadedFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCDirectoryServiceSubmitUploadedFile(cDirectoryServiceBaseUrlCDirectoryServiceSubmitUploadedFileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CDirectoryService', 'baseUrlCDirectoryServiceSubmitUploadedFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionCreateDynamicCollectionBodyParam = {
      objecttype: 'string',
      name: 'string',
      sql: 'string',
      filters: 'string',
      subobjecttype: 'string',
      description: 'string',
      parent: 'string'
    };
    describe('#baseUrlCollectionCreateDynamicCollection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionCreateDynamicCollection(collectionBaseUrlCollectionCreateDynamicCollectionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionCreateDynamicCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionCreateManualCollectionBodyParam = {
      ObjectType: 'string',
      Name: 'string',
      SubObjectType: 'string',
      Add: 'string',
      Description: 'string',
      Parent: 'string'
    };
    describe('#baseUrlCollectionCreateManualCollection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionCreateManualCollection(collectionBaseUrlCollectionCreateManualCollectionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionCreateManualCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionDeleteCollectionBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlCollectionDeleteCollection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionDeleteCollection(collectionBaseUrlCollectionDeleteCollectionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionDeleteCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionGetBucketContentsBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlCollectionGetBucketContents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionGetBucketContents(collectionBaseUrlCollectionGetBucketContentsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionGetBucketContents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionGetCollectionBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlCollectionGetCollection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionGetCollection(collectionBaseUrlCollectionGetCollectionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionGetCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionGetCollectionPermissionsBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlCollectionGetCollectionPermissions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionGetCollectionPermissions(collectionBaseUrlCollectionGetCollectionPermissionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionGetCollectionPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionGetCollectionReferencesBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlCollectionGetCollectionReferences - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionGetCollectionReferences(collectionBaseUrlCollectionGetCollectionReferencesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionGetCollectionReferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionGetCollectionRightsBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlCollectionGetCollectionRights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionGetCollectionRights(collectionBaseUrlCollectionGetCollectionRightsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionGetCollectionRights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionGetCollectionTemplateBodyParam = {
      ObjectType: 'string',
      SubObjectType: 'string'
    };
    describe('#baseUrlCollectionGetCollectionTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionGetCollectionTemplate(collectionBaseUrlCollectionGetCollectionTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionGetCollectionTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionGetMembersBodyParam = {
      ID: 'string',
      recurse: 'string'
    };
    describe('#baseUrlCollectionGetMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionGetMembers(collectionBaseUrlCollectionGetMembersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionGetMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionGetObjectCollectionsBodyParam = {
      ObjectType: 'string',
      SubObjectType: 'string'
    };
    describe('#baseUrlCollectionGetObjectCollections - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionGetObjectCollections(collectionBaseUrlCollectionGetObjectCollectionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionGetObjectCollections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionGetObjectCollectionsAndFiltersBodyParam = {
      ObjectType: 'string',
      CollectionType: 'string',
      NoBuiltIns: 'string',
      SubObjectType: 'string',
      ReduceSysadmin: 'string'
    };
    describe('#baseUrlCollectionGetObjectCollectionsAndFilters - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionGetObjectCollectionsAndFilters(collectionBaseUrlCollectionGetObjectCollectionsAndFiltersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionGetObjectCollectionsAndFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionIsMemberBodyParam = {
      ID: 'string',
      Table: 'string',
      Key: 'string',
      directHint: 'string',
      recurse: 'string'
    };
    describe('#baseUrlCollectionIsMember - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionIsMember(collectionBaseUrlCollectionIsMemberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionIsMember', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionSetCollectionPermissionsBodyParam = {
      Grants: {
        Rights: '<string>',
        Principal: '<string>',
        PrincipalId: '<string>',
        PType: '<string>'
      },
      ID: 'string'
    };
    describe('#baseUrlCollectionSetCollectionPermissions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionSetCollectionPermissions(collectionBaseUrlCollectionSetCollectionPermissionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionSetCollectionPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionUpdateCollectionBodyParam = {
      ID: 'string',
      sql: 'string',
      desccription: 'string',
      name: 'string',
      parent: 'string'
    };
    describe('#baseUrlCollectionUpdateCollection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionUpdateCollection(collectionBaseUrlCollectionUpdateCollectionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionUpdateCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionBaseUrlCollectionUpdateMembersCollectionBodyParam = {
      args: {
        id: '<string>',
        Add: [
          '<string>',
          '<string>'
        ],
        Remove: [
          '<string>',
          '<string>'
        ]
      }
    };
    describe('#baseUrlCollectionUpdateMembersCollection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCollectionUpdateMembersCollection(collectionBaseUrlCollectionUpdateMembersCollectionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collection', 'baseUrlCollectionUpdateMembersCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreValue = 'fakedata';
    const coreOldvalue = 'fakedata';
    const coreLabel = 'fakedata';
    describe('#baseUrlCoreAddBlockedIpRange - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreAddBlockedIpRange(coreValue, coreOldvalue, coreLabel, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreAddBlockedIpRange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreAddPremDetectRange - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreAddPremDetectRange(coreValue, coreOldvalue, coreLabel, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreAddPremDetectRange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreBaseUrlCoreAssignDirectoryFileRightsToRolesBodyParam = {
      Permission: {
        Role: '<string>',
        Rights: '<string>'
      },
      path: 'string'
    };
    describe('#baseUrlCoreAssignDirectoryFileRightsToRoles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreAssignDirectoryFileRightsToRoles(coreBaseUrlCoreAssignDirectoryFileRightsToRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreAssignDirectoryFileRightsToRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreBaseUrlCoreAssignDirectoryRightsToRolesBodyParam = {
      Permission: {
        Role: '<string>',
        Rights: '<string>'
      },
      path: 'string'
    };
    describe('#baseUrlCoreAssignDirectoryRightsToRoles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreAssignDirectoryRightsToRoles(coreBaseUrlCoreAssignDirectoryRightsToRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreAssignDirectoryRightsToRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreBaseUrlCoreAssignFileRightsToRolesBodyParam = {
      Permission: {
        Role: '<string>',
        Rights: '<string>'
      },
      path: 'string'
    };
    describe('#baseUrlCoreAssignFileRightsToRoles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreAssignFileRightsToRoles(coreBaseUrlCoreAssignFileRightsToRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreAssignFileRightsToRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreProxyUuid = 'fakedata';
    describe('#baseUrlCoreCheckProxyHealth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreCheckProxyHealth(coreProxyUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreCheckProxyHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const corePathParam = 'fakedata';
    describe('#baseUrlCoreCreateDirectory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreCreateDirectory(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreCreateDirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreCreateTenantReportsDirectory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreCreateTenantReportsDirectory((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreCreateTenantReportsDirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreCreateUserHomeReportsDirectory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreCreateUserHomeReportsDirectory((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreCreateUserHomeReportsDirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDelBlockedIpRange - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDelBlockedIpRange(coreValue, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDelBlockedIpRange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDelPremDetectRange - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDelPremDetectRange(coreValue, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDelPremDetectRange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreAlias = 'fakedata';
    const coreBaseUrlCoreDeleteAliasBodyParam = {
      Suffix: 'string'
    };
    describe('#baseUrlCoreDeleteAlias - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDeleteAlias(coreAlias, coreBaseUrlCoreDeleteAliasBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDeleteAlias', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreBaseUrlCoreDeleteAliasesBodyParam = {
      Suffixes: [
        'string'
      ]
    };
    describe('#baseUrlCoreDeleteAliases - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDeleteAliases(coreBaseUrlCoreDeleteAliasesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDeleteAliases', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreBaseUrlCoreDeleteCertificateBodyParam = {
      Thumbprints: [
        'string'
      ]
    };
    describe('#baseUrlCoreDeleteCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDeleteCertificate(coreBaseUrlCoreDeleteCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDeleteCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteDirectory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDeleteDirectory(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDeleteDirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDeleteFile(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDeleteFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreBaseUrlCoreDeleteFilesBodyParam = {
      paths: 'string'
    };
    describe('#baseUrlCoreDeleteFiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDeleteFiles(coreBaseUrlCoreDeleteFilesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDeleteFiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreBaseUrlCoreDeleteProxiesBodyParam = {
      Proxies: 'string'
    };
    describe('#baseUrlCoreDeleteProxies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDeleteProxies(coreBaseUrlCoreDeleteProxiesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDeleteProxies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteProxy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDeleteProxy(coreProxyUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDeleteProxy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreKey = 'fakedata';
    describe('#baseUrlCoreDeleteTenantConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDeleteTenantConfig(coreKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDeleteTenantConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDirectoryExists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDirectoryExists(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDirectoryExists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreThumbprint = 'fakedata';
    const coreFilename = 'fakedata';
    describe('#baseUrlCoreDownloadCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDownloadCertificate(coreThumbprint, coreFilename, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDownloadCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDownloadFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreDownloadFile(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreDownloadFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreFileExists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreFileExists(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreFileExists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreBaseUrlCoreGenerateNewProxyCodeBodyParam = {
      maxUses: 5,
      validLength: 3
    };
    describe('#baseUrlCoreGenerateNewProxyCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGenerateNewProxyCode(coreBaseUrlCoreGenerateNewProxyCodeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGenerateNewProxyCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const corePasswordLength = 'fakedata';
    describe('#baseUrlCoreGeneratePassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGeneratePassword(corePasswordLength, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGeneratePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreForestRootDomain = 'fakedata';
    describe('#baseUrlCoreGetAdLoginSuffixesByForest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetAdLoginSuffixesByForest(coreForestRootDomain, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetAdLoginSuffixesByForest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreDirectoryServiceUuidOrDomainName = 'fakedata';
    describe('#baseUrlCoreGetAdTopology - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetAdTopology(coreDirectoryServiceUuidOrDomainName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetAdTopology', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetAliasesForTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetAliasesForTenant((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetAliasesForTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreRole = 'fakedata';
    describe('#baseUrlCoreGetAssignedAdministrativeRights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetAssignedAdministrativeRights(coreRole, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetAssignedAdministrativeRights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetBlockedIpRanges - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetBlockedIpRanges((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetBlockedIpRanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreTenantId = 'fakedata';
    describe('#baseUrlCoreGetCaCertChain - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetCaCertChain(coreFilename, coreTenantId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetCaCertChain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetCdsAliasesForTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetCdsAliasesForTenant((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetCdsAliasesForTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetCloudCACert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetCloudCACert(coreFilename, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetCloudCACert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreProxyId = 'fakedata';
    describe('#baseUrlCoreGetConnectorLog4NetConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetConnectorLog4NetConfig(coreProxyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetConnectorLog4NetConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetCurrentIwaJsonpUrl - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetCurrentIwaJsonpUrl((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetCurrentIwaJsonpUrl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetCurrentIwaUrl - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetCurrentIwaUrl((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetCurrentIwaUrl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreCertFileName = 'fakedata';
    describe('#baseUrlCoreGetDefaultGlobalAppSigningCert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetDefaultGlobalAppSigningCert(coreCertFileName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetDefaultGlobalAppSigningCert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDirectories - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetDirectories(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetDirectories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreFilter = 'fakedata';
    const coreFileext = 'fakedata';
    describe('#baseUrlCoreGetDirectoryContents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetDirectoryContents(corePathParam, coreFilter, coreFileext, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetDirectoryContents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDirectoryFileRolesAndRights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetDirectoryFileRolesAndRights(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetDirectoryFileRolesAndRights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDirectoryInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetDirectoryInfo(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetDirectoryInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDirectoryRolesAndRights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetDirectoryRolesAndRights(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetDirectoryRolesAndRights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDirectoryServices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetDirectoryServices((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetDirectoryServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreDirectoryServiceUuid = 'fakedata';
    const coreDomainName = 'fakedata';
    describe('#baseUrlCoreGetDomainControllersForDomain - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetDomainControllersForDomain(coreDirectoryServiceUuid, coreDomainName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetDomainControllersForDomain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDownloadUrls - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetDownloadUrls((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetDownloadUrls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetFileInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetFileInfo(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetFileInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetFileRolesAndRights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetFileRolesAndRights(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetFileRolesAndRights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetIwaTrustRootCert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetIwaTrustRootCert(coreFilename, coreTenantId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetIwaTrustRootCert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreTag = 'fakedata';
    const coreLocale = 'fakedata';
    const coreQualifier = 'fakedata';
    describe('#baseUrlCoreGetLocTag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetLocTag(coreTag, coreLocale, coreQualifier, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetLocTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreId = 'fakedata';
    const coreUseCache = 'fakedata';
    const coreGetAdminAccountStatus = 'fakedata';
    describe('#baseUrlCoreGetOUTreeContents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetOUTreeContents(coreId, coreDirectoryServiceUuid, coreUseCache, coreGetAdminAccountStatus, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetOUTreeContents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetPremDetectRanges - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetPremDetectRanges((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetPremDetectRanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetProxyIwaHostCertificateFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetProxyIwaHostCertificateFile(coreProxyUuid, coreFilename, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetProxyIwaHostCertificateFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetProxyIwaSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetProxyIwaSettings(coreProxyUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetProxyIwaSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetPurchasedLicenses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetPurchasedLicenses((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetPurchasedLicenses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetReportsDirectoryContents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetReportsDirectoryContents(corePathParam, coreFilter, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetReportsDirectoryContents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetSupportedCultures - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetSupportedCultures((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetSupportedCultures', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetTenantCACert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetTenantCACert(coreFilename, coreTenantId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetTenantCACert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreDflt = 'fakedata';
    describe('#baseUrlCoreGetTenantConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetTenantConfig(coreKey, coreDflt, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetTenantConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreName = 'fakedata';
    describe('#baseUrlCoreGetUniqueFileName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetUniqueFileName(corePathParam, coreName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetUniqueFileName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreID = 'fakedata';
    const coreSettingType = 'fakedata';
    describe('#baseUrlCoreGetUserSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetUserSettings(coreID, coreSettingType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetUserSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetZsoCertAuthority - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetZsoCertAuthority(coreCertFileName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetZsoCertAuthority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetZsoHostInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreGetZsoHostInfo((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreGetZsoHostInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreState = 'fakedata';
    const coreBaseUrlCoreHandleTwilioSmsReceiptBodyParam = {
      SMsStatus: 'string',
      AccountSid: 'string',
      SmsSid: 'string',
      MessageStatus: 'string',
      MessageSid: 'string'
    };
    describe('#baseUrlCoreHandleTwilioSmsReceipt - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreHandleTwilioSmsReceipt(coreState, coreBaseUrlCoreHandleTwilioSmsReceiptBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreHandleTwilioSmsReceipt', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreIssueUserCert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreIssueUserCert((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreIssueUserCert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreCertPass = 'fakedata';
    const coreDeviceId = 'fakedata';
    describe('#baseUrlCoreIssueZsoUserCert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreIssueZsoUserCert(coreCertFileName, coreCertPass, coreDeviceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreIssueZsoUserCert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreListDirectory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreListDirectory(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreListDirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreLocalizeReportNameDesc - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreLocalizeReportNameDesc(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreLocalizeReportNameDesc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreFileName = 'fakedata';
    const coreText = 'fakedata';
    const coreContentType = 'fakedata';
    describe('#baseUrlCoreMakeFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreMakeFile(coreFileName, coreText, coreContentType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreMakeFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreToPath = 'fakedata';
    describe('#baseUrlCoreMoveDirectory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreMoveDirectory(corePathParam, coreToPath, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreMoveDirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreNotifyEnvironment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreNotifyEnvironment(coreProxyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreNotifyEnvironment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreOneTimePass = 'fakedata';
    const coreTargetUrl = 'fakedata';
    describe('#baseUrlCoreProcessProxyIwaCloudRedirect - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreProcessProxyIwaCloudRedirect(coreOneTimePass, coreTargetUrl, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreProcessProxyIwaCloudRedirect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreReIssueIwaHostCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreReIssueIwaHostCertificate(coreProxyUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreReIssueIwaHostCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreReadFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreReadFile(corePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreReadFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreNewName = 'fakedata';
    describe('#baseUrlCoreRenameCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreRenameCertificate(coreThumbprint, coreNewName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreRenameCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreBaseUrlCoreSetConnectorLog4NetConfigBodyParam = {
      lintfile_maxSizeRollBackups: 'string',
      rollingfile_file: 'string',
      lintfile_maximumFileSize: 'string',
      rollingfile_maxSizeRollBackups: 'string',
      rollingfile_maximumFileSize: 'string',
      lintfile_file: 'string',
      logLevel: 'string'
    };
    describe('#baseUrlCoreSetConnectorLog4NetConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreSetConnectorLog4NetConfig(coreProxyId, coreBaseUrlCoreSetConnectorLog4NetConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreSetConnectorLog4NetConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreType = 'fakedata';
    describe('#baseUrlCoreSetDefaultCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreSetDefaultCertificate(coreType, coreThumbprint, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreSetDefaultCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const corePasswd = 'fakedata';
    const coreBaseUrlCoreSetProxyIwaHostCertificateFileBodyParam = {
      cert: 'string'
    };
    describe('#baseUrlCoreSetProxyIwaHostCertificateFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreSetProxyIwaHostCertificateFile(coreProxyUuid, corePasswd, coreBaseUrlCoreSetProxyIwaHostCertificateFileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreSetProxyIwaHostCertificateFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreBaseUrlCoreSetProxyIwaSettingsBodyParam = {
      RadiusServerConfig: 'string',
      RadiusConfig: 'string'
    };
    describe('#baseUrlCoreSetProxyIwaSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreSetProxyIwaSettings(coreProxyUuid, coreBaseUrlCoreSetProxyIwaSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreSetProxyIwaSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreSetTenantConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreSetTenantConfig(coreKey, coreValue, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreSetTenantConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreServiceName = 'fakedata';
    describe('#baseUrlCoreStartService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreStartService(coreProxyId, coreServiceName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreStartService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreStopService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreStopService(coreProxyId, coreServiceName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreStopService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreDomain = 'fakedata';
    const coreOldName = 'fakedata';
    const coreCdsAlias = 'fakedata';
    describe('#baseUrlCoreStoreAlias - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreStoreAlias(coreAlias, coreDomain, coreOldName, coreCdsAlias, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreStoreAlias', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreGuid = 'fakedata';
    describe('#baseUrlCoreStoreUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreStoreUser(coreName, coreGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreStoreUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreTarget = 'fakedata';
    const coreBaseUrlCoreStoreUserSettingBodyParam = {
      ID: 'string',
      Target: 'string',
      SettingType: 'string'
    };
    describe('#baseUrlCoreStoreUserSetting - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreStoreUserSetting(coreID, coreTarget, coreSettingType, coreBaseUrlCoreStoreUserSettingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreStoreUserSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const corePrecedence = 'fakedata';
    describe('#baseUrlCoreUpdateDirectoryServicesPrecedence - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreUpdateDirectoryServicesPrecedence(corePrecedence, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreUpdateDirectoryServicesPrecedence', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const corePassword = 'fakedata';
    const coreBaseUrlCoreUpdateProxyIwaSettingsBodyParam = {
      Form: {
        HttpPort: '<integer>',
        Port: '<integer>',
        WebProxyEnabled: '<boolean>',
        CertThumbprint: '<string>',
        WebProxyPort: '<integer>',
        Enabled: '<boolean>',
        IwaCheckTimeout: '<integer>',
        Name: '<string>',
        Hostname: '<string>'
      }
    };
    describe('#baseUrlCoreUpdateProxyIwaSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreUpdateProxyIwaSettings(coreProxyUuid, corePassword, coreBaseUrlCoreUpdateProxyIwaSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreUpdateProxyIwaSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreBaseUrlCoreUploadCertificateBodyParam = {
      File: 'string'
    };
    describe('#baseUrlCoreUploadCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreUploadCertificate(coreBaseUrlCoreUploadCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreUploadCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const coreContent = 'fakedata';
    describe('#baseUrlCoreWriteFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCoreWriteFile(corePathParam, coreText, coreContent, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Core', 'baseUrlCoreWriteFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cssIntegrationBaseUrlCssIntegrationDzdoExecuteBodyParam = {
      device_type: 'string'
    };
    describe('#baseUrlCssIntegrationDzdoExecute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCssIntegrationDzdoExecute(cssIntegrationBaseUrlCssIntegrationDzdoExecuteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CssIntegration', 'baseUrlCssIntegrationDzdoExecute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCssIntegrationGetChallengeProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCssIntegrationGetChallengeProfiles((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CssIntegration', 'baseUrlCssIntegrationGetChallengeProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cssIntegrationBaseUrlCssIntegrationMfaLoginBodyParam = {
      device_type: 'string'
    };
    describe('#baseUrlCssIntegrationMfaLogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCssIntegrationMfaLogin(cssIntegrationBaseUrlCssIntegrationMfaLoginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CssIntegration', 'baseUrlCssIntegrationMfaLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cssIntegrationBaseUrlCssIntegrationSetChallengeProfilesBodyParam = {
      MfaChallenge: 'string',
      DzdoChallenge: 'string'
    };
    describe('#baseUrlCssIntegrationSetChallengeProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlCssIntegrationSetChallengeProfiles(cssIntegrationBaseUrlCssIntegrationSetChallengeProfilesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CssIntegration', 'baseUrlCssIntegrationSetChallengeProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extDataBaseUrlExtDataGetColumnBodyParam = {
      ID: 'string',
      Column: 'string',
      Table: 'string'
    };
    describe('#baseUrlExtDataGetColumn - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlExtDataGetColumn(extDataBaseUrlExtDataGetColumnBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtData', 'baseUrlExtDataGetColumn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extDataBaseUrlExtDataGetColumnsBodyParam = {
      ID: 'string',
      Table: 'string',
      IncludeNulls: 'string'
    };
    describe('#baseUrlExtDataGetColumns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlExtDataGetColumns(extDataBaseUrlExtDataGetColumnsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtData', 'baseUrlExtDataGetColumns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extDataBaseUrlExtDataGetSchemaBodyParam = {
      Table: 'string'
    };
    describe('#baseUrlExtDataGetSchema - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlExtDataGetSchema(extDataBaseUrlExtDataGetSchemaBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtData', 'baseUrlExtDataGetSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extDataBaseUrlExtDataSetColumnBodyParam = {
      ID: 'string',
      Value: 'string',
      Column: 'string',
      Table: 'string'
    };
    describe('#baseUrlExtDataSetColumn - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlExtDataSetColumn(extDataBaseUrlExtDataSetColumnBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtData', 'baseUrlExtDataSetColumn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extDataBaseUrlExtDataSetColumnsBodyParam = {
      Columns: 'string',
      ID: 'string',
      Table: 'string'
    };
    describe('#baseUrlExtDataSetColumns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlExtDataSetColumns(extDataBaseUrlExtDataSetColumnsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtData', 'baseUrlExtDataSetColumns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extDataBaseUrlExtDataUpdateSchemaBodyParam = {
      Columns: [
        {
          Title: '<string>',
          Type: '<string>',
          Description: '<string>',
          Name: '<string>'
        }
      ],
      Table: 'string'
    };
    describe('#baseUrlExtDataUpdateSchema - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlExtDataUpdateSchema(extDataBaseUrlExtDataUpdateSchemaBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtData', 'baseUrlExtDataUpdateSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const externalCaMgmtBaseUrlExternalCaMgmtAddCertAuthorityBodyParam = {
      externalCa: 'string'
    };
    describe('#baseUrlExternalCaMgmtAddCertAuthority - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlExternalCaMgmtAddCertAuthority(externalCaMgmtBaseUrlExternalCaMgmtAddCertAuthorityBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalCaMgmt', 'baseUrlExternalCaMgmtAddCertAuthority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const externalCaMgmtExternalCaId = 'fakedata';
    describe('#baseUrlExternalCaMgmtDownloadCertAuthority - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlExternalCaMgmtDownloadCertAuthority(externalCaMgmtExternalCaId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalCaMgmt', 'baseUrlExternalCaMgmtDownloadCertAuthority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExternalCaMgmtGetCertAuthorities - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlExternalCaMgmtGetCertAuthorities((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalCaMgmt', 'baseUrlExternalCaMgmtGetCertAuthorities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExternalCaMgmtRemoveCertAuthority - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlExternalCaMgmtRemoveCertAuthority(externalCaMgmtExternalCaId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalCaMgmt', 'baseUrlExternalCaMgmtRemoveCertAuthority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const externalCaMgmtBaseUrlExternalCaMgmtUpdateCertAuthorityBodyParam = {
      externalCa: 'string'
    };
    describe('#baseUrlExternalCaMgmtUpdateCertAuthority - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlExternalCaMgmtUpdateCertAuthority(externalCaMgmtExternalCaId, externalCaMgmtBaseUrlExternalCaMgmtUpdateCertAuthorityBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalCaMgmt', 'baseUrlExternalCaMgmtUpdateCertAuthority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationAddGlobalGroupAssertionMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationAddGlobalGroupAssertionMapping((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationAddGlobalGroupAssertionMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const federationBaseUrlFederationCreateFederationBodyParam = {
      FederationName: 'string',
      FederationType: 'string',
      Mappings: [
        'string'
      ],
      Domains: [
        'string'
      ]
    };
    describe('#baseUrlFederationCreateFederation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationCreateFederation(federationBaseUrlFederationCreateFederationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationCreateFederation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const federationBaseUrlFederationDeleteFederationBodyParam = {
      FederationUuid: 'string'
    };
    describe('#baseUrlFederationDeleteFederation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationDeleteFederation(federationBaseUrlFederationDeleteFederationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationDeleteFederation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationDeleteGlobalGroupAssertionMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationDeleteGlobalGroupAssertionMapping((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationDeleteGlobalGroupAssertionMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const federationBaseUrlFederationFederationMetadataBodyParam = {
      domain: 'string',
      FederationType: 'string'
    };
    describe('#baseUrlFederationFederationMetadata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationFederationMetadata(federationBaseUrlFederationFederationMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationFederationMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const federationBaseUrlFederationGetFederatedGroupMembersBodyParam = {
      Group: 'string',
      PageNumber: 'string',
      PageSize: 'string'
    };
    describe('#baseUrlFederationGetFederatedGroupMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationGetFederatedGroupMembers(federationBaseUrlFederationGetFederatedGroupMembersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationGetFederatedGroupMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const federationBaseUrlFederationGetFederatedGroupsForUserBodyParam = {
      User: 'string'
    };
    describe('#baseUrlFederationGetFederatedGroupsForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationGetFederatedGroupsForUser(federationBaseUrlFederationGetFederatedGroupsForUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationGetFederatedGroupsForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const federationBaseUrlFederationGetFederationBodyParam = {
      FederationUuid: 'string'
    };
    describe('#baseUrlFederationGetFederation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationGetFederation(federationBaseUrlFederationGetFederationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationGetFederation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const federationBaseUrlFederationGetFederationGroupAssertionMappingsBodyParam = {
      FederationUuid: 'string'
    };
    describe('#baseUrlFederationGetFederationGroupAssertionMappings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationGetFederationGroupAssertionMappings(federationBaseUrlFederationGetFederationGroupAssertionMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationGetFederationGroupAssertionMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetFederationTypes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationGetFederationTypes((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationGetFederationTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetFederations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationGetFederations((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationGetFederations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const federationBaseUrlFederationGetGlobalFederationSettingsBodyParam = {
      FederationType: 'string'
    };
    describe('#baseUrlFederationGetGlobalFederationSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationGetGlobalFederationSettings(federationBaseUrlFederationGetGlobalFederationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationGetGlobalFederationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetGlobalGroupAssertionMappings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationGetGlobalGroupAssertionMappings((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationGetGlobalGroupAssertionMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationGetGroups((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationGetGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const federationBaseUrlFederationRemoveUserFromFederatedGroupBodyParam = {
      Group: 'string',
      User: 'string',
      Federation: 'string'
    };
    describe('#baseUrlFederationRemoveUserFromFederatedGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationRemoveUserFromFederatedGroup(federationBaseUrlFederationRemoveUserFromFederatedGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationRemoveUserFromFederatedGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationSPSigningCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationSPSigningCertificate((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationSPSigningCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationSPSigningCertificateAuthority - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationSPSigningCertificateAuthority((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationSPSigningCertificateAuthority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const federationBaseUrlFederationUpdateFederationBodyParam = {
      FederationName: 'string',
      FederationType: 'string',
      Mappings: [
        'string'
      ],
      Domains: [
        'string'
      ],
      FederationUuid: 'string'
    };
    describe('#baseUrlFederationUpdateFederation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationUpdateFederation(federationBaseUrlFederationUpdateFederationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationUpdateFederation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const federationBaseUrlFederationUpdateFederationGroupAssertionMappingsBodyParam = {
      FederationUuid: 'string'
    };
    describe('#baseUrlFederationUpdateFederationGroupAssertionMappings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationUpdateFederationGroupAssertionMappings(federationBaseUrlFederationUpdateFederationGroupAssertionMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationUpdateFederationGroupAssertionMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationUpdateGlobalGroupAssertionMappings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlFederationUpdateGlobalGroupAssertionMappings((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Federation', 'baseUrlFederationUpdateGlobalGroupAssertionMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const googleDirectoryCode = 'fakedata';
    const googleDirectoryState = 'fakedata';
    const googleDirectoryError = 'fakedata';
    describe('#baseUrlGoogleDirectoryAuthCallback - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlGoogleDirectoryAuthCallback(googleDirectoryCode, googleDirectoryState, googleDirectoryError, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GoogleDirectory', 'baseUrlGoogleDirectoryAuthCallback', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const googleDirectoryPollingToken = 'fakedata';
    describe('#baseUrlGoogleDirectoryGetAuthTokenState - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlGoogleDirectoryGetAuthTokenState(googleDirectoryPollingToken, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GoogleDirectory', 'baseUrlGoogleDirectoryGetAuthTokenState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const googleDirectoryDirectoryServiceUuid = 'fakedata';
    describe('#baseUrlGoogleDirectoryGetDirectoryServiceConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlGoogleDirectoryGetDirectoryServiceConfig(googleDirectoryDirectoryServiceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GoogleDirectory', 'baseUrlGoogleDirectoryGetDirectoryServiceConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const googleDirectoryBaseUrlGoogleDirectoryGetServiceLoginUrlInfoBodyParam = {
      DirectoryServiceUuid: 'string',
      DomainName: 'string'
    };
    describe('#baseUrlGoogleDirectoryGetServiceLoginUrlInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlGoogleDirectoryGetServiceLoginUrlInfo(googleDirectoryBaseUrlGoogleDirectoryGetServiceLoginUrlInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GoogleDirectory', 'baseUrlGoogleDirectoryGetServiceLoginUrlInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlGoogleDirectoryRemoveDirectoryService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlGoogleDirectoryRemoveDirectoryService(googleDirectoryDirectoryServiceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GoogleDirectory', 'baseUrlGoogleDirectoryRemoveDirectoryService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const googleDirectoryBaseUrlGoogleDirectoryUpdateDirectoryServiceConfigBodyParam = {
      TrustedRedirectUris: [
        'string'
      ],
      IdpName: 'string',
      CustomConfigEnabled: 'string',
      ApplicationClientId: 'string',
      ApplicationClientSecret: 'string'
    };
    describe('#baseUrlGoogleDirectoryUpdateDirectoryServiceConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlGoogleDirectoryUpdateDirectoryServiceConfig(googleDirectoryDirectoryServiceUuid, googleDirectoryBaseUrlGoogleDirectoryUpdateDirectoryServiceConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GoogleDirectory', 'baseUrlGoogleDirectoryUpdateDirectoryServiceConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobFlowBaseUrlJobFlowDeleteJobBodyParam = {
      jobid: 'string'
    };
    describe('#baseUrlJobFlowDeleteJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlJobFlowDeleteJob(jobFlowBaseUrlJobFlowDeleteJobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobFlow', 'baseUrlJobFlowDeleteJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobFlowBaseUrlJobFlowEventBodyParam = {
      jobid: 'string',
      sync: 'string',
      args: 'string',
      event: 'string'
    };
    describe('#baseUrlJobFlowEvent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlJobFlowEvent(jobFlowBaseUrlJobFlowEventBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobFlow', 'baseUrlJobFlowEvent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobFlowBaseUrlJobFlowGetJobBodyParam = {
      jobid: 'string'
    };
    describe('#baseUrlJobFlowGetJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlJobFlowGetJob(jobFlowBaseUrlJobFlowGetJobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobFlow', 'baseUrlJobFlowGetJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobFlowBaseUrlJobFlowGetJobsBodyParam = {
      type: 'string'
    };
    describe('#baseUrlJobFlowGetJobs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlJobFlowGetJobs(jobFlowBaseUrlJobFlowGetJobsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobFlow', 'baseUrlJobFlowGetJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobFlowBaseUrlJobFlowGetMyJobsBodyParam = {
      type: 'string'
    };
    describe('#baseUrlJobFlowGetMyJobs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlJobFlowGetMyJobs(jobFlowBaseUrlJobFlowGetMyJobsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobFlow', 'baseUrlJobFlowGetMyJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobFlowBaseUrlJobFlowStartJobBodyParam = {
      args: 'string',
      script: 'string'
    };
    describe('#baseUrlJobFlowStartJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlJobFlowStartJob(jobFlowBaseUrlJobFlowStartJobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobFlow', 'baseUrlJobFlowStartJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jsManageRowKey = 'fakedata';
    describe('#baseUrlJsManageGetDashboardRolesAndRights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlJsManageGetDashboardRolesAndRights(jsManageRowKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JsManage', 'baseUrlJsManageGetDashboardRolesAndRights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlJsManageGetReportRolesAndRights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlJsManageGetReportRolesAndRights(jsManageRowKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JsManage', 'baseUrlJsManageGetReportRolesAndRights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const kmipBaseUrlKmipConfigureBodyParam = {
      issueClientCerts: 'string',
      kport: 'string',
      kaddress: 'string',
      serverCaCert: 'string',
      clientPkc12OrPemCert: 'string'
    };
    describe('#baseUrlKmipConfigure - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlKmipConfigure(kmipBaseUrlKmipConfigureBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Kmip', 'baseUrlKmipConfigure', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlKmipDeleteConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlKmipDeleteConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Kmip', 'baseUrlKmipDeleteConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlKmipGetConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlKmipGetConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Kmip', 'baseUrlKmipGetConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlKmipPasswordStoredRemotely - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlKmipPasswordStoredRemotely((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Kmip', 'baseUrlKmipPasswordStoredRemotely', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceAddLDAPDirectoryServiceConfigBodyParam = {
      UniqueIdentifier: 'string',
      UsePagedSearch: 'string',
      UseBrokenShadowExpire: 'string',
      PropertyToAttributeMap: 'string',
      ServerType: 'string',
      ScriptingPropertyToAttributeMap: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceAddLDAPDirectoryServiceConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceAddLDAPDirectoryServiceConfig(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceAddLDAPDirectoryServiceConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceAddLDAPDirectoryServiceConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceDeleteLDAPDirectoryServiceConfigBodyParam = {
      DirectoryServiceUuid: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceDeleteLDAPDirectoryServiceConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceDeleteLDAPDirectoryServiceConfig(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceDeleteLDAPDirectoryServiceConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceDeleteLDAPDirectoryServiceConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetCloudConnectorsBodyParam = {
      DirectoryServiceUuid: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceGetCloudConnectors - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetCloudConnectors(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetCloudConnectorsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceGetCloudConnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetDirectoryServiceVersionBodyParam = {
      directoryServiceUuid: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceGetDirectoryServiceVersion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetDirectoryServiceVersion(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetDirectoryServiceVersionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceGetDirectoryServiceVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceConfigBodyParam = {
      DirectoryServiceUuid: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceConfig(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceUuidByNameBodyParam = {
      DirectoryServiceName: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceUuidByName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceUuidByName(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceUuidByNameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceUuidByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetMappableAttributeListBodyParam = {
      operation: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceGetMappableAttributeList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetMappableAttributeList(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetMappableAttributeListBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceGetMappableAttributeList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetPropertyToAttributeMappingsBodyParam = {
      directoryServiceUuid: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceGetPropertyToAttributeMappings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetPropertyToAttributeMappings(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetPropertyToAttributeMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceGetPropertyToAttributeMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetScriptingPropertyToAttributeMappingsBodyParam = {
      directoryServiceUuid: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceGetScriptingPropertyToAttributeMappings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetScriptingPropertyToAttributeMappings(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceGetScriptingPropertyToAttributeMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceGetScriptingPropertyToAttributeMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceModifyLDAPDirectoryServiceConfigBodyParam = {
      UniqueIdentifier: 'string',
      UsePagedSearch: 'string',
      UseBrokenShadowExpire: 'string',
      PropertyToAttributeMap: 'string',
      ServerType: 'string',
      ScriptingPropertyToAttributeMap: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceModifyLDAPDirectoryServiceConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceModifyLDAPDirectoryServiceConfig(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceModifyLDAPDirectoryServiceConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceModifyLDAPDirectoryServiceConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceSetPropertyToAttributeMappingsBodyParam = {
      directoryServiceUuid: 'string',
      propertyToAttributeMappings: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceSetPropertyToAttributeMappings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceSetPropertyToAttributeMappings(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceSetPropertyToAttributeMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceSetPropertyToAttributeMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceSetScriptingPropertyToAttributeMappingsBodyParam = {
      scriptingPropertyToAttributeMappings: 'string',
      directoryServiceUuid: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceSetScriptingPropertyToAttributeMappings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceSetScriptingPropertyToAttributeMappings(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceSetScriptingPropertyToAttributeMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceSetScriptingPropertyToAttributeMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceTestUserLookupBodyParam = {
      Version: 'string',
      TestUserName: 'string',
      DirectoryServiceUuid: 'string',
      Settings: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceTestUserLookup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceTestUserLookup(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceTestUserLookupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceTestUserLookup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceVerifyLDAPDirectoryServiceConfigBodyParam = {
      UniqueIdentifier: 'string',
      UsePagedSearch: 'string',
      UseBrokenShadowExpire: 'string',
      PropertyToAttributeMap: 'string',
      ServerType: 'string',
      ScriptingPropertyToAttributeMap: 'string'
    };
    describe('#baseUrlLDAPDirectoryServiceVerifyLDAPDirectoryServiceConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceVerifyLDAPDirectoryServiceConfig(lDAPDirectoryServiceBaseUrlLDAPDirectoryServiceVerifyLDAPDirectoryServiceConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectoryService', 'baseUrlLDAPDirectoryServiceVerifyLDAPDirectoryServiceConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mobileDeviceID = 'fakedata';
    describe('#baseUrlMobileDeleteDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileDeleteDevice(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileDeleteDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileDisableSSO - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileDisableSSO(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileDisableSSO', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileEnableSSO - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileEnableSSO(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileEnableSSO', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mobileBaseUrlMobileGetGlobalDevicePermissionsBodyParam = {
      RRFormat: false
    };
    describe('#baseUrlMobileGetGlobalDevicePermissions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileGetGlobalDevicePermissions(mobileBaseUrlMobileGetGlobalDevicePermissionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileGetGlobalDevicePermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileKnoxResetContainerPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileKnoxResetContainerPassword(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileKnoxResetContainerPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileLockClientApp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileLockClientApp(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileLockClientApp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileLockDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileLockDevice(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileLockDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobilePingDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobilePingDevice(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobilePingDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobilePowerOff - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobilePowerOff(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobilePowerOff', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileReapplyDevicePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileReapplyDevicePolicy(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileReapplyDevicePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileReboot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileReboot(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileReboot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileRemoveDeviceProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileRemoveDeviceProfile(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileRemoveDeviceProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileResetClientAppLockPin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileResetClientAppLockPin(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileResetClientAppLockPin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mobileBaseUrlMobileSetDevicePermissionsBodyParam = {
      ID: 'string',
      Grants: {
        Rights: '<string>',
        Principal: '<string>',
        PrincipalId: '<string>',
        PType: '<string>'
      }
    };
    describe('#baseUrlMobileSetDevicePermissions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileSetDevicePermissions(mobileBaseUrlMobileSetDevicePermissionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileSetDevicePermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileSetPrimaryDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileSetPrimaryDevice(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileSetPrimaryDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileUnlockDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileUnlockDevice(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileUnlockDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileUpdateDevicePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileUpdateDevicePolicy(mobileDeviceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileUpdateDevicePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mobilePasscode = 'fakedata';
    describe('#baseUrlMobileWipeDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlMobileWipeDevice(mobileDeviceID, mobilePasscode, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mobile', 'baseUrlMobileWipeDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oathBaseUrlOathAddOrUpdateProfileBodyParam = {
      UserProfile: {
        IsImportByAdmin: '<boolean>',
        OathType: '<string>',
        Version: '<string>',
        Period: '<integer>',
        Digits: '<integer>',
        DeviceId: '<string>',
        AuthEndPointUrl: '<string>',
        SafeSecret: samProps.authentication.password,
        AccountName: '<string>',
        IsCma: '<boolean>',
        Issuer: '<string>',
        Uuid: '<string>',
        Algorithm: '<string>',
        Counter: '<integer>',
        UserUuid: '<string>',
        IntervalDelta: 0
      }
    };
    describe('#baseUrlOathAddOrUpdateProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathAddOrUpdateProfile(oathBaseUrlOathAddOrUpdateProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathAddOrUpdateProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oathUserToken = 'fakedata';
    describe('#baseUrlOathCentrifyOathOtpProfileCheck - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathCentrifyOathOtpProfileCheck(oathUserToken, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathCentrifyOathOtpProfileCheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oathBaseUrlOathDeleteProfilesBodyParam = {
      Uuids: [
        'string'
      ]
    };
    describe('#baseUrlOathDeleteProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathDeleteProfiles(oathBaseUrlOathDeleteProfilesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathDeleteProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oathBaseUrlOathGetDataFromCsvFileBodyParam = {
      FileName: 'string'
    };
    describe('#baseUrlOathGetDataFromCsvFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathGetDataFromCsvFile(oathBaseUrlOathGetDataFromCsvFileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathGetDataFromCsvFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathGetImportProfileList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathGetImportProfileList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathGetImportProfileList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathGetProfileList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathGetProfileList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathGetProfileList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathGetProfileListForDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathGetProfileListForDevice((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathGetProfileListForDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oathBaseUrlOathResetCentrifyOathProfileBodyParam = {
      Uuid: 'string'
    };
    describe('#baseUrlOathResetCentrifyOathProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathResetCentrifyOathProfile(oathBaseUrlOathResetCentrifyOathProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathResetCentrifyOathProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oathFirstCode = 'fakedata';
    const oathSecondCode = 'fakedata';
    const oathTokenId = 'fakedata';
    const oathBaseUrlOathResynchronizeOathTokenBodyParam = {
      firstCode: 'string',
      secondCode: 'string',
      tokenId: 'string'
    };
    describe('#baseUrlOathResynchronizeOathToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathResynchronizeOathToken(oathFirstCode, oathSecondCode, oathTokenId, oathBaseUrlOathResynchronizeOathTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathResynchronizeOathToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oathBaseUrlOathSaveProfileBodyParam = {
      Uuid: 'string',
      UserProfile: {
        IsImportByAdmin: '<boolean>',
        OathType: '<string>',
        Version: '<string>',
        Period: '<integer>',
        Digits: '<integer>',
        DeviceId: '<string>',
        AuthEndPointUrl: '<string>',
        SafeSecret: samProps.authentication.password,
        AccountName: '<string>',
        IsCma: '<boolean>',
        Issuer: '<string>',
        Uuid: '<string>',
        Algorithm: '<string>',
        Counter: '<integer>',
        UserUuid: '<string>',
        IntervalDelta: 0
      }
    };
    describe('#baseUrlOathSaveProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathSaveProfile(oathBaseUrlOathSaveProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathSaveProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oathOathProfileDe = 'fakedata';
    const oathShowSecret = 'fakedata';
    describe('#baseUrlOathSetResponseParamsToEntity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathSetResponseParamsToEntity(oathOathProfileDe, oathShowSecret, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathSetResponseParamsToEntity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oathBaseUrlOathSubmitUploadedFileBodyParam = {
      ReturnID: 'string',
      AdminEmail: 'string'
    };
    describe('#baseUrlOathSubmitUploadedFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathSubmitUploadedFile(oathBaseUrlOathSubmitUploadedFileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathSubmitUploadedFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oathBaseUrlOathUpdateOathProfileCounterBodyParam = {
      Uuid: 'string',
      Counter: 'string'
    };
    describe('#baseUrlOathUpdateOathProfileCounter - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathUpdateOathProfileCounter(oathBaseUrlOathUpdateOathProfileCounterBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathUpdateOathProfileCounter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oathUuid = 'fakedata';
    const oathOtpCode = 'fakedata';
    const oathUseOathDefaults = 'fakedata';
    describe('#baseUrlOathValidateOtpCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOathValidateOtpCode(oathUuid, oathOtpCode, oathUseOathDefaults, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Oath', 'baseUrlOathValidateOtpCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuth2Bounce = 'fakedata';
    const oAuth2BaseUrlOAuth2AuthorizeBodyParam = {};
    describe('#baseUrlOAuth2Authorize - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOAuth2Authorize(oAuth2Bounce, oAuth2BaseUrlOAuth2AuthorizeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth2', 'baseUrlOAuth2Authorize', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuth2Result = 'fakedata';
    const oAuth2Scopes = 'fakedata';
    describe('#baseUrlOAuth2Confirm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOAuth2Confirm(oAuth2Bounce, oAuth2Result, oAuth2Scopes, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth2', 'baseUrlOAuth2Confirm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuth2PostLogoutRedirectUri = 'fakedata';
    const oAuth2State = 'fakedata';
    const oAuth2IdTokenHint = 'fakedata';
    describe('#baseUrlOAuth2EndSession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOAuth2EndSession(oAuth2PostLogoutRedirectUri, oAuth2State, oAuth2IdTokenHint, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth2', 'baseUrlOAuth2EndSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuth2ServiceName = 'fakedata';
    const oAuth2BaseUrlOAuth2GetMetaBodyParam = {
      customerid: 'string'
    };
    describe('#baseUrlOAuth2GetMeta - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOAuth2GetMeta(oAuth2ServiceName, oAuth2BaseUrlOAuth2GetMetaBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth2', 'baseUrlOAuth2GetMeta', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuth2BaseUrlOAuth2IntrospectBodyParam = {
      token: 'string'
    };
    describe('#baseUrlOAuth2Introspect - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOAuth2Introspect(oAuth2BaseUrlOAuth2IntrospectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth2', 'baseUrlOAuth2Introspect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuth2BaseUrlOAuth2KeysBodyParam = {
      customerid: 'string'
    };
    describe('#baseUrlOAuth2Keys - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOAuth2Keys(oAuth2BaseUrlOAuth2KeysBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth2', 'baseUrlOAuth2Keys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOAuth2Revoke - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOAuth2Revoke((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth2', 'baseUrlOAuth2Revoke', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuth2BaseUrlOAuth2TokenBodyParam = {
      grant_type: 'string'
    };
    describe('#baseUrlOAuth2Token - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOAuth2Token(oAuth2BaseUrlOAuth2TokenBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth2', 'baseUrlOAuth2Token', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuth2BaseUrlOAuth2UserInfoBodyParam = {
      Scopes: [
        'string'
      ]
    };
    describe('#baseUrlOAuth2UserInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOAuth2UserInfo(oAuth2BaseUrlOAuth2UserInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth2', 'baseUrlOAuth2UserInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const radiusIsQueryResponse = 'fakedata';
    describe('#baseUrlRadiusGetClients - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRadiusGetClients(radiusIsQueryResponse, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Radius', 'baseUrlRadiusGetClients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const radiusConnectorUuid = 'fakedata';
    describe('#baseUrlRadiusGetConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRadiusGetConfig(radiusConnectorUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Radius', 'baseUrlRadiusGetConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRadiusGetServers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRadiusGetServers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Radius', 'baseUrlRadiusGetServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRadiusGetUserIdentifierAttributes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRadiusGetUserIdentifierAttributes((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Radius', 'baseUrlRadiusGetUserIdentifierAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const radiusBaseUrlRadiusRemoveClientsBodyParam = {
      clients: [
        'string'
      ]
    };
    describe('#baseUrlRadiusRemoveClients - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRadiusRemoveClients(radiusBaseUrlRadiusRemoveClientsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Radius', 'baseUrlRadiusRemoveClients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const radiusBaseUrlRadiusRemoveServersBodyParam = {
      HostAddress: [
        'string'
      ]
    };
    describe('#baseUrlRadiusRemoveServers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRadiusRemoveServers(radiusBaseUrlRadiusRemoveServersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Radius', 'baseUrlRadiusRemoveServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const radiusBaseUrlRadiusSetClientBodyParam = {
      ClientAddress: 'string',
      ClientCulture: 'string',
      ClientName: 'string',
      ClientDescription: 'string',
      ClientSecret: 'string',
      ClientAllowNewLinesInPrompts: 'string'
    };
    describe('#baseUrlRadiusSetClient - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRadiusSetClient(radiusBaseUrlRadiusSetClientBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Radius', 'baseUrlRadiusSetClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const radiusBaseUrlRadiusSetConfigBodyParam = {
      Port: 'string',
      connectorUuid: 'string',
      ReceiveTimeout: 'string'
    };
    describe('#baseUrlRadiusSetConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRadiusSetConfig(radiusBaseUrlRadiusSetConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Radius', 'baseUrlRadiusSetConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const radiusBaseUrlRadiusSetServerBodyParam = {
      HostDescription: 'string',
      HostDisplayName: 'string',
      UserIdSubstAttr: 'string',
      HostAddress: 'string',
      HostCustomPromptMechChosen: 'string',
      HostSharedSecret: 'string',
      UserIdCustomSubstAttrName: 'string',
      HostPort: 'string',
      ReceiveTimeout: 'string',
      MaxAttempts: 'string'
    };
    describe('#baseUrlRadiusSetServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRadiusSetServer(radiusBaseUrlRadiusSetServerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Radius', 'baseUrlRadiusSetServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const registrationBaseUrlRegistrationCustomerInfoBodyParam = {
      pass: 'string',
      user: 'string'
    };
    describe('#baseUrlRegistrationCustomerInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRegistrationCustomerInfo(registrationBaseUrlRegistrationCustomerInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Registration', 'baseUrlRegistrationCustomerInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const registrationBaseUrlRegistrationRegisterNewTenantBodyParam = {
      AdminUser: 'string',
      Brand: 'string',
      InitialAlias: 'string',
      CustomerName: 'string',
      AdminPass: 'string',
      CustomerPass: 'string',
      ManagedBy: 'string',
      InitialStateChangeReason: 'string',
      Company: 'string',
      IsMsp: 'string',
      EntitlementIdsToEnable: [
        'string'
      ],
      CustomerTenantData: 'string',
      CustomerData: 'string',
      AuthSource: 'string',
      CustomerToken: 'string',
      AdditionalConfigSettings: 'string',
      AwsCustomerId: 'string'
    };
    describe('#baseUrlRegistrationRegisterNewTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRegistrationRegisterNewTenant(registrationBaseUrlRegistrationRegisterNewTenantBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Registration', 'baseUrlRegistrationRegisterNewTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rolesBaseUrlRolesGetPagedRoleMembersBodyParam = {
      Name: 'string',
      FilterValue: 'string',
      PageNumber: 'string',
      Ascending: 'string',
      FilterBy: [
        'string'
      ],
      PageSize: 'string',
      SortBy: 'string'
    };
    describe('#baseUrlRolesGetPagedRoleMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRolesGetPagedRoleMembers(rolesBaseUrlRolesGetPagedRoleMembersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'baseUrlRolesGetPagedRoleMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rolesBaseUrlRolesUpdateRoleBodyParam = {
      Name: 'string',
      CheckPrincipalTypes: [
        'string'
      ],
      Roles: 'string',
      Users: 'string',
      Groups: 'string',
      Description: 'string'
    };
    describe('#baseUrlRolesUpdateRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlRolesUpdateRole(rolesBaseUrlRolesUpdateRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'baseUrlRolesUpdateRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageBaseUrlSaasManageAddUsersAndGroupsToRoleBodyParam = {
      Name: 'string',
      CheckPrincipalTypes: [
        'string'
      ],
      Roles: [
        'string'
      ],
      Users: [
        'string'
      ],
      Groups: [
        'string'
      ]
    };
    describe('#baseUrlSaasManageAddUsersAndGroupsToRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageAddUsersAndGroupsToRole(saasManageBaseUrlSaasManageAddUsersAndGroupsToRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageAddUsersAndGroupsToRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageBaseUrlSaasManageDeleteApplicationBodyParam = {
      _RowKey: [
        'string'
      ]
    };
    describe('#baseUrlSaasManageDeleteApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageDeleteApplication(saasManageBaseUrlSaasManageDeleteApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageDeleteApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageName = 'fakedata';
    describe('#baseUrlSaasManageDeleteRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageDeleteRole(saasManageName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageDeleteRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageBaseUrlSaasManageDeleteRolesBodyParam = [
      'string'
    ];
    describe('#baseUrlSaasManageDeleteRoles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageDeleteRoles(saasManageBaseUrlSaasManageDeleteRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageDeleteRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageGetAppIDByServiceName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageGetAppIDByServiceName(saasManageName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageGetAppIDByServiceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageRowKey = 'fakedata';
    describe('#baseUrlSaasManageGetApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageGetApplication(saasManageRowKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageGetApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageSuppressPrincipalsList = 'fakedata';
    describe('#baseUrlSaasManageGetRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageGetRole(saasManageName, saasManageSuppressPrincipalsList, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageGetRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageGetRoleMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageGetRoleMembers(saasManageName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageGetRoleMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageGetTemplatesAndCategories - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageGetTemplatesAndCategories((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageGetTemplatesAndCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageBaseUrlSaasManageImportAppFromTemplateBodyParam = {
      ID: [
        'string'
      ]
    };
    describe('#baseUrlSaasManageImportAppFromTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageImportAppFromTemplate(saasManageBaseUrlSaasManageImportAppFromTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageImportAppFromTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageAppKey = 'fakedata';
    describe('#baseUrlSaasManageIsApplicationAvailableInCatalog - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageIsApplicationAvailableInCatalog(saasManageAppKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageIsApplicationAvailableInCatalog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageBaseUrlSaasManageRemoveUsersAndGroupsFromRoleBodyParam = {
      Name: 'string',
      Roles: [
        'string'
      ],
      Users: [
        'string'
      ],
      Groups: [
        'string'
      ]
    };
    describe('#baseUrlSaasManageRemoveUsersAndGroupsFromRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageRemoveUsersAndGroupsFromRole(saasManageBaseUrlSaasManageRemoveUsersAndGroupsFromRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageRemoveUsersAndGroupsFromRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageBaseUrlSaasManageSetApplicationPermissionsBodyParam = {
      ID: 'string',
      Grants: {
        Rights: '<string>',
        Principal: '<string>',
        PrincipalId: '<string>',
        PType: '<string>'
      }
    };
    describe('#baseUrlSaasManageSetApplicationPermissions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageSetApplicationPermissions(saasManageBaseUrlSaasManageSetApplicationPermissionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageSetApplicationPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageBaseUrlSaasManageStoreRoleBodyParam = {
      Name: 'string',
      CheckPrincipalTypes: [
        'string'
      ],
      Roles: [
        'string'
      ],
      Users: [
        'string'
      ],
      Groups: [
        'string'
      ],
      Description: 'string'
    };
    describe('#baseUrlSaasManageStoreRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageStoreRole(saasManageBaseUrlSaasManageStoreRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageStoreRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageBaseUrlSaasManageUpdateApplicationDEBodyParam = {
      _RowKey: 'string',
      ShowRegistration: 'string',
      Handler: 'string',
      IconUri: 'string',
      AppRoles: 'string',
      Description: 'string',
      Name: 'string'
    };
    describe('#baseUrlSaasManageUpdateApplicationDE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageUpdateApplicationDE(saasManageBaseUrlSaasManageUpdateApplicationDEBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageUpdateApplicationDE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saasManageBaseUrlSaasManageUpdateRoleBodyParam = {
      Name: 'string',
      CheckPrincipalTypes: [
        'string'
      ],
      Roles: [
        'string'
      ],
      Users: [
        'string'
      ],
      Groups: [
        'string'
      ],
      Description: 'string'
    };
    describe('#baseUrlSaasManageUpdateRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSaasManageUpdateRole(saasManageBaseUrlSaasManageUpdateRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaasManage', 'baseUrlSaasManageUpdateRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulerHistoryJobid = 'fakedata';
    describe('#baseUrlSchedulerHistoryDeleteJobHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSchedulerHistoryDeleteJobHistory(schedulerHistoryJobid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SchedulerHistory', 'baseUrlSchedulerHistoryDeleteJobHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulerHistoryJobId = 'fakedata';
    describe('#baseUrlSchedulerHistoryGetJobReport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSchedulerHistoryGetJobReport(schedulerHistoryJobId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SchedulerHistory', 'baseUrlSchedulerHistoryGetJobReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityAdvanceAuthenticationBodyParam = {
      Action: 'string',
      SessionId: 'string',
      MechanismId: 'string',
      TenantId: 'string',
      PersistentLogin: 'string',
      Answer: 'string'
    };
    describe('#baseUrlSecurityAdvanceAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityAdvanceAuthentication(securityBaseUrlSecurityAdvanceAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityAdvanceAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityAdvanceForgotUsernameBodyParam = {
      TenantId: 'string',
      Action: 'string',
      Answer: 'string'
    };
    describe('#baseUrlSecurityAdvanceForgotUsername - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityAdvanceForgotUsername(securityBaseUrlSecurityAdvanceForgotUsernameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityAdvanceForgotUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityAmIAuthenticatedBodyParam = {};
    describe('#baseUrlSecurityAmIAuthenticated - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityAmIAuthenticated(securityBaseUrlSecurityAmIAuthenticatedBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityAmIAuthenticated', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityAnswer = 'fakedata';
    const securityBaseUrlSecurityAnswerOOBChallengeBodyParam = {};
    describe('#baseUrlSecurityAnswerOOBChallenge - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityAnswerOOBChallenge(securityAnswer, securityBaseUrlSecurityAnswerOOBChallengeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityAnswerOOBChallenge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityProfileName = 'fakedata';
    describe('#baseUrlSecurityChallengeUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityChallengeUser(securityProfileName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityChallengeUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityCleanupAuthenticationBodyParam = {
      TenantId: 'string',
      SessionId: 'string'
    };
    describe('#baseUrlSecurityCleanupAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityCleanupAuthentication(securityBaseUrlSecurityCleanupAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityCleanupAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityDoIHaveRight - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityDoIHaveRight((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityDoIHaveRight', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityForgotUsernameBodyParam = {
      SearchKey: 'string'
    };
    describe('#baseUrlSecurityForgotUsername - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityForgotUsername(securityBaseUrlSecurityForgotUsernameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityForgotUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityUse = 'fakedata';
    describe('#baseUrlSecurityGetOneTimePassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityGetOneTimePassword(securityUse, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityGetOneTimePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityGetRiskAnalysisLevels - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityGetRiskAnalysisLevels((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityGetRiskAnalysisLevels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securitySystemID = 'fakedata';
    const securityUser = 'fakedata';
    const securityPassword = 'fakedata';
    const securityPersist = 'fakedata';
    const securityBaseUrlSecurityLoginBodyParam = {};
    describe('#baseUrlSecurityLogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityLogin(securitySystemID, securityUser, securityPassword, securityPersist, securityBaseUrlSecurityLoginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityRedirectUrl = 'fakedata';
    const securityAllowIWA = 'fakedata';
    const securityBaseUrlSecurityLogoutBodyParam = {};
    describe('#baseUrlSecurityLogout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityLogout(securityRedirectUrl, securityAllowIWA, securityBaseUrlSecurityLogoutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityLogout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityCustomerId = 'fakedata';
    const securityElevate = 'fakedata';
    const securityBaseUrlSecurityMultiAuthLoginBodyParam = {
      nextMechId: 'string',
      RecId: 'string',
      PFType: 'string'
    };
    describe('#baseUrlSecurityMultiAuthLogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityMultiAuthLogin(securityUser, securityCustomerId, securityPersist, securityElevate, securityBaseUrlSecurityMultiAuthLoginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityMultiAuthLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityOnDemandChallengeBodyParam = {
      User: 'string',
      PolicyModifier: 'string'
    };
    describe('#baseUrlSecurityOnDemandChallenge - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityOnDemandChallenge(securityBaseUrlSecurityOnDemandChallengeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityOnDemandChallenge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityRefreshToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityRefreshToken((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityRefreshToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityExtIdpAuthChallengeState = 'fakedata';
    const securityBaseUrlSecurityResumeFromExtIdpAuthBodyParam = {
      ExtIdpAuthChallengeState: 'string'
    };
    describe('#baseUrlSecurityResumeFromExtIdpAuth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityResumeFromExtIdpAuth(securityExtIdpAuthChallengeState, securityBaseUrlSecurityResumeFromExtIdpAuthBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityResumeFromExtIdpAuth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityStartAuthenticationBodyParam = {
      Version: 'string',
      User: 'string',
      TenantId: 'string',
      ApplicationId: 'string',
      MfaRequestor: 'string'
    };
    describe('#baseUrlSecurityStartAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityStartAuthentication(securityBaseUrlSecurityStartAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityStartAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityStartChallengeBodyParam = {
      Version: 'string',
      ChallengeStateId: 'string'
    };
    describe('#baseUrlSecurityStartChallenge - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityStartChallenge(securityBaseUrlSecurityStartChallengeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityStartChallenge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityStartForgotUsernameBodyParam = {
      Version: 'string'
    };
    describe('#baseUrlSecurityStartForgotUsername - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityStartForgotUsername(securityBaseUrlSecurityStartForgotUsernameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityStartForgotUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityStartSocialAuthenticationBodyParam = {
      PostExtIdpAuthCallbackUrl: 'string',
      IdpName: 'string'
    };
    describe('#baseUrlSecurityStartSocialAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityStartSocialAuthentication(securityBaseUrlSecurityStartSocialAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityStartSocialAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityOtpCode = 'fakedata';
    const securityUserUuid = 'fakedata';
    const securityBaseUrlSecuritySubmitOathOtpCodeBodyParam = {
      otpCode: 'string',
      userUuid: 'string'
    };
    describe('#baseUrlSecuritySubmitOathOtpCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecuritySubmitOathOtpCode(securityOtpCode, securityUserUuid, securityBaseUrlSecuritySubmitOathOtpCodeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecuritySubmitOathOtpCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityTask = 'fakedata';
    describe('#baseUrlSecurityTaskCheck - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityTaskCheck(securityTask, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityTaskCheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityTaskChecksBodyParam = {
      tasks: [
        'string'
      ]
    };
    describe('#baseUrlSecurityTaskChecks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityTaskChecks(securityBaseUrlSecurityTaskChecksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityTaskChecks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityTwilioPhoneChallengeCompletedBodyParam = {
      ResponseXml: 'string'
    };
    describe('#baseUrlSecurityTwilioPhoneChallengeCompleted - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityTwilioPhoneChallengeCompleted(securityBaseUrlSecurityTwilioPhoneChallengeCompletedBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityTwilioPhoneChallengeCompleted', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityBaseUrlSecurityTwilioPhoneChallengeNotAnsweredBodyParam = {
      ResponseXml: 'string'
    };
    describe('#baseUrlSecurityTwilioPhoneChallengeNotAnswered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityTwilioPhoneChallengeNotAnswered(securityBaseUrlSecurityTwilioPhoneChallengeNotAnsweredBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityTwilioPhoneChallengeNotAnswered', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityChallenge = 'fakedata';
    describe('#baseUrlSecurityWhoAmI - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSecurityWhoAmI(securityChallenge, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'baseUrlSecurityWhoAmI', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverAgentBaseUrlServerAgentAddEnrollmentCodeBodyParam = {
      NeverExpire: 'string',
      MaxUseCount: 'string',
      IPRange: [
        'string'
      ],
      Description: 'string',
      NoMaxUseCount: 'string',
      UseCount: 'string',
      SetID: [
        'string'
      ],
      OwnerType: 'string',
      ConnectorID: [
        'string'
      ],
      Owner: 'string',
      OwnerID: 'string',
      SetName: [
        'string'
      ]
    };
    describe('#baseUrlServerAgentAddEnrollmentCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentAddEnrollmentCode(serverAgentBaseUrlServerAgentAddEnrollmentCodeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentAddEnrollmentCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverAgentBaseUrlServerAgentDeleteEnrollmentCodeBodyParam = {
      EnrollmentCode: 'string'
    };
    describe('#baseUrlServerAgentDeleteEnrollmentCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentDeleteEnrollmentCode(serverAgentBaseUrlServerAgentDeleteEnrollmentCodeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentDeleteEnrollmentCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverAgentBaseUrlServerAgentDisableFeaturesBodyParam = {};
    describe('#baseUrlServerAgentDisableFeatures - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentDisableFeatures(serverAgentBaseUrlServerAgentDisableFeaturesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentDisableFeatures', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentEnableFeatures - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentEnableFeatures((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentEnableFeatures', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverAgentBaseUrlServerAgentEnableFeaturesV2BodyParam = {
      Features: 'string',
      Uuid: 'string',
      ResourceName: 'string',
      AccountName: 'string',
      AgentAuthRoles: 'string'
    };
    describe('#baseUrlServerAgentEnableFeaturesV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentEnableFeaturesV2(serverAgentBaseUrlServerAgentEnableFeaturesV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentEnableFeaturesV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentEnroll - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentEnroll((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentEnroll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverAgentBaseUrlServerAgentEnrollV2BodyParam = {
      OperatingSystem: 'string',
      Name: 'string',
      AgentVersion: 'string',
      FQDN: 'string',
      ResourceSetting: 'string',
      Owner: 'string',
      CertificatePassword: 'string',
      ResourcePolicy: 'string',
      DeviceID: 'string',
      ResourceName: 'string',
      Suffix: 'string',
      Overwrite: true
    };
    describe('#baseUrlServerAgentEnrollV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentEnrollV2(serverAgentBaseUrlServerAgentEnrollV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentEnrollV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverAgentBaseUrlServerAgentGetAllEnrollmentCodesBodyParam = {
      RRFormat: true
    };
    describe('#baseUrlServerAgentGetAllEnrollmentCodes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentGetAllEnrollmentCodes(serverAgentBaseUrlServerAgentGetAllEnrollmentCodesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentGetAllEnrollmentCodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverAgentBaseUrlServerAgentGetCertificateBodyParam = {
      CertificatePassword: 'string',
      ServerAuthentication: true
    };
    describe('#baseUrlServerAgentGetCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentGetCertificate(serverAgentBaseUrlServerAgentGetCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentGetCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentRegister - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentRegister((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentRegister', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverAgentBaseUrlServerAgentRegisterV2BodyParam = {
      OperatingSystem: 'string',
      Name: 'string',
      AgentVersion: 'string',
      FQDN: 'string',
      ResourceSetting: 'string',
      EnrollmentCode: 'string',
      Owner: 'string',
      CertificatePassword: 'string',
      ResourcePolicy: 'string',
      DeviceID: 'string',
      ResourceName: 'string',
      Suffix: 'string',
      Overwrite: false
    };
    describe('#baseUrlServerAgentRegisterV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentRegisterV2(serverAgentBaseUrlServerAgentRegisterV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentRegisterV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverAgentBaseUrlServerAgentUnenrollBodyParam = {
      Delete: 'string',
      Uuid: 'string',
      ResourceName: 'string',
      AccountName: 'string'
    };
    describe('#baseUrlServerAgentUnenroll - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentUnenroll(serverAgentBaseUrlServerAgentUnenrollBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentUnenroll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverAgentBaseUrlServerAgentVerifyPasswordV2BodyParam = {
      Password: 'string',
      Uuid: 'string',
      AllowPasswordExpiration: 'string'
    };
    describe('#baseUrlServerAgentVerifyPasswordV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerAgentVerifyPasswordV2(serverAgentBaseUrlServerAgentVerifyPasswordV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerAgent', 'baseUrlServerAgentVerifyPasswordV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManageAddAccountBodyParam = {
      Password: 'string',
      SshKeyId: 'string',
      Host: 'string',
      DomainID: 'string',
      DatabaseID: 'string',
      User: 'string',
      IsManaged: false,
      CaseSensitiveCheck: 'string',
      UseWheel: true,
      Description: 'string'
    };
    describe('#baseUrlServerManageAddAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManageAddAccount(serverManageBaseUrlServerManageAddAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManageAddAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManageCheckinPasswordBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlServerManageCheckinPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManageCheckinPassword(serverManageBaseUrlServerManageCheckinPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManageCheckinPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManageCheckoutPasswordBodyParam = {
      ID: 'string',
      Lifetime: 'string',
      Description: 'string'
    };
    describe('#baseUrlServerManageCheckoutPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManageCheckoutPassword(serverManageBaseUrlServerManageCheckoutPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManageCheckoutPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManageDeleteAccountBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlServerManageDeleteAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManageDeleteAccount(serverManageBaseUrlServerManageDeleteAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManageDeleteAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManageExtendCheckoutBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlServerManageExtendCheckout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManageExtendCheckout(serverManageBaseUrlServerManageExtendCheckoutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManageExtendCheckout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManageGetAccountPermissionsBodyParam = {
      ID: 'string',
      RRFormat: false
    };
    describe('#baseUrlServerManageGetAccountPermissions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManageGetAccountPermissions(serverManageBaseUrlServerManageGetAccountPermissionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManageGetAccountPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManageGetRetiredPasswordBodyParam = {
      ID: 'string'
    };
    describe('#baseUrlServerManageGetRetiredPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManageGetRetiredPassword(serverManageBaseUrlServerManageGetRetiredPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManageGetRetiredPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManagePreCheckoutBodyParam = {
      PVID: 'string'
    };
    describe('#baseUrlServerManagePreCheckout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManagePreCheckout(serverManageBaseUrlServerManagePreCheckoutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManagePreCheckout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManageSetAccountPermissionsBodyParam = {
      Grants: {
        Rights: '<string>',
        Principal: '<string>',
        PrincipalId: '<string>',
        PType: '<string>'
      },
      PVID: 'string'
    };
    describe('#baseUrlServerManageSetAccountPermissions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManageSetAccountPermissions(serverManageBaseUrlServerManageSetAccountPermissionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManageSetAccountPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManageSetDomainPermissionsBodyParam = {
      ID: 'string',
      Grants: {
        Rights: '<string>',
        Principal: '<string>',
        PrincipalId: '<string>',
        PType: '<string>'
      }
    };
    describe('#baseUrlServerManageSetDomainPermissions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManageSetDomainPermissions(serverManageBaseUrlServerManageSetDomainPermissionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManageSetDomainPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManageUpdateAccountBodyParam = {
      WorkflowSent: 'string',
      WorkflowApprover: {
        DirectoryServiceUuid: '<string>',
        ObjectType: '<string>',
        Name: '<string>',
        SystemName: '<string>',
        ServiceInstanceLocalized: '<string>',
        Enabled: '<boolean>',
        StatusEnum: '<string>',
        Status: '<string>',
        DistinguishedName: '<string>',
        ServiceInstance: '<string>',
        Type: '<string>',
        DisplayName: '<string>',
        InternalName: '<string>',
        Guid: '<string>',
        PType: '<string>',
        EMail: '<string>',
        Principal: '<string>',
        BackupApprover: '<object>',
        Locked: '<boolean>',
        ServiceType: '<string>'
      },
      Description: 'string',
      WorkflowApprovers: [
        'string'
      ],
      IsManaged: 'string',
      DatabaseID: 'string',
      UseWheel: 'string',
      DomainID: 'string',
      Host: 'string',
      WorkflowEnabled: 'string',
      User: 'string',
      BackupApprover: 'string',
      ID: 'string',
      WorkflowApproversList: [
        'string'
      ],
      WorkflowDefaultOptions: 'string',
      NoManagerAction: 'string'
    };
    describe('#baseUrlServerManageUpdateAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManageUpdateAccount(serverManageBaseUrlServerManageUpdateAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManageUpdateAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverManageBaseUrlServerManageUpdatePasswordBodyParam = {
      Password: 'string',
      ID: 'string'
    };
    describe('#baseUrlServerManageUpdatePassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlServerManageUpdatePassword(serverManageBaseUrlServerManageUpdatePasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerManage', 'baseUrlServerManageUpdatePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const socialAuthCode = 'fakedata';
    const socialAuthState = 'fakedata';
    describe('#baseUrlSocialAuthFacebookAuthCallback - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthFacebookAuthCallback(socialAuthCode, socialAuthState, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuth', 'baseUrlSocialAuthFacebookAuthCallback', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthGoogleAuthCallback - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthGoogleAuthCallback(socialAuthCode, socialAuthState, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuth', 'baseUrlSocialAuthGoogleAuthCallback', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthLinkedInAuthCallback - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthLinkedInAuthCallback(socialAuthCode, socialAuthState, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuth', 'baseUrlSocialAuthLinkedInAuthCallback', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthMicrosoftActAuthCallback - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthMicrosoftActAuthCallback(socialAuthCode, socialAuthState, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuth', 'baseUrlSocialAuthMicrosoftActAuthCallback', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const socialAuthOauthToken = 'fakedata';
    const socialAuthOauthVerifier = 'fakedata';
    describe('#baseUrlSocialAuthTwitterAuthCallback - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthTwitterAuthCallback(socialAuthOauthToken, socialAuthOauthVerifier, socialAuthState, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuth', 'baseUrlSocialAuthTwitterAuthCallback', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const socialAuthMgmtBaseUrlSocialAuthMgmtGetAllCustomConfigBodyParam = {
      GetSecretAsPlainText: false
    };
    describe('#baseUrlSocialAuthMgmtGetAllCustomConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthMgmtGetAllCustomConfig(socialAuthMgmtBaseUrlSocialAuthMgmtGetAllCustomConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuthMgmt', 'baseUrlSocialAuthMgmtGetAllCustomConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const socialAuthMgmtBaseUrlSocialAuthMgmtGetApplicationClientSecretBodyParam = {
      ApplicationClientId: 'string',
      IdpName: 'string'
    };
    describe('#baseUrlSocialAuthMgmtGetApplicationClientSecret - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthMgmtGetApplicationClientSecret(socialAuthMgmtBaseUrlSocialAuthMgmtGetApplicationClientSecretBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuthMgmt', 'baseUrlSocialAuthMgmtGetApplicationClientSecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthMgmtGetAuthConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthMgmtGetAuthConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuthMgmt', 'baseUrlSocialAuthMgmtGetAuthConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const socialAuthMgmtBaseUrlSocialAuthMgmtGetCustomConfigBodyParam = {
      IdpName: 'string',
      GetSecretAsPlainText: false
    };
    describe('#baseUrlSocialAuthMgmtGetCustomConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthMgmtGetCustomConfig(socialAuthMgmtBaseUrlSocialAuthMgmtGetCustomConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuthMgmt', 'baseUrlSocialAuthMgmtGetCustomConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthMgmtResetAuthConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthMgmtResetAuthConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuthMgmt', 'baseUrlSocialAuthMgmtResetAuthConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const socialAuthMgmtBaseUrlSocialAuthMgmtSetAuthConfigBodyParam = {
      EnableMicrosoftLogin: 'string',
      Version: 'string',
      EnableTwitterLogin: 'string',
      EnableGoogleLogin: 'string',
      EnableLinkedInLogin: 'string',
      EnableFacebookLogin: 'string'
    };
    describe('#baseUrlSocialAuthMgmtSetAuthConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthMgmtSetAuthConfig(socialAuthMgmtBaseUrlSocialAuthMgmtSetAuthConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuthMgmt', 'baseUrlSocialAuthMgmtSetAuthConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const socialAuthMgmtBaseUrlSocialAuthMgmtSetCustomConfigBodyParam = {
      TrustedRedirectUris: [
        'string'
      ],
      IdpName: 'string',
      CustomConfigEnabled: 'string',
      ApplicationClientSecret: 'string'
    };
    describe('#baseUrlSocialAuthMgmtSetCustomConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSocialAuthMgmtSetCustomConfig(socialAuthMgmtBaseUrlSocialAuthMgmtSetCustomConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SocialAuthMgmt', 'baseUrlSocialAuthMgmtSetCustomConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSysInfoAbout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSysInfoAbout((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SysInfo', 'baseUrlSysInfoAbout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSysInfoDummy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSysInfoDummy((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SysInfo', 'baseUrlSysInfoDummy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSysInfoGetMySession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSysInfoGetMySession((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SysInfo', 'baseUrlSysInfoGetMySession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSysInfoVersion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlSysInfoVersion((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SysInfo', 'baseUrlSysInfoVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskBaseUrlTaskCancelJobBodyParam = {
      jobId: 'string',
      reason: 'string'
    };
    describe('#baseUrlTaskCancelJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTaskCancelJob(taskBaseUrlTaskCancelJobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'baseUrlTaskCancelJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskBaseUrlTaskCreateOneTimeJobBodyParam = {
      ScriptArgs: 'string',
      TimeoutSeconds: 'string',
      ScriptPath: 'string',
      ScheduledTimeTicks: 'string',
      Description: 'string',
      Name: 'string'
    };
    describe('#baseUrlTaskCreateOneTimeJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTaskCreateOneTimeJob(taskBaseUrlTaskCreateOneTimeJobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'baseUrlTaskCreateOneTimeJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskBaseUrlTaskEmailReportBodyParam = {
      format: 'string',
      parameters: 'string',
      emailTo: 'string',
      scriptPath: 'string'
    };
    describe('#baseUrlTaskEmailReport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTaskEmailReport(taskBaseUrlTaskEmailReportBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'baseUrlTaskEmailReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskBaseUrlTaskGetJobHistoryBodyParam = {
      Args: {
        FilterValue: '<string>',
        PageNumber: 1,
        Token: null,
        FilterQuery: {
          JobHidden: '<boolean>'
        },
        PageSize: 100
      }
    };
    describe('#baseUrlTaskGetJobHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTaskGetJobHistory(taskBaseUrlTaskGetJobHistoryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'baseUrlTaskGetJobHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskJobId = 'fakedata';
    describe('#baseUrlTaskGetSingleJobHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTaskGetSingleJobHistory(taskJobId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'baseUrlTaskGetSingleJobHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskHoursBack = 'fakedata';
    describe('#baseUrlTaskJobReport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTaskJobReport(taskHoursBack, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'baseUrlTaskJobReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantCnamesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantCnamesGet((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantCnames', 'baseUrlTenantCnamesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantCnamesGetDomainInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantCnamesGetDomainInfo((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantCnames', 'baseUrlTenantCnamesGetDomainInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantCnamesCnamePrefix = 'fakedata';
    describe('#baseUrlTenantCnamesRegister - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantCnamesRegister(tenantCnamesCnamePrefix, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantCnames', 'baseUrlTenantCnamesRegister', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantCnamesCustomCname = 'fakedata';
    describe('#baseUrlTenantCnamesSetPreferred - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantCnamesSetPreferred(tenantCnamesCustomCname, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantCnames', 'baseUrlTenantCnamesSetPreferred', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantCnamesUiGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantCnamesUiGet((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantCnames', 'baseUrlTenantCnamesUiGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantCnamesUnRegister - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantCnamesUnRegister(tenantCnamesCustomCname, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantCnames', 'baseUrlTenantCnamesUnRegister', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigId = 'fakedata';
    const tenantConfigBaseUrlTenantConfigDeleteAdminSecurityQuestionBodyParam = {
      Id: 'string'
    };
    describe('#baseUrlTenantConfigDeleteAdminSecurityQuestion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigDeleteAdminSecurityQuestion(tenantConfigId, tenantConfigBaseUrlTenantConfigDeleteAdminSecurityQuestionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigDeleteAdminSecurityQuestion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigBaseUrlTenantConfigDeleteAdvancedConfigBodyParam = {
      tenantId: 'string',
      key: 'string'
    };
    describe('#baseUrlTenantConfigDeleteAdvancedConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigDeleteAdvancedConfig(tenantConfigBaseUrlTenantConfigDeleteAdvancedConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigDeleteAdvancedConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetAdminSecurityQuestion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigGetAdminSecurityQuestion(tenantConfigId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigGetAdminSecurityQuestion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetAdminSecurityQuestions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigGetAdminSecurityQuestions((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigGetAdminSecurityQuestions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetAdvancedConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigGetAdvancedConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigGetAdvancedConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetCustomerConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigGetCustomerConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigGetCustomerConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetEditableMailTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigGetEditableMailTemplates((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigGetEditableMailTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigTemplateName = 'fakedata';
    const tenantConfigTemplateType = 'fakedata';
    describe('#baseUrlTenantConfigGetEditableMessageTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigGetEditableMessageTemplate(tenantConfigTemplateName, tenantConfigTemplateType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigGetEditableMessageTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetEditableMessageTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigGetEditableMessageTemplates((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigGetEditableMessageTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetGoogleKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigGetGoogleKey((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigGetGoogleKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetMobileConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigGetMobileConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigGetMobileConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetSMTPConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigGetSMTPConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigGetSMTPConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetTwilioConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigGetTwilioConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigGetTwilioConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigResetPortalConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigResetPortalConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigResetPortalConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigTemplatePath = 'fakedata';
    describe('#baseUrlTenantConfigSendTestMessageTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigSendTestMessageTemplate(tenantConfigTemplatePath, tenantConfigTemplateType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigSendTestMessageTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigBaseUrlTenantConfigSetAdminSecurityQuestionBodyParam = {
      Question: 'string',
      Culture: 'string'
    };
    describe('#baseUrlTenantConfigSetAdminSecurityQuestion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigSetAdminSecurityQuestion(tenantConfigBaseUrlTenantConfigSetAdminSecurityQuestionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigSetAdminSecurityQuestion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigBaseUrlTenantConfigSetAdvancedConfigBodyParam = {
      tenantId: 'string',
      value: 'string',
      key: 'string'
    };
    describe('#baseUrlTenantConfigSetAdvancedConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigSetAdvancedConfig(tenantConfigBaseUrlTenantConfigSetAdvancedConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigSetAdvancedConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigBaseUrlTenantConfigSetCustomerConfigBodyParam = {
      OtpCodeLength: 'string',
      Icon: 'string',
      EnableUmc: 'string',
      SendPasswordChangeConfirmation: 'string',
      LoginBannerMessage: 'string',
      LoginSampleText: 'string',
      PortalImage: 'string',
      LoginBannerMessageL10nEnabled: 'string',
      ThemeColor: 'string',
      GlobalImage: 'string',
      MfaAttributeMapping: [
        'string'
      ],
      LoginBackgroundImage: 'string',
      CompanyName: 'string',
      IsOriginValidationEnabled: 'string',
      WelcomeMessage: 'string',
      EmailImage: 'string',
      LoginImage: 'string',
      IsOriginValidationOnGetEnabled: 'string',
      ForgotUsernameAllowed: 'string',
      NavigationColor: 'string',
      IsPasswordPersistanceEnabled: 'string',
      FastSearchEnabledEntities: [
        'string'
      ],
      PrivacyPolicyLink: 'string',
      AllowCors: [
        'string'
      ],
      BackgroundColor: 'string',
      CustomerCompany: 'string',
      DisplayLoginBanner: 'string',
      TermsOfUseLink: 'string'
    };
    describe('#baseUrlTenantConfigSetCustomerConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigSetCustomerConfig(tenantConfigBaseUrlTenantConfigSetCustomerConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigSetCustomerConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigBaseUrlTenantConfigSetGoogleKeyBodyParam = {
      googleKey: 'string',
      GoogleKeyEnabled: 'string'
    };
    describe('#baseUrlTenantConfigSetGoogleKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigSetGoogleKey(tenantConfigBaseUrlTenantConfigSetGoogleKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigSetGoogleKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigBaseUrlTenantConfigSetMobileConfigBodyParam = {
      CompanyName: 'string',
      WelcomeMessage: 'string',
      EndpointEnrollWelcomeTextL10nEnabled: 'string',
      CompanyImageBackgroundColor: 'string',
      CompanyImage: 'string',
      LocalizationMappings: [
        'string'
      ]
    };
    describe('#baseUrlTenantConfigSetMobileConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigSetMobileConfig(tenantConfigBaseUrlTenantConfigSetMobileConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigSetMobileConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigValue = 'fakedata';
    describe('#baseUrlTenantConfigSetPasswordPersistance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigSetPasswordPersistance(tenantConfigValue, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigSetPasswordPersistance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigBaseUrlTenantConfigSetSMTPConfigBodyParam = {
      Port: 'string',
      Host: 'string',
      SSL: 'string',
      Pass: 'string',
      User: 'string',
      SmtpConfigEnabled: 'string',
      UseConnectors: 'string',
      Connectors: [
        'string'
      ]
    };
    describe('#baseUrlTenantConfigSetSMTPConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigSetSMTPConfig(tenantConfigBaseUrlTenantConfigSetSMTPConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigSetSMTPConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigBaseUrlTenantConfigSetTwilioConfigBodyParam = {
      Sid: 'string',
      Token: 'string',
      FromNumber: 'string',
      SmsConfigEnabled: 'string'
    };
    describe('#baseUrlTenantConfigSetTwilioConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigSetTwilioConfig(tenantConfigBaseUrlTenantConfigSetTwilioConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigSetTwilioConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigBaseUrlTenantConfigTestSMTPConfigBodyParam = {
      Port: 'string',
      Host: 'string',
      SSL: 'string',
      Pass: 'string',
      User: 'string',
      SmtpConfigEnabled: 'string',
      UseConnectors: 'string',
      Connectors: [
        'string'
      ]
    };
    describe('#baseUrlTenantConfigTestSMTPConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigTestSMTPConfig(tenantConfigBaseUrlTenantConfigTestSMTPConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigTestSMTPConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantConfigBaseUrlTenantConfigTestTwilioConfigBodyParam = {
      Sid: 'string',
      Token: 'string',
      FromNumber: 'string',
      SmsConfigEnabled: 'string'
    };
    describe('#baseUrlTenantConfigTestTwilioConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlTenantConfigTestTwilioConfig(tenantConfigBaseUrlTenantConfigTestTwilioConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantConfig', 'baseUrlTenantConfigTestTwilioConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const u2fRawRegisterResponse = 'fakedata';
    const u2fBaseUrlU2fAnswerRegistrationChallengeBodyParam = {
      ClientData: 'string',
      RegistrationData: 'string',
      Challenge: 'string'
    };
    describe('#baseUrlU2fAnswerRegistrationChallenge - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlU2fAnswerRegistrationChallenge(u2fRawRegisterResponse, u2fBaseUrlU2fAnswerRegistrationChallengeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('U2f', 'baseUrlU2fAnswerRegistrationChallenge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const u2fKeyHandle = 'fakedata';
    describe('#baseUrlU2fDeleteU2fDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlU2fDeleteU2fDevice(u2fKeyHandle, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('U2f', 'baseUrlU2fDeleteU2fDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const u2fBaseUrlU2fDeleteU2fDevicesBodyParam = {
      KeyHandles: [
        'string'
      ]
    };
    describe('#baseUrlU2fDeleteU2fDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlU2fDeleteU2fDevices(u2fBaseUrlU2fDeleteU2fDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('U2f', 'baseUrlU2fDeleteU2fDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const u2fBaseUrlU2fFacetsBodyParam = {};
    describe('#baseUrlU2fFacets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlU2fFacets(u2fBaseUrlU2fFacetsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('U2f', 'baseUrlU2fFacets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const u2fUserDefinedName = 'fakedata';
    const u2fAuthenticatortype = 'fakedata';
    const u2fBaseUrlU2fGetRegistrationChallengeBodyParam = {};
    describe('#baseUrlU2fGetRegistrationChallenge - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlU2fGetRegistrationChallenge(u2fUserDefinedName, u2fAuthenticatortype, u2fBaseUrlU2fGetRegistrationChallengeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('U2f', 'baseUrlU2fGetRegistrationChallenge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlU2fGetU2fDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlU2fGetU2fDevices((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('U2f', 'baseUrlU2fGetU2fDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const u2fType = 'fakedata';
    describe('#baseUrlU2fGetU2fDevicesForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlU2fGetU2fDevicesForUser(u2fType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('U2f', 'baseUrlU2fGetU2fDevicesForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uPRestAppkey = 'fakedata';
    const uPRestBaseUrlUPRestGetAppByKeyBodyParam = {
      markAppVisited: true
    };
    describe('#baseUrlUPRestGetAppByKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUPRestGetAppByKey(uPRestAppkey, uPRestBaseUrlUPRestGetAppByKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UPRest', 'baseUrlUPRestGetAppByKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uPRestUserUuid = 'fakedata';
    describe('#baseUrlUPRestGetResultantAppsForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUPRestGetResultantAppsForUser(uPRestUserUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UPRest', 'baseUrlUPRestGetResultantAppsForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUPRestGetTagsForApp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUPRestGetTagsForApp(uPRestAppkey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UPRest', 'baseUrlUPRestGetTagsForApp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uPRestForce = 'fakedata';
    const uPRestUsername = 'fakedata';
    describe('#baseUrlUPRestGetUPData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUPRestGetUPData(uPRestForce, uPRestUsername, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UPRest', 'baseUrlUPRestGetUPData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uPRestBaseUrlUPRestSetUserCredsForAppBodyParam = {
      Password: 'string',
      appkey: 'string',
      Username: 'string'
    };
    describe('#baseUrlUPRestSetUserCredsForApp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUPRestSetUserCredsForApp(uPRestBaseUrlUPRestSetUserCredsForAppBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UPRest', 'baseUrlUPRestSetUserCredsForApp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uPRestBaseUrlUPRestUpsertTagsForAppBodyParam = {
      appkey: 'string',
      tagnames: [
        'string'
      ]
    };
    describe('#baseUrlUPRestUpsertTagsForApp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUPRestUpsertTagsForApp(uPRestBaseUrlUPRestUpsertTagsForAppBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UPRest', 'baseUrlUPRestUpsertTagsForApp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtAnalyzeAdaptiveMfaRisk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtAnalyzeAdaptiveMfaRisk((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtAnalyzeAdaptiveMfaRisk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtID = 'fakedata';
    const userMgmtDirectoryServiceUuid = 'fakedata';
    describe('#baseUrlUserMgmtCanEditUserAttributes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtCanEditUserAttributes(userMgmtID, userMgmtDirectoryServiceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtCanEditUserAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtChangeUserAttributesBodyParam = {
      ID: 'string',
      CmaRedirectedUserUuid: 'string'
    };
    describe('#baseUrlUserMgmtChangeUserAttributes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtChangeUserAttributes(userMgmtBaseUrlUserMgmtChangeUserAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtChangeUserAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtChangeUserPasswordBodyParam = {
      oldPassword: 'string',
      newPassword: 'string'
    };
    describe('#baseUrlUserMgmtChangeUserPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtChangeUserPassword(userMgmtBaseUrlUserMgmtChangeUserPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtChangeUserPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtCheckUserProfileChallenge - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtCheckUserProfileChallenge((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtCheckUserProfileChallenge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtDirectoryServiceQueryBodyParam = {
      roles: 'string',
      group: 'string',
      directoryServices: [
        'string'
      ],
      user: 'string'
    };
    describe('#baseUrlUserMgmtDirectoryServiceQuery - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtDirectoryServiceQuery(userMgmtBaseUrlUserMgmtDirectoryServiceQueryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtDirectoryServiceQuery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtUuidOrName = 'fakedata';
    describe('#baseUrlUserMgmtGetCachedEntity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtGetCachedEntity(userMgmtUuidOrName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtGetCachedEntity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetCachedUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtGetCachedUser(userMgmtUuidOrName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtGetCachedUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtId = 'fakedata';
    const userMgmtAddAdminQuestions = 'fakedata';
    const userMgmtBaseUrlUserMgmtGetSecurityQuestionsBodyParam = {
      addAdminQuestions: false,
      Id: 'string'
    };
    describe('#baseUrlUserMgmtGetSecurityQuestions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtGetSecurityQuestions(userMgmtId, userMgmtAddAdminQuestions, userMgmtBaseUrlUserMgmtGetSecurityQuestionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtGetSecurityQuestions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUserAttributes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtGetUserAttributes(userMgmtID, userMgmtDirectoryServiceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtGetUserAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtUser = 'fakedata';
    describe('#baseUrlUserMgmtGetUserCertificateInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtGetUserCertificateInfo(userMgmtUser, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtGetUserCertificateInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUserHierarchy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtGetUserHierarchy(userMgmtID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtGetUserHierarchy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUserInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtGetUserInfo(userMgmtID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtGetUserInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUserPicture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtGetUserPicture(userMgmtID, userMgmtDirectoryServiceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtGetUserPicture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUserPreferences - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtGetUserPreferences((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtGetUserPreferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUsersRolesAndAdministrativeRights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtGetUsersRolesAndAdministrativeRights(userMgmtId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtGetUsersRolesAndAdministrativeRights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtInviteUsersBodyParam = {
      Entities: [
        'string'
      ],
      EmailInvite: 'string',
      SmsInvite: 'string',
      Role: 'string',
      GroupInvite: 'string'
    };
    describe('#baseUrlUserMgmtInviteUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtInviteUsers(userMgmtBaseUrlUserMgmtInviteUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtInviteUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtIsUserCloudLocked - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtIsUserCloudLocked(userMgmtUser, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtIsUserCloudLocked', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtIsUserLockedOutByPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtIsUserLockedOutByPolicy(userMgmtUser, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtIsUserLockedOutByPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtIsUserSubjectToCloudLocks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtIsUserSubjectToCloudLocks(userMgmtUser, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtIsUserSubjectToCloudLocks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtRemoveUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtRemoveUser(userMgmtID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtRemoveUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtThumbprint = 'fakedata';
    const userMgmtCertType = 'fakedata';
    describe('#baseUrlUserMgmtRemoveUserCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtRemoveUserCertificate(userMgmtUser, userMgmtThumbprint, userMgmtCertType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtRemoveUserCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtRemoveUsersBodyParam = {
      Users: [
        'string'
      ]
    };
    describe('#baseUrlUserMgmtRemoveUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtRemoveUsers(userMgmtBaseUrlUserMgmtRemoveUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtRemoveUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtResetSecurityQuestionsBodyParam = {
      Id: 'string'
    };
    describe('#baseUrlUserMgmtResetSecurityQuestions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtResetSecurityQuestions(userMgmtBaseUrlUserMgmtResetSecurityQuestionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtResetSecurityQuestions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtResetUserPasswordBodyParam = {
      ID: 'string',
      newPassword: 'string'
    };
    describe('#baseUrlUserMgmtResetUserPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtResetUserPassword(userMgmtBaseUrlUserMgmtResetUserPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtResetUserPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtSendLoginEmail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtSendLoginEmail(userMgmtID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtSendLoginEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtSendLoginEmailsBodyParam = {
      ID: [
        'string'
      ]
    };
    describe('#baseUrlUserMgmtSendLoginEmails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtSendLoginEmails(userMgmtBaseUrlUserMgmtSendLoginEmailsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtSendLoginEmails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtSendSmsInvite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtSendSmsInvite(userMgmtID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtSendSmsInvite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtLockUser = 'fakedata';
    describe('#baseUrlUserMgmtSetCloudLock - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtSetCloudLock(userMgmtUser, userMgmtLockUser, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtSetCloudLock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtSetPhonePinBodyParam = {
      ID: 'string',
      phonepin: 'string'
    };
    describe('#baseUrlUserMgmtSetPhonePin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtSetPhonePin(userMgmtBaseUrlUserMgmtSetPhonePinBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtSetPhonePin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtSetSecurityQuestionBodyParam = {};
    describe('#baseUrlUserMgmtSetSecurityQuestion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtSetSecurityQuestion(userMgmtBaseUrlUserMgmtSetSecurityQuestionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtSetSecurityQuestion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtUserUuidOrName = 'fakedata';
    describe('#baseUrlUserMgmtUncacheUserPreferences - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtUncacheUserPreferences(userMgmtUserUuidOrName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtUncacheUserPreferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtUpdateSecurityQuestionsBodyParam = {
      Added: [
        'string'
      ],
      Id: 'string',
      Deleted: [
        'string'
      ],
      Replace: true
    };
    describe('#baseUrlUserMgmtUpdateSecurityQuestions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtUpdateSecurityQuestions(userMgmtBaseUrlUserMgmtUpdateSecurityQuestionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtUpdateSecurityQuestions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMgmtBaseUrlUserMgmtUpdateUserPreferencesBodyParam = {
      PreferredCulture: 'string'
    };
    describe('#baseUrlUserMgmtUpdateUserPreferences - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUserMgmtUpdateUserPreferences(userMgmtBaseUrlUserMgmtUpdateUserPreferencesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMgmt', 'baseUrlUserMgmtUpdateUserPreferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vfsPathParam = 'fakedata';
    const vfsBaseUrlVfsGetFileBodyParam = {};
    describe('#baseUrlVfsGetFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlVfsGetFile(vfsPathParam, vfsBaseUrlVfsGetFileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vfs', 'baseUrlVfsGetFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlVfsGetFileLower - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlVfsGetFileLower(vfsPathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vfs', 'baseUrlVfsGetFileLower', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zsoSessionId = 'fakedata';
    describe('#baseUrlZsoAuthenticateSession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlZsoAuthenticateSession(zsoSessionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zso', 'baseUrlZsoAuthenticateSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zsoRedirectUrl = 'fakedata';
    describe('#baseUrlZsoCertLogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlZsoCertLogin(zsoRedirectUrl, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zso', 'baseUrlZsoCertLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlZsoClearMacSafariZsoCookie - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlZsoClearMacSafariZsoCookie((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zso', 'baseUrlZsoClearMacSafariZsoCookie', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlZsoIsMacSafariZsoCookieSet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlZsoIsMacSafariZsoCookieSet((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zso', 'baseUrlZsoIsMacSafariZsoCookieSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlZsoIsSessionAuthenticated - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlZsoIsSessionAuthenticated(zsoSessionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zso', 'baseUrlZsoIsSessionAuthenticated', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlZsoSetMacSafariZsoCookie - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlZsoSetMacSafariZsoCookie((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zso', 'baseUrlZsoSetMacSafariZsoCookie', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlHealthCheck - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlHealthCheck((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IDaptiveIdentityPlatform', 'baseUrlHealthCheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iDaptiveIdentityPlatformCustomerid = 'fakedata';
    const iDaptiveIdentityPlatformDebug = 'fakedata';
    const iDaptiveIdentityPlatformBaseUrlHomeGetLoginDataBodyParam = {};
    describe('#baseUrlHomeGetLoginData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlHomeGetLoginData(iDaptiveIdentityPlatformCustomerid, iDaptiveIdentityPlatformDebug, iDaptiveIdentityPlatformBaseUrlHomeGetLoginDataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IDaptiveIdentityPlatform', 'baseUrlHomeGetLoginData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iDaptiveIdentityPlatformPathParam = 'fakedata';
    const iDaptiveIdentityPlatformBaseUrlLibOnlyVfsGetFileBodyParam = {};
    describe('#baseUrlLibOnlyVfsGetFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlLibOnlyVfsGetFile(iDaptiveIdentityPlatformPathParam, iDaptiveIdentityPlatformBaseUrlLibOnlyVfsGetFileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IDaptiveIdentityPlatform', 'baseUrlLibOnlyVfsGetFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iDaptiveIdentityPlatformBaseUrlOAuth2ManCreateClientTokenBodyParam = {
      AppID: 'string',
      ClientSecret: 'string',
      ClientID: 'string'
    };
    describe('#baseUrlOAuth2ManCreateClientToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlOAuth2ManCreateClientToken(iDaptiveIdentityPlatformBaseUrlOAuth2ManCreateClientTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IDaptiveIdentityPlatform', 'baseUrlOAuth2ManCreateClientToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iDaptiveIdentityPlatformRedirUrl = 'fakedata';
    describe('#baseUrlPKILogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlPKILogin(iDaptiveIdentityPlatformRedirUrl, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IDaptiveIdentityPlatform', 'baseUrlPKILogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iDaptiveIdentityPlatformShortUrlKey = 'fakedata';
    describe('#baseUrlUEvaluate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.baseUrlUEvaluate(iDaptiveIdentityPlatformShortUrlKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IDaptiveIdentityPlatform', 'baseUrlUEvaluate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const generalAddAllowedReferrerBodyParam = {
      referrerURL: 'string',
      regularExpression: false
    };
    describe('#addAllowedReferrer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAllowedReferrer(generalAddAllowedReferrerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('General', 'addAllowedReferrer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllowedReferrer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllowedReferrer((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('General', 'getAllowedReferrer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPDirectoriesCreateDirectoryBodyParam = {
      DirectoryType: 'string',
      HostAddresses: [
        'string'
      ],
      BindUsername: 'string',
      BindPassword: 'string',
      Port: 3,
      DomainName: 'string',
      DomainBaseContext: 'string'
    };
    describe('#createDirectory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDirectory(lDAPDirectoriesCreateDirectoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.DirectoryType);
                assert.equal(true, Array.isArray(data.response.HostAddresses));
                assert.equal('string', data.response.BindUsername);
                assert.equal('string', data.response.BindPassword);
                assert.equal(8, data.response.Port);
                assert.equal(false, data.response.SSLConnect);
                assert.equal('string', data.response.LDAPDirectoryName);
                assert.equal(4, data.response.LDAPDirectoryQueryOrder);
                assert.equal('string', data.response.LDAPDirectoryDescription);
                assert.equal('string', data.response.VaultObjectNamesPrefix);
                assert.equal('string', data.response.PasswordObjectPath);
                assert.equal('string', data.response.LDAPDirectoryGroupBaseContext);
                assert.equal(2, data.response.ReferralsChasingHopLimit);
                assert.equal(true, data.response.AppendFriendlyDomainNameToGroup);
                assert.equal(true, data.response.RequireReferredDirectoryDefinition);
                assert.equal(false, data.response.ReferralsDNSLookup);
                assert.equal(false, data.response.DisableUserEnumeration);
                assert.equal(true, data.response.AdditionalQueryFilterOptimize);
                assert.equal(false, data.response.ClientBrowsing);
                assert.equal(false, data.response.ExternalObjectCreation);
                assert.equal(true, data.response.Authentication);
                assert.equal(true, data.response.UseLDAPCertificatesOnly);
                assert.equal(false, data.response.DisablePaging);
                assert.equal(true, data.response.ProvisionDisabledUsers);
                assert.equal('string', data.response.LDAPDirectoryUsage);
                assert.equal('string', data.response.DCList);
                assert.equal('string', data.response.DomainName);
                assert.equal('string', data.response.DomainBaseContext);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectories', 'createDirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDirectories((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectories', 'getDirectories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectoryDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDirectoryDetails((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.DirectoryType);
                assert.equal('string', data.response.BindUsername);
                assert.equal('string', data.response.BindPassword);
                assert.equal(false, data.response.SSLConnect);
                assert.equal('string', data.response.LDAPDirectoryName);
                assert.equal(10, data.response.LDAPDirectoryQueryOrder);
                assert.equal('string', data.response.LDAPDirectoryDescription);
                assert.equal('string', data.response.VaultObjectNamesPrefix);
                assert.equal('string', data.response.PasswordObjectPath);
                assert.equal('string', data.response.LDAPDirectoryGroupBaseContext);
                assert.equal(2, data.response.ReferralsChasingHopLimit);
                assert.equal(false, data.response.AppendFriendlyDomainNameToGroup);
                assert.equal(true, data.response.RequireReferredDirectoryDefinition);
                assert.equal(false, data.response.ReferralsDNSLookup);
                assert.equal(true, data.response.DisableUserEnumeration);
                assert.equal(false, data.response.AdditionalQueryFilterOptimize);
                assert.equal(true, data.response.ClientBrowsing);
                assert.equal(false, data.response.ExternalObjectCreation);
                assert.equal(false, data.response.Authentication);
                assert.equal(false, data.response.UseLDAPCertificatesOnly);
                assert.equal(true, data.response.DisablePaging);
                assert.equal(false, data.response.ProvisionDisabledUsers);
                assert.equal(true, Array.isArray(data.response.LDAPDirectoryUsage));
                assert.equal(true, Array.isArray(data.response.DCList));
                assert.equal('string', data.response.DomainName);
                assert.equal('string', data.response.DomainBaseContext);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectories', 'getDirectoryDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPMappingsCreateDirectoryMappingBodyParam = {
      MappingName: 'string',
      LDAPBranch: 'string',
      DomainGroups: [
        'string'
      ],
      MappingAuthorizations: [
        1
      ]
    };
    describe('#createDirectoryMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDirectoryMapping(lDAPMappingsCreateDirectoryMappingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPMappings', 'createDirectoryMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPMappingsReorderDirectoryMappingsBodyParam = 'fakedata';
    describe('#reorderDirectoryMappings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reorderDirectoryMappings(lDAPMappingsReorderDirectoryMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPMappings', 'reorderDirectoryMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectoryMappingList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDirectoryMappingList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPMappings', 'getDirectoryMappingList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPMappingsEditDirectoryMappingBodyParam = {
      LDAPBranch: 'string',
      VaultGroups: [
        'string'
      ],
      MappingAuthorizations: [
        'string'
      ],
      Location: 'string',
      AuthenticationMethod: [
        'string'
      ],
      UserType: 'string',
      DisableUser: false,
      UserActivityLogPeriod: 6,
      UserExpiration: 6,
      LogonFromHour: 5,
      LogonToHour: 5,
      MappingID: 5,
      DirectoryMappingOrder: 2,
      MappingName: 'string',
      LDAPQuery: 'string',
      DomainGroups: [
        'string'
      ]
    };
    describe('#editDirectoryMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editDirectoryMapping(lDAPMappingsEditDirectoryMappingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPMappings', 'editDirectoryMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMappingDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMappingDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPMappings', 'getMappingDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sessionActionsResumeaSuspendedSessionBodyParam = {};
    describe('#resumeaSuspendedSession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resumeaSuspendedSession(sessionActionsResumeaSuspendedSessionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SessionActions', 'resumeaSuspendedSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sessionActionsSuspendanActiveSessionBodyParam = {};
    describe('#suspendanActiveSession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.suspendanActiveSession(sessionActionsSuspendanActiveSessionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SessionActions', 'suspendanActiveSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sessionActionsTerminateanActiveSessionBodyParam = {};
    describe('#terminateanActiveSession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.terminateanActiveSession(sessionActionsTerminateanActiveSessionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SessionActions', 'terminateanActiveSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#monitoraLiveSession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.monitoraLiveSession((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SessionActions', 'monitoraLiveSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#playRecording - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.playRecording((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Recordings', 'playRecording', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordingsLimit = 555;
    const recordingsSort = 'fakedata';
    const recordingsOffset = 555;
    const recordingsSearch = 'fakedata';
    const recordingsSafe = 'fakedata';
    const recordingsFromTime = 555;
    const recordingsToTime = 555;
    const recordingsActivities = 'fakedata';
    describe('#getRecordings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRecordings(recordingsLimit, recordingsSort, recordingsOffset, recordingsSearch, recordingsSafe, recordingsFromTime, recordingsToTime, recordingsActivities, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Recordings', 'getRecordings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecordingDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRecordingDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Recordings', 'getRecordingDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecordingActivities - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRecordingActivities((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Recordings', 'getRecordingActivities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecordingProperties - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRecordingProperties((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Recordings', 'getRecordingProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitorSessionsLimit = 555;
    const monitorSessionsSort = 'fakedata';
    const monitorSessionsOffset = 555;
    const monitorSessionsSearch = 'fakedata';
    const monitorSessionsSafe = 'fakedata';
    const monitorSessionsFromTime = 555;
    const monitorSessionsToTime = 555;
    const monitorSessionsActivities = 'fakedata';
    describe('#getLiveSessions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLiveSessions(monitorSessionsLimit, monitorSessionsSort, monitorSessionsOffset, monitorSessionsSearch, monitorSessionsSafe, monitorSessionsFromTime, monitorSessionsToTime, monitorSessionsActivities, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorSessions', 'getLiveSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLiveSessionDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLiveSessionDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorSessions', 'getLiveSessionDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLiveSessionActivities - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLiveSessionActivities((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorSessions', 'getLiveSessionActivities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLiveSessionProperties - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLiveSessionProperties((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorSessions', 'getLiveSessionProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const onboardingRulesAddOnboardingRuleBodyParam = {
      DecisionPlatformId: 'string',
      DecisionSafeName: 'string',
      IsAdminUIDFilter: 'string',
      MachineTypeFilter: 'string',
      SystemTypeFilter: 'string',
      UserNameFilter: 'string'
    };
    describe('#addOnboardingRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addOnboardingRule(onboardingRulesAddOnboardingRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OnboardingRules', 'addOnboardingRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const onboardingRulesId = 555;
    const onboardingRulesUpdateOnboardingRuleBodyParam = 'fakedata';
    describe('#updateOnboardingRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateOnboardingRule(onboardingRulesId, onboardingRulesUpdateOnboardingRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OnboardingRules', 'updateOnboardingRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOnboardingRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOnboardingRule((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OnboardingRules', 'getOnboardingRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountACLAddress = 'fakedata';
    const accountACLUserName = 'fakedata';
    const accountACLPolicyID = 'fakedata';
    const accountAddAccountACLBodyParam = {
      Command: 'string',
      CommandGroup: true,
      PermissionType: 'string',
      Restrictions: 'string',
      UserName: 'string'
    };
    describe('#addAccountACL - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAccountACL(accountAddAccountACLBodyParam, accountACLAddress, accountACLUserName, accountACLPolicyID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Account', 'addAccountACL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAccountACL - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAccountACL(accountACLAddress, accountACLUserName, accountACLPolicyID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Account', 'listAccountACL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateTargetPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateTargetPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TargetPlatforms', 'activateTargetPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateTargetPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateTargetPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TargetPlatforms', 'deactivateTargetPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const targetPlatformsDuplicateTargetPlatformsBodyParam = {
      Name: 'string',
      Description: 'string'
    };
    describe('#duplicateTargetPlatforms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.duplicateTargetPlatforms(targetPlatformsDuplicateTargetPlatformsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TargetPlatforms', 'duplicateTargetPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTargetPlatforms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTargetPlatforms((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Platforms));
                assert.equal(4, data.response.Total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TargetPlatforms', 'getTargetPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dependentPlatformsDuplicateDependentPlatformsBodyParam = {
      Name: 'string',
      Description: 'string'
    };
    describe('#duplicateDependentPlatforms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.duplicateDependentPlatforms(dependentPlatformsDuplicateDependentPlatformsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DependentPlatforms', 'duplicateDependentPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dependentPlatformsSearch = 'fakedata';
    describe('#getDependentPlatforms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDependentPlatforms(dependentPlatformsSearch, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DependentPlatforms', 'getDependentPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateGroupPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateGroupPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GroupPlatforms', 'activateGroupPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateGroupPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateGroupPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GroupPlatforms', 'deactivateGroupPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupPlatformsDuplicateGroupPlatformsBodyParam = {
      Name: 'string',
      Description: 'string'
    };
    describe('#duplicateGroupPlatforms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.duplicateGroupPlatforms(groupPlatformsDuplicateGroupPlatformsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GroupPlatforms', 'duplicateGroupPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupPlatformsSearch = 'fakedata';
    describe('#getGroupPlatforms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupPlatforms(groupPlatformsSearch, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Platforms));
                assert.equal(5, data.response.Total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GroupPlatforms', 'getGroupPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateRotationalGroupPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateRotationalGroupPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RotationalGroupPlatforms', 'activateRotationalGroupPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateRotationalGroupPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateRotationalGroupPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RotationalGroupPlatforms', 'deactivateRotationalGroupPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rotationalGroupPlatformsDuplicateRotationalGroupPlatformsBodyParam = {
      Name: 'string',
      Description: 'string'
    };
    describe('#duplicateRotationalGroupPlatforms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.duplicateRotationalGroupPlatforms(rotationalGroupPlatformsDuplicateRotationalGroupPlatformsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RotationalGroupPlatforms', 'duplicateRotationalGroupPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rotationalGroupPlatformsSearch = 'fakedata';
    describe('#getRotationalGroupPlatforms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRotationalGroupPlatforms(rotationalGroupPlatformsSearch, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Platforms));
                assert.equal(3, data.response.Total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RotationalGroupPlatforms', 'getRotationalGroupPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsImportPlatformBodyParam = 'fakedata';
    describe('#importPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importPlatform(platformsImportPlatformBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'importPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'exportPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsActive = true;
    const platformsPlatformType = 'fakedata';
    const platformsPlatformName = 'fakedata';
    describe('#getPlatforms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPlatforms(platformsActive, platformsPlatformType, platformsPlatformName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'getPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlatformDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPlatformDetails((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.PlatformID);
                assert.equal('object', typeof data.response.Details);
                assert.equal(true, data.response.active);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'getPlatformDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pTAServerAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pTAServerAuthentication((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PTAInstallation', 'pTAServerAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serverEncryptionKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.serverEncryptionKey((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PTAInstallation', 'serverEncryptionKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#installPTA - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.installPTA((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PTAInstallation', 'installPTA', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityEventsAddRiskyCommandsRuleBodyParam = {
      category: 'string',
      regex: 'string',
      score: 4,
      description: 'string',
      response: 'string',
      active: true,
      scope: {
        vaultUsers: {
          mode: 'EXCLUDE',
          list: [
            'john*'
          ]
        },
        machines: {
          mode: 'INCLUDE',
          list: [
            '*'
          ]
        }
      }
    };
    describe('#addRiskyCommandsRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addRiskyCommandsRule(securityEventsAddRiskyCommandsRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityEvents', 'addRiskyCommandsRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityEvents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSecurityEvents((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityEvents', 'getSecurityEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityEventsUpdateSecurityEventBodyParam = {
      mStatus: 'string'
    };
    describe('#updateSecurityEvent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSecurityEvent(securityEventsUpdateSecurityEventBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityEvents', 'updateSecurityEvent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecuritySettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSecuritySettings((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityEvents', 'getSecuritySettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSecurityRemediationSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSecurityRemediationSettings((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityEvents', 'updateSecurityRemediationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityEventsUpdateRiskyCommandsRuleBodyParam = {};
    describe('#updateRiskyCommandsRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRiskyCommandsRule(securityEventsUpdateRiskyCommandsRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityEvents', 'updateRiskyCommandsRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTAReplicationStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPTAReplicationStatus((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivilegedThreatAnalyticsPTA', 'getPTAReplicationStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const confirmRequestsConfirmRequestBodyParam = {
      Reason: 'string'
    };
    describe('#confirmRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.confirmRequest(confirmRequestsConfirmRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfirmRequests', 'confirmRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const confirmRequestsRejectRequestBodyParam = {
      Reason: 'string'
    };
    describe('#rejectRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rejectRequest(confirmRequestsRejectRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfirmRequests', 'rejectRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const confirmRequestsOnlywaiting = true;
    const confirmRequestsExpired = true;
    describe('#getIncomingRequestList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIncomingRequestList(confirmRequestsOnlywaiting, confirmRequestsExpired, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfirmRequests', 'getIncomingRequestList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetailsofaRequestforConfirmation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDetailsofaRequestforConfirmation((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfirmRequests', 'getDetailsofaRequestforConfirmation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const myRequestsCreateaRequestBodyParam = {
      AccountID: 'string',
      Reason: 'string',
      TicketingSystemName: 'string'
    };
    describe('#createaRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createaRequest(myRequestsCreateaRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MyRequests', 'createaRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const myRequestsOnlywaiting = true;
    const myRequestsExpired = true;
    describe('#getMyRequests - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMyRequests(myRequestsOnlywaiting, myRequestsExpired, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MyRequests', 'getMyRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetailsofMyRequests - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDetailsofMyRequests((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MyRequests', 'getDetailsofMyRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverType = 'fakedata';
    describe('#logo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logo(serverType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Server', 'logo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#server - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.server((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ExternalVersion);
                assert.equal('string', data.response.InternalVersion);
                assert.equal('string', data.response.ServerName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Server', 'server', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verify - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.verify((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ApplicationName);
                assert.equal(true, Array.isArray(data.response.AuthenticationMethods));
                assert.equal('string', data.response.ServerId);
                assert.equal('string', data.response.ServerName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Server', 'verify', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sessionManagementImportConnectionComponentBodyParam = {};
    describe('#importConnectionComponent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importConnectionComponent(sessionManagementImportConnectionComponentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SessionManagement', 'importConnectionComponent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllConnectionComponents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllConnectionComponents((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.PSMConnectors));
                assert.equal(3, data.response.Total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SessionManagement', 'getAllConnectionComponents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPSMServers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPSMServers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.PSMServers));
                assert.equal(2, data.response.Total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SessionManagement', 'getAllPSMServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionManagementPolicyofPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSessionManagementPolicyofPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SessionManagement', 'getSessionManagementPolicyofPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sessionManagementUpdateSessionManagementPolicyofPlatformBodyParam = {
      PSMServerId: 'string',
      PSMServerName: 'string',
      PSMConnectors: [
        {
          PSMConnectorID: 'PSM-AWSConsoleWithSTS',
          Enabled: false
        }
      ]
    };
    describe('#updateSessionManagementPolicyofPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSessionManagementPolicyofPlatform(sessionManagementUpdateSessionManagementPolicyofPlatformBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SessionManagement', 'updateSessionManagementPolicyofPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getConfiguration((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'getConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemDetails((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ComponentsDetails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemHealth', 'systemDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemSummary((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Components));
                assert.equal(true, Array.isArray(data.response.Vaults));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemHealth', 'systemSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemHealthMatch = 'fakedata';
    describe('#getPTASystemHealth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPTASystemHealth(systemHealthMatch, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemHealth', 'getPTASystemHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const publicSSHAuthenticationAddaPublicSSHKeyBodyParam = {
      PublicSSHKey: 'string'
    };
    describe('#addaPublicSSHKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addaPublicSSHKey(publicSSHAuthenticationAddaPublicSSHKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.AddUserAuthorizedKeyResult);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicSSHAuthentication', 'addaPublicSSHKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPublicSSHKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPublicSSHKey((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.GetUserAuthorizedKeysResult));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicSSHAuthentication', 'getPublicSSHKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const privateSSHAuthenticationGenerateanMFACachingSSHKeyBodyParam = {
      formats: 'string',
      keyPassword: 'string'
    };
    describe('#generateanMFACachingSSHKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateanMFACachingSSHKey(privateSSHAuthenticationGenerateanMFACachingSSHKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateSSHAuthentication', 'generateanMFACachingSSHKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const privateSSHAuthenticationGenerateanMFACachingSSHKeyforAnotherUserBodyParam = {
      formats: 'string',
      keyPassword: 'string'
    };
    describe('#generateanMFACachingSSHKeyforAnotherUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateanMFACachingSSHKeyforAnotherUser(privateSSHAuthenticationGenerateanMFACachingSSHKeyforAnotherUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateSSHAuthentication', 'generateanMFACachingSSHKeyforAnotherUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccount((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'deleteAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSafe - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSafe((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'deleteSafe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGroup((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'deleteGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeUserfromGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeUserfromGroup((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'removeUserfromGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUser((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V2API', 'deleteUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountV93 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccountV93((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'deleteAccountV93', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSafeMember - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSafeMember((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'deleteSafeMember', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUser1((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('V1API', 'deleteUser1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountGroupsDeleteMemberfromAccountGroupBodyParam = {};
    describe('#deleteMemberfromAccountGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMemberfromAccountGroup(accountGroupsDeleteMemberfromAccountGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountGroups', 'deleteMemberfromAccountGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationsAppID = 'fakedata';
    describe('#deleteaSpecificApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteaSpecificApplication(applicationsAppID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Applications', 'deleteaSpecificApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationsAuthID = 555;
    describe('#deleteaSpecificAuthentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteaSpecificAuthentication(applicationsAuthID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Applications', 'deleteaSpecificAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpenIDConnectIdentityProvider - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOpenIDConnectIdentityProvider((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OpenIDConnectIdentityProvider', 'deleteOpenIDConnectIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthenticationMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuthenticationMethod((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthenticationMethodsConfig', 'deleteAuthenticationMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyId = 555;
    describe('#deletePolicyACL - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicyACL(policyId, policyACLPolicyID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deletePolicyACL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicy((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagePolicies', 'deletePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDirectory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDirectory((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPDirectories', 'deleteDirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDirectoryMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDirectoryMapping((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPMappings', 'deleteDirectoryMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOnboardingRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOnboardingRule(onboardingRulesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OnboardingRules', 'deleteOnboardingRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountId = 555;
    describe('#deleteAccountACL - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccountACL(accountId, accountACLAddress, accountACLUserName, accountACLPolicyID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Account', 'deleteAccountACL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTargetPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTargetPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TargetPlatforms', 'deleteTargetPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDependentPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDependentPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DependentPlatforms', 'deleteDependentPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroupPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGroupPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GroupPlatforms', 'deleteGroupPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRotationalGroupPlatform - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRotationalGroupPlatform((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RotationalGroupPlatforms', 'deleteRotationalGroupPlatform', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const myRequestsDeleteMyRequestBodyParam = {};
    describe('#deleteMyRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMyRequest(myRequestsDeleteMyRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MyRequests', 'deleteMyRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePublicSSHKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePublicSSHKey((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PublicSSHAuthentication', 'deletePublicSSHKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteanMFACachingSSHkey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteanMFACachingSSHkey((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateSSHAuthentication', 'deleteanMFACachingSSHkey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllMFACachingSSHKeys - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAllMFACachingSSHKeys((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateSSHAuthentication', 'deleteAllMFACachingSSHKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteanMFACachingSSHKeyforAnotherUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteanMFACachingSSHKeyforAnotherUser((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cyberark-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PrivateSSHAuthentication', 'deleteanMFACachingSSHKeyforAnotherUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
